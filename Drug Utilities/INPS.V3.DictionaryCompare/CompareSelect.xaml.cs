﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace INPS.V3.DictionaryCompare
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class CompareSelect : Window
    {
        private List<string> _dictionaryList = new List<string>();
        private Compare _compare;

        public CompareSelect()
        {
            InitializeComponent();
            lstSourceDictionaries.Items.Clear();
            lstCompareDictionaries.Items.Clear();
            _compare = new Compare();
            _dictionaryList = _compare.GetDictionaryDisplayStrings();
            foreach (string s in _dictionaryList)
                lstSourceDictionaries.Items.Add(s);
            lstSourceDictionaries.SelectedIndex = 0;
        }

        private void lstSourceDictionaries_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string item = lstSourceDictionaries.SelectedItem.ToString();
            int position = _dictionaryList.IndexOf(item);
            lstCompareDictionaries.Items.Clear();
            for (int i = position + 1; i < _dictionaryList.Count; i++)
                lstCompareDictionaries.Items.Add(_dictionaryList[i]);
            lstCompareDictionaries.SelectedIndex = 0;
        }

        private void browse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.SaveFileDialog fdlg = new System.Windows.Forms.SaveFileDialog();
            fdlg.Filter = "Text file|*.txt";
            System.Windows.Forms.DialogResult res = fdlg.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
                txtOutputFolder.Text = fdlg.FileName;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnCompare_Click(object sender, RoutedEventArgs e)
        {
            statusBar.Items.Add(new TextBox().Text = "compare running - please wait...");
            statusBar.Visibility = Visibility.Visible;
            try
            {
                _compare.ProduceReport(lstCompareDictionaries.SelectedValue.ToString(), lstSourceDictionaries.SelectedValue.ToString(), txtOutputFolder.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Comparison report error", MessageBoxButton.OK, MessageBoxImage.Error);
                statusBar.Items.Clear();
                statusBar.Items.Add(new TextBox().Text = "error returned: " + ex.Message);
                return;
            }
            MessageBox.Show("Report saved to " + txtOutputFolder.Text);
            statusBar.Items.Clear();
            statusBar.Items.Add(new TextBox().Text = "Report run successfully");
        }
    }
}
