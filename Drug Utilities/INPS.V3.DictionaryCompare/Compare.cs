﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows;
using System.IO;
using INPS.V3.Data;

namespace INPS.V3.DictionaryCompare
{
    public class Compare
    {
        private StringBuilder _fileContents;
        private SortedList<string, string> _dictionaries;
        private InpsDrugConnection _connection = new InpsDrugConnection();
        private SummaryDetails _summary = new SummaryDetails();
        private string _newSchema;
        private string _oldSchema;
        private SortedList<Int32, DrugClassChanges> _newDrugClassCodes = new SortedList<Int32, DrugClassChanges>();
        private SortedList<Int32, DrugClassChanges> _changedDrugClassCodes = new SortedList<Int32, DrugClassChanges>();
        private SortedList<Int32, DrugClassChanges> _drugsInClassChanges = new SortedList<Int32, DrugClassChanges>();

        private SortedList<Int64, DrugChanges> _newDrugs = new SortedList<Int64, DrugChanges>();
        private SortedList<Int64, DrugChanges> _changedDrugs = new SortedList<Int64, DrugChanges>();

        public Compare()
        {
            _connection.Connect();
            SortedList<string, string> dictionaries = new SortedList<string, string>();
            string liveVersion = "";
            try
            {
                liveVersion = _connection.GetFirstRow("SELECT inps_version FROM drug_dictionary.drug_dictionary WHERE live = true")["inps_version"].ToString();
            }
            catch
            {
                MessageBox.Show("Live dictionary version cannot be identified", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            DataTable table = _connection.GetTable("SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_inps_drug'");
            foreach (DataRow row in table.Rows)
            {
                //add sort string and display string
                try
                {
                    string sortString = row["schema_name"].ToString();
                    string displayString = sortString.Substring(0, 4) + " "
                        + _months[Convert.ToInt32(sortString.Substring(5, 2))]
                        + " v." + sortString.Substring(8, 2);
                    if (liveVersion != "" && sortString.StartsWith(liveVersion.Replace(".", "_"))) displayString = displayString + " (Live)";
                    dictionaries.Add(sortString, displayString);
                }
                catch
                {
                    //add warning to status bar?
                }
            }
            _dictionaries = dictionaries;            
        }

        private class DrugClassChanges
        {
            private Int32 _classCode;

            public Int32 ClassCode
            {
                get { return _classCode; }
            }
            private string _oldName;

            public string OldName
            {
                get { return _oldName; }
            }
            private string _currentName;

            public string CurrentName
            {
                get { return _currentName; }
            }
            private SortedList<Int64, string> _additions;

            public SortedList<Int64, string> Additions
            {
                get { return _additions; }
                set { _additions = value; }
            }
            private SortedList<Int64, string> _removals;

            public SortedList<Int64, string> Removals
            {
                get { return _removals; }
                set { _removals = value; }
            }

            public DrugClassChanges(Int32 classCode, string currentName)
            {
                _classCode = classCode;
                _currentName = currentName;
            }

            public DrugClassChanges(Int32 classCode, string currentName, string oldName)
            {
                _classCode = classCode;
                _currentName = currentName;
                _oldName = oldName;
            }
        }

        private class DrugChanges
        {
            private Int32 _drugCode;

            public Int32 DrugCode
            {
                get { return _drugCode; }
            }

            private Int64 _dmdCode;

            public Int64 DmdCode
            {
                get { return _dmdCode; }
            }

            private string _currentName;

            public string CurrentName
            {
                get { return _currentName; }
            }

            //true if VMP or branded generic
            private bool _isGeneric;

            public bool IsGeneric
            {
                get { return _isGeneric; }
            }

            private bool _isDiscontinued;

            public bool IsDiscontinued
            {
                get { return _isDiscontinued; }
            }

            private DrugType _dType;

            public DrugType DType
            {
                get { return _dType; }
            }

            private List<FieldValues> _fields;

            public List<FieldValues> Fields
            {
                get { return _fields; }
            }

            private SortedList<Int32, string> _drugClasses;

            public SortedList<Int32, string> DrugClasses
            {
                get { return _drugClasses; }
            }

            private List<string> _addedIngredients;

            public List<string> AddedIngredients
            {
                get { return _addedIngredients; }
            }
            private List<string> _removedIngredients;

            public List<string> RemovedIngredients
            {
                get { return _removedIngredients; }
            }

            public DrugChanges(Int32 drugCode, Int64 dmdCode, string currentName, DrugType type, bool isGeneric, bool isDiscontinued)
            {
                _drugCode = drugCode;
                _dmdCode = dmdCode;
                _currentName = currentName;
                _dType = type;
                _isGeneric = isGeneric;
                _isDiscontinued = isDiscontinued;
            }

            public DrugChanges(Int32 drugCode, Int64 dmdCode, string currentName, DrugType type, bool isGeneric, bool isDiscontinued, SortedList<Int32, string> drugClasses)
            {
                _drugCode = drugCode;
                _dmdCode = dmdCode;
                _currentName = currentName;
                _dType = type;
                _isGeneric = isGeneric;
                _isDiscontinued = isDiscontinued;
                _drugClasses = drugClasses;
            }

            public DrugChanges(Int32 drugCode, Int64 dmdCode, string currentName, DrugType type, bool isGeneric, List<FieldValues> values, List<string> addedIngredients, List<string> removedIngredients)
            {
                _drugCode = drugCode;
                _dmdCode = dmdCode;
                _currentName = currentName;
                _dType = type;
                _isGeneric = isGeneric;
                _fields = values;
                _addedIngredients = addedIngredients;
                _removedIngredients = removedIngredients;
            }
    
        }

        enum DrugType {VMP, AMP, LEX};

        private class FieldValues
        {
            private string _fieldName;

            public string FieldName
            {
                get { return _fieldName; }
            }
            private string _oldValue;

            public string OldValue
            {
                get { return _oldValue; }
            }
            private string _currentValue;

            public string CurrentValue
            {
                get { return _currentValue; }
            }

            public FieldValues(string fieldName, string currentValue)
            {
                _fieldName = fieldName;
                _currentValue = currentValue;
            }

            public FieldValues(string fieldName, string currentValue, string oldValue)
            {
                _fieldName = fieldName;
                _currentValue = currentValue;
                _oldValue = oldValue;
            }
        }

        private struct SummaryDetails
        {
            public string buildDate;
            public string dmdVersion;
            public string npVersion;
        }

        private void SetSummaryDetails()
        {
            string query = String.Format("SELECT * from \"{0}\".drug_dictionary WHERE live = true", _newSchema);
            DataRow row = _connection.GetFirstRow(query);
            if (row != null)
            {
                _summary.buildDate = row["build_date"].ToString();
                _summary.dmdVersion = row["dmd_version"].ToString();
                _summary.npVersion = row["np_Data_version"].ToString();
            }
        }

        private void SetDrugClassChanges()
        {
            _newDrugClassCodes.Clear();
            _changedDrugClassCodes.Clear();
            _drugsInClassChanges.Clear();
            string query = String.Format("SELECT * FROM \"{0}\".drug_class", _newSchema);
            DataTable table = _connection.GetTable(query);
            //foreach entry check if present or if changed
            if (table != null)
            {
                foreach (DataRow row in table.Rows)
                {
                    DrugClassChanges change = null;
                    query = String.Format("SELECT * FROM \"{0}\".drug_class WHERE drug_class_code = {1}", _oldSchema, row["drug_class_code"].ToString());
                    DataRow oldRow = _connection.GetFirstRow(query);
                    if (oldRow == null)
                    {
                        change = new DrugClassChanges(Convert.ToInt32(row["drug_class_code"]), row["description"].ToString());
                        //new drug classes
                        _newDrugClassCodes.Add(Convert.ToInt32(row["drug_class_code"]), change);
                    }
                    else
                    {
                        if (oldRow["description"].ToString() != row["description"].ToString())
                            _changedDrugClassCodes.Add(Convert.ToInt32(row["drug_class_code"]), new DrugClassChanges(Convert.ToInt32(row["drug_class_code"]), row["description"].ToString(), oldRow["description"].ToString()));
                    }
                    //get drugs in each class
                    SortedList<Int64, string> currentDrugs = new SortedList<Int64, string>();
                    SortedList<Int64, string> oldDrugs = new SortedList<Int64, string>();
                    //get current list of drugs in class
                    query = String.Format("SELECT dc.*, d.name, d.dmd_code FROM \"{0}\".drug_class_mapping dc JOIN \"{0}\".drug_gemscript d ON dc.drug_code = d.drug_code WHERE drug_class_code = {1}", _newSchema, row["drug_class_code"].ToString());
                    DataTable t = _connection.GetTable(query);
                    if (t != null)
                    {
                        foreach (DataRow r in t.Rows)
                            if (!currentDrugs.ContainsKey(Convert.ToInt64(r["dmd_code"]))) currentDrugs.Add(Convert.ToInt64(r["dmd_code"]), r["name"].ToString());
                    }
                    //get previous list of drugs in class
                    query = String.Format("SELECT dc.*, d.name, d.dmd_code FROM \"{0}\".drug_class_mapping dc JOIN \"{0}\".drug_gemscript d ON dc.drug_code = d.drug_code WHERE drug_class_code = {1}", _oldSchema, row["drug_class_code"].ToString());
                    t = _connection.GetTable(query);
                    if (t != null)
                    {
                        foreach (DataRow r in t.Rows)
                            if (!oldDrugs.ContainsKey(Convert.ToInt64(r["dmd_code"]))) oldDrugs.Add(Convert.ToInt64(r["dmd_code"]), r["name"].ToString());
                    }
                    //Compare lists of drugs in class
                    SortedList<Int64, string> additions = new SortedList<Int64, string>();
                    SortedList<Int64, string> removals = new SortedList<Int64, string>();
                    foreach (Int64 i in currentDrugs.Keys)
                    {
                        if (!oldDrugs.ContainsKey(i) && !oldDrugs.ContainsValue(currentDrugs[i]))
                            additions.Add(Convert.ToInt64(i), currentDrugs[i]);
                    }
                    foreach (Int64 i in oldDrugs.Keys)
                    {
                        if (!currentDrugs.ContainsKey(i) && !currentDrugs.ContainsValue(oldDrugs[i]))
                            removals.Add(Convert.ToInt64(i), oldDrugs[i]);
                    }
                    if (additions.Count > 0 || removals.Count > 0)
                    {
                        //create change if not exists yet
                        if (change == null)
                            change = new DrugClassChanges(Convert.ToInt32(row["drug_class_code"]), row["description"].ToString());
                        change.Additions = additions;
                        change.Removals = removals;
                        _drugsInClassChanges.Add(Convert.ToInt32(row["drug_class_code"]), change);
                    }
                }
            }
        }

        private void SetDrugChanges()
        {
            //get new drugs
            _newDrugs.Clear();
            _changedDrugs.Clear();
            string query = String.Format("SELECT * FROM \"{0}\".drug_gemscript WHERE england = true AND is_active = true", _newSchema);
            DataTable table = _connection.GetTable(query);
            if (table != null)
            {
                foreach (DataRow currentRow in table.Rows)
                {
                    //check if exists in old drug table
                    //if not add to list of new drugs
                    //some drug codes may exist more than once due to inactive duplicates for different views
                    {
                        Int32 drugCode = Convert.ToInt32(currentRow["drug_code"]);
                        Int64 dmdCode = Convert.ToInt64(currentRow["dmd_code"]);
                        bool isGeneric = Convert.ToBoolean(currentRow["is_branded_generic"]) || Convert.ToBoolean(currentRow["is_generic"]);
                        DrugType dt;
                        if (currentRow["dmd_type"].ToString() == "") dt = DrugType.LEX;
                        else if (currentRow["dmd_type"].ToString() == "715011000027108") dt = DrugType.AMP;
                        else dt = DrugType.VMP;
                        query = String.Format("SELECT * FROM \"{0}\".drug_gemscript WHERE drug_code = {1} AND england  = true AND is_active = true", _oldSchema, drugCode);
                        DataRow previousRow = _connection.GetFirstRow(query);
                        try
                        {
                            if (previousRow == null)
                            {
                                if (!_newDrugs.ContainsKey(Convert.ToInt32(currentRow["drug_code"])))
                                {
                                    //add drug class codes for each new drug
                                    query = String.Format("SELECT dcm.drug_class_code, description from \"{0}\".drug_class_mapping dcm JOIN \"{0}\".drug_class dc ON dc.drug_class_code = dcm.drug_class_code WHERE drug_code = {1}",
                                        _newSchema, drugCode);
                                    DataTable c = _connection.GetTable(query);
                                    if (c != null && c.Rows.Count > 0)
                                    {
                                        SortedList<Int32, string> drugClasses = new SortedList<int, string>();
                                        foreach (DataRow cr in c.Rows)
                                            drugClasses.Add(Convert.ToInt32(cr["drug_class_code"]), cr["description"].ToString());
                                        _newDrugs.Add(Convert.ToInt64(currentRow["dmd_code"]), new DrugChanges(drugCode, dmdCode, currentRow["name"].ToString(), dt, isGeneric, Convert.ToBoolean(currentRow["is_discontinued"]), drugClasses));
                                    }
                                    else
                                    {
                                        _newDrugs.Add(Convert.ToInt64(currentRow["dmd_code"]), new DrugChanges(drugCode, dmdCode, currentRow["name"].ToString(), dt, isGeneric, Convert.ToBoolean(currentRow["is_discontinued"])));
                                    }
                                }
                            }
                            else
                            {
                                //check all fields in inps_drug.drug to be compared
                                string[] fields = { "name", "is_discontinued", "is_available", "is_triangle", "is_doh_approved", "search_key", "is_acbs", "is_fp10" };
                                List<FieldValues> f = new List<FieldValues>();
                                foreach (string s in fields)
                                    if (currentRow[s].ToString() != previousRow[s].ToString())
                                        f.Add(new FieldValues(s, currentRow[s].ToString(), previousRow[s].ToString()));
                                //check ingredients
                                query = String.Format("SELECT * FROM \"{0}\".compo_cip_pa WHERE \"ID_PRODUCT\" = '{1}'", _newSchema.Replace("inps_drug", "bcb_datas"), Convert.ToInt64(currentRow["dmd_code"]));
                                DataTable currentIngredients = _connection.GetTable(query);
                                List<string> currentIngredientList = new List<string>();
                                List<string> oldIngredientList = new List<string>();
                                if (currentIngredients != null)
                                    foreach (DataRow ci in currentIngredients.Rows)
                                        currentIngredientList.Add(ci["DMD_CODE"].ToString() + "|" + ci["CODE_PA"].ToString());
                                query = String.Format("SELECT * FROM \"{0}\".compo_cip_pa WHERE \"ID_PRODUCT\" = '{1}'", _oldSchema.Replace("inps_drug", "bcb_datas"), Convert.ToInt64(currentRow["dmd_code"]));
                                DataTable oldIngredients = _connection.GetTable(query);
                                if (oldIngredients != null)
                                    foreach (DataRow ci in oldIngredients.Rows)
                                        oldIngredientList.Add(ci["DMD_CODE"].ToString() + "|" + ci["CODE_PA"].ToString());
                                //Compare lists of drugs in class
                                List<string> additions = new List<string>();
                                List<string> removals = new List<string>();
                                foreach (string s in currentIngredientList)
                                    if (!oldIngredientList.Contains(s))
                                    {
                                        //need to get ingredient name here
                                        string iName = "";
                                        if (s.Split('|')[0] != "0")
                                        {
                                            //use dm+d code
                                            query = String.Format("SELECT * FROM bcb_inps.ingredients WHERE id = {0}", s.Split('|')[0]);
                                            DataRow ing = _connection.GetFirstRow(query);
                                            if (ing != null)
                                                iName = ing["name"].ToString();
                                        }
                                        if (iName == "") //if dmd code was '0' or query did not return result
                                        {
                                            query = String.Format("SELECT * FROM bcb_etr.compo_composants WHERE \"CODE_COMPOSANT\"= {0}", s.Split('|')[1]);
                                            DataRow ing = _connection.GetFirstRow(query);
                                            if (ing != null)
                                                iName = ing["LIBELLE"].ToString();
                                        }
                                        additions.Add(iName);
                                    }
                                foreach (string s in oldIngredientList)
                                    if (!currentIngredientList.Contains(s))
                                    {
                                        //need to get ingredient name here
                                        string iName = "";
                                        if (s.Split('|')[0] != "0")
                                        {
                                            //use dm+d code
                                            query = String.Format("SELECT * FROM bcb_inps.ingredients WHERE id = {0}", s.Split('|')[0]);
                                            DataRow ing = _connection.GetFirstRow(query);
                                            if (ing != null)
                                                iName = ing["name"].ToString();
                                        }
                                        if (iName == "") //if dmd code was '0' or query did not return result
                                        {
                                            query = String.Format("SELECT * FROM bcb_etr.compo_composants WHERE \"CODE_COMPOSANT\"= {0}", s.Split('|')[1]);
                                            DataRow ing = _connection.GetFirstRow(query);
                                            if (ing != null)
                                                iName = ing["LIBELLE"].ToString();
                                        }
                                        removals.Add(iName);
                                    }
                                if (f.Count > 0 || additions.Count > 0 || removals.Count > 0)
                                    if (!_changedDrugs.ContainsKey(dmdCode))
                                        _changedDrugs.Add(Convert.ToInt64(currentRow["dmd_code"]), new DrugChanges(drugCode, dmdCode, currentRow["name"].ToString(), dt, isGeneric, f, additions, removals));
                            }
                        }
                        catch (Exception e)
                        {
                            throw new Exception("Error adding drug " + dmdCode + " to list of changed/added drugs", e);
                        }
                    }                  
                }
            }
        }

        public void ProduceReport(string oldDictionary, string newDictionary, string outputFile)
        {
            //checked outputFile path is valid
            try
            {
                Path.GetDirectoryName(outputFile);
                Path.GetFileName(outputFile);

            }
            catch
            {
                throw new Exception(string.Format("Report filepath ({0}) is not valid", outputFile));
            }
            _oldSchema = _dictionaries.Keys[_dictionaries.IndexOfValue(oldDictionary)];
            _newSchema = _dictionaries.Keys[_dictionaries.IndexOfValue(newDictionary)];
            SetSummaryDetails();
            SetDrugClassChanges();
            SetDrugChanges();
            OutputReport(outputFile);
            
            //MessageBox.Show("Report saved to " + outputFile);
        }

        private void OutputReport(string outputFile)
        {
            string newline = "\r\n";
            _fileContents = new StringBuilder();
            _fileContents.Append("GEMSCRIPT DICTIONARY COMPARISON - Changes between " + _dictionaries[_newSchema] + " and " + _dictionaries[_oldSchema]);
            _fileContents.Append(newline + newline + "***********************************************************************************");
            _fileContents.Append(newline + "SUMMARY");
            _fileContents.Append(newline + "***********************************************************************************");
            _fileContents.Append(newline + "Build date: ");
            _fileContents.Append(_summary.buildDate);
            _fileContents.Append(newline + "DM+D version: ");
            _fileContents.Append(_summary.dmdVersion);
            _fileContents.Append(newline + "Nexphase data version: ");
            _fileContents.Append(_summary.npVersion);
            _fileContents.Append(newline + newline + "***********************************************************************************");
            _fileContents.Append(newline + "NEW DRUG CLASSES");
            _fileContents.Append(newline + "***********************************************************************************");
            foreach (KeyValuePair<Int32, DrugClassChanges> item in _newDrugClassCodes)
                _fileContents.Append(newline + item.Key + "\t" + item.Value.CurrentName);
            _fileContents.Append(newline + newline + "***********************************************************************************");
            _fileContents.Append(newline + "CHANGED DRUG CLASSES");
            _fileContents.Append(newline + "***********************************************************************************");
            foreach (KeyValuePair<Int32, DrugClassChanges> item in _changedDrugClassCodes)
                _fileContents.Append(newline + newline + "CLASS CODE:\t" + item.Key + newline + "PREVIOUS NAME:\t" + item.Value.OldName + newline + "CURRENT NAME:\t" + item.Value.CurrentName);
            _fileContents.Append(newline + newline + "***********************************************************************************");
            _fileContents.Append(newline + "CHANGES TO DRUGS IN DRUG CLASSES");
            _fileContents.Append(newline + "***********************************************************************************");
            foreach (KeyValuePair<Int32, DrugClassChanges> item in _drugsInClassChanges)
            {
                _fileContents.Append(newline + newline + "===================================================================================");
                _fileContents.Append(newline + item.Key + "\t" + item.Value.CurrentName);
                _fileContents.Append(newline + "===================================================================================");
                if (item.Value.Additions != null && item.Value.Additions.Count > 0)
                {
                    _fileContents.Append(newline + newline + "ADDITIONS");
                    _fileContents.Append(newline + "---------");
                    foreach (KeyValuePair<Int64, string> p in item.Value.Additions)
                        _fileContents.Append(newline + p.Key + "\t" + p.Value);
                }
                if (item.Value.Removals != null && item.Value.Removals.Count > 0)
                {
                    _fileContents.Append(newline + newline + "REMOVALS");
                    _fileContents.Append(newline + "--------");
                    foreach (KeyValuePair<Int64, string> p in item.Value.Removals)
                        _fileContents.Append(newline + p.Key + "\t" + p.Value);
                }
            }
            _fileContents.Append(newline + newline + "***********************************************************************************");
            _fileContents.Append(newline + "NEW DRUGS");
            _fileContents.Append(newline + "***********************************************************************************");
            foreach (DrugChanges item in _newDrugs.Values)
            {
                if (!item.IsDiscontinued)
                {
                    _fileContents.Append(newline + item.DmdCode.ToString().PadLeft(8, '0') + "\t" + (item.IsGeneric ? "GENERIC" : "BRANDED") + "\t" + item.DType.ToString() + "\t" + item.CurrentName);
                    if (item.DrugClasses != null)
                    {
                        _fileContents.Append(newline + "\tDRUG CLASSES");
                        foreach (KeyValuePair<Int32, string> dc in item.DrugClasses)
                            _fileContents.Append(newline + "\t" + dc.Key + "\t" + dc.Value);
                        _fileContents.Append(newline);
                    }
                }
            }
            _fileContents.Append(newline + newline + "***********************************************************************************");
            _fileContents.Append(newline + "CHANGED DRUGS");
            _fileContents.Append(newline + "***********************************************************************************");
            foreach (DrugChanges item in _changedDrugs.Values)
            {
                _fileContents.Append(newline + newline + "===================================================================================");
                _fileContents.Append(newline + item.DmdCode.ToString().PadLeft(8, '0') + "\t" + (item.IsGeneric ? "GENERIC" : "BRANDED") + "\t" + item.DType.ToString() + "\t" + item.CurrentName);
                _fileContents.Append(newline + "===================================================================================");
                //_changedDrugClassCodes fields
                foreach (FieldValues f in item.Fields)
                {
                    _fileContents.Append(newline + "-----------------------------------------------" + newline + "FIELD\t" + f.FieldName + newline + "-----------------------------------------------");
                    _fileContents.Append(newline + "PREVIOUS VALUE\t" + f.OldValue + newline + "CURRENT VALUE\t" + f.CurrentValue);
                }
                if (item.AddedIngredients != null && item.AddedIngredients.Count > 0)
                {
                    _fileContents.Append(newline + "-----------------------------------------------" + newline + "ADDED INGREDIENTS" + newline + "-----------------------------------------------");
                    foreach (String s in item.AddedIngredients)
                    {
                        _fileContents.Append(newline + "\t" + s);
                    }
                }
                if (item.RemovedIngredients != null && item.RemovedIngredients.Count > 0)
                {
                    _fileContents.Append(newline + "-----------------------------------------------" + newline + "REMOVED INGREDIENTS" + newline + "-----------------------------------------------");
                    foreach (String s in item.RemovedIngredients)
                    {
                        _fileContents.Append(newline + "\t" + s);
                    }
                }
            }
            string outputDirectory = outputFile.Substring(0, outputFile.Replace("/", "\\").LastIndexOf("\\"));
            if (!Directory.Exists(outputDirectory)) Directory.CreateDirectory(outputDirectory);
            File.WriteAllText(outputFile, _fileContents.ToString());
        }

        public List<string> GetDictionaryDisplayStrings()
        {
            List<string> displayStrings = new List<string>();
            foreach (KeyValuePair<string, string> item in _dictionaries.OrderByDescending(x => x.Key))
                displayStrings.Add(item.Value);
            return displayStrings;
        }

        public void GenerateLatestComparisonReport(string filePath)
        {
            //work backwards through dictionary til first 8 char of key not the same as current
            string currentMonth = _dictionaries.Values[_dictionaries.Count - 1].Substring(0, 8);
            int compareDictionary = _dictionaries.Count - 2;
            for (int i = compareDictionary; i >= 0; i--)
            {
                if (!_dictionaries.Values[i].Substring(0, 8).Equals(currentMonth))
                {
                    compareDictionary = i;
                    break;
                }
            }
            ProduceReport(_dictionaries.Values[compareDictionary], _dictionaries.Values[_dictionaries.Count - 1], filePath);
        }

        private static String[] _months = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    }
}
