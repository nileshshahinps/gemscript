﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using INPS.V3.Data;
using INPS.V3.DictionaryCompare;

namespace Drug_Utilities
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private DrugDataBuilder _builder;

        public Window1()
        {
            try
            {
                InitializeComponent();
                this.btnBuildDrugs.IsEnabled = false;
                DrugDataBuilder.Settings.Add("database", Properties.Settings.Default.Database);
                DrugDataBuilder.Settings.Add("host", Properties.Settings.Default.Host);
                DrugDataBuilder.Settings.Add("user", Properties.Settings.Default.User);
                DrugDataBuilder.Settings.Add("password", Properties.Settings.Default.Password);
                DrugDataBuilder.Settings.Add("root_folder", Properties.Settings.Default.RootFolder);
                //create Listener for this date
                Trace.Listeners.Clear();
                string name = string.Format("C:/Temp/DrugDictionaryBuilder_{0}.log", DateTime.Now.ToString("yyyyMMdd"));
                TraceListener myTraceListener = new TextWriterTraceListener(new FileStream(name, FileMode.Append));
                Trace.Listeners.Add(myTraceListener);
                Trace.AutoFlush = true;
                if (CreateBuilder()) this.btnBuildDrugs.IsEnabled = true;        
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception thrown\n" + e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CreateBuilder()
        {
            bool settingsOK = false;
            if (DrugDataBuilder.Settings["database"] != "" && DrugDataBuilder.Settings["host"] != "" && DrugDataBuilder.Settings["user"] != "" && DrugDataBuilder.Settings["password"] != "")
            {
                try
                {
                    _builder = new DrugDataBuilder();
                    textBoxICF.Text = _builder.GetMinICF();
                    textBoxICD.Text = _builder.GetICD10();
                    textBoxYear.Text = DateTime.Now.Year.ToString();
                    textBoxMonth.Text = DateTime.Now.Month.ToString("00");
                    settingsOK = true;
                }
                catch (Exception e)
                {
                    _builder = null;
                    MessageBox.Show("Builder cannot connect to specified database - please check settings.\n" + e.Message + (e.InnerException != null ? e.InnerException.Message : ""), "Builder error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (DrugDataBuilder.Settings["root_folder"] == "" || !Directory.Exists(DrugDataBuilder.Settings["root_folder"]))
                {
                    settingsOK  = false;
                    MessageBox.Show("Builder cannot find root folder - please check settings.\n", "Builder error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return settingsOK;
        }

        private void btnBuildDrugs_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trace.WriteLine(DateTime.Now + " "  + "Build started");
                _builder = new DrugDataBuilder();
                Dictionary<String, String> versionDetails = new Dictionary<String, String>();
                versionDetails.Add("year", textBoxYear.Text);
                versionDetails.Add("month", textBoxMonth.Text);
                versionDetails.Add("icf", textBoxICF.Text);
                versionDetails.Add("icd10", textBoxICD.Text);
                _builder.setDictionaryVersion(versionDetails);
                _builder.ImportRoutesMappingTable();
                Trace.WriteLine(DateTime.Now + " " + "Route mapping table imported");
                _builder.BuildAllSetUp();
                Trace.WriteLine(DateTime.Now + " " + "Initial set up complete");
                _builder.ImportICD10();
                Trace.WriteLine(DateTime.Now + " " + "IDC 10 imported");
                _builder.BuildRdc();
                Trace.WriteLine(DateTime.Now + " " + "RDC built");
                _builder.BuildAll();
                Trace.WriteLine(DateTime.Now + " " + "inps_gemscript built");
                _builder.WriteDictionaryChangeDetails();
                Trace.WriteLine(DateTime.Now + " " + "dictionary change table written");
                _builder.ImportUnmappedLex(cbxMultilexBuild.IsChecked == true);
                Trace.WriteLine(DateTime.Now + " " + "unmapped lex imported");
                _builder.ReconcileDrugTables();
                Trace.WriteLine(DateTime.Now + " " + "drug tables reconciled");
                _builder.WriteTermDetails();
                Trace.WriteLine(DateTime.Now + " " + "term details written");
                _builder.ImportNexphaseData();
                Trace.WriteLine(DateTime.Now + " " + "nexphase data imported");
                //_builder.addVMPNexphaseDataToAMPs();
                //Trace.WriteLine(DateTime.Now + " " + "nexphase data - VMP data added to AMPs");
                _builder.AddLabelTerms();
                Trace.WriteLine(DateTime.Now + " " + "label terms added");
                //write dictionary items  
                _builder.PopulateDrugInpsTable();
                Trace.WriteLine(DateTime.Now + " " + "inps table populated");
                _builder.CreateDrugGemCSV();
                Trace.WriteLine(DateTime.Now + " " + "drug gem csv created");
                _builder.CreatePreparationList();
                Trace.WriteLine(DateTime.Now + " " + "preparation list created");
                _builder.CreateCodeList();
                Trace.WriteLine(DateTime.Now + " " + "code list created");
                _builder.CreateDrugMapXml();
                Trace.WriteLine(DateTime.Now + " " + "drug map created");
                _builder.WriteDictionaryDetails(versionDetails);
                Trace.WriteLine(DateTime.Now + " " + "dictionary details written");
                _builder.CreateRecordCount();
                _builder.BuildAllTidyUp();
                Trace.WriteLine(DateTime.Now + " " + "build tidy completed");
                Compare compare = new Compare();
                compare.GenerateLatestComparisonReport(Properties.Settings.Default.RootFolder + "\\" + _builder.DictionaryVersion + "\\ComparisonReport.txt");
                Trace.WriteLine(DateTime.Now + " " + "compare report generated");
                labelOutput.Content = _builder.TraceText;
                labelErrorText.Text = _builder.ErrorText;
                Trace.WriteLine(DateTime.Now + " " + _builder.ErrorText);
            }
            catch (System.Exception ex)
            {
                labelErrorText.Text = ex.Message + "\n" + _builder.ErrorText;
                Trace.WriteLine(DateTime.Now + " " + ex.Message);
                Trace.WriteLine(ex.StackTrace);
            }
        }

        private void btnConnection_Click(object sender, RoutedEventArgs e)
        {
            this.btnBuildDrugs.IsEnabled = false;
            Config conf = new Config(DrugDataBuilder.Settings);
            conf.ShowDialog();
            if (CreateBuilder()) this.btnBuildDrugs.IsEnabled = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Database = DrugDataBuilder.Settings["database"];
            Properties.Settings.Default.Host = DrugDataBuilder.Settings["host"];
            Properties.Settings.Default.User = DrugDataBuilder.Settings["user"];
            Properties.Settings.Default.Password = DrugDataBuilder.Settings["password"];
            Properties.Settings.Default.RootFolder = DrugDataBuilder.Settings["root_folder"];
            Properties.Settings.Default.Save();
            Trace.Close();
        }

        private void btnCompare_Click(object sender, RoutedEventArgs e)
        {
            CompareSelect choose = new CompareSelect();
            choose.ShowDialog();
        }
    }
}
