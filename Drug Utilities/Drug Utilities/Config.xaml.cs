﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Drug_Utilities
{
    /// <summary>
    /// Interaction logic for Config.xaml
    /// </summary>
    public partial class Config : Window
    {

        private Dictionary<string, string> _settings;

        public Config(Dictionary<string, string> settings)
        {
            InitializeComponent();
            _settings = settings;
            txtDatabase.Text = _settings["database"];
            txtHost.Text = _settings["host"];
            txtUser.Text = _settings["user"];
            txtPassword.Password = _settings["password"];
            txtRootFolder.Text = _settings["root_folder"];
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            _settings["database"] = txtDatabase.Text;
            _settings["host"] = txtHost.Text;
            _settings["user"] = txtUser.Text;
            _settings["password"] = txtPassword.Password;
            _settings["root_folder"] = txtRootFolder.Text;
            this.DialogResult = true;
            this.Close();
        }

        private void browse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fdlg = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult res = fdlg.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                txtRootFolder.Text = fdlg.SelectedPath;
            }
        }
    }
}
