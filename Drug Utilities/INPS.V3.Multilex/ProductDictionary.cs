﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INPS.V3.Multilex
{
    public class ProductCode
    {
        private int _code;
        public class LexFormatCode
        {
            private int _code;
            public LexFormatCode(int code)
            {
                _code = code;
            }

            public int Code { get {return _code;}}
        }

        public class VisionFormatCode
        {
            private int _code;
            public VisionFormatCode(int code)
            {
                _code = code;
            }

            public int Code { get {return _code;}}
        }

        public ProductCode(LexFormatCode code)
        {
            _code = code.Code;
        }

        public ProductCode(VisionFormatCode code)
        {
            _code = code.Code%1000 * 1000000 + code.Code/1000;
        }

        public int LexCode
        {
            get { return _code;}
        }

        public int VisionCode
        {
            get {return (_code%1000000) * 1000 + _code/1000000;}
        }

        public string VisionCodeText
        {
            get { return String.Format("{0:d8}", VisionCode); }
        }
    }

    public class LexProduct
    {
        private ProductCode _lexCode;
        private ulong _dmdCode;

        public LexProduct(ProductCode lexCode, ulong dmdCode)
        {
            _lexCode = lexCode;
            _dmdCode = dmdCode;
        }

        public ProductCode LexCode
        {
            get {return _lexCode;}
        }

        public ulong DmdCode
        {
            get {return _dmdCode;}
        }
    }

    public class Converter
    {

        private static String b64Value = "0123456789?@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        public static String getB10FromB64(String b64)
        {
            Int64 b10 = 0;
            int i = b64.Length - 1;
            foreach (char c in b64)
            {
                b10 = b10 + b64Value.IndexOf(c) * Convert.ToInt64(Math.Pow(64, i));
                i--;
            }
            return Convert.ToUInt64(b10).ToString();
        }

        public static String getB64FromB10(Int64 b10)
        {
            string b64 = "";
            int max = 0;

            while (Math.Floor((b10 / Math.Pow(64, ++max))) > 0);

            for (int i = max - 1; i >= 0; i--)
            {
                Int32 d = Convert.ToInt16(Convert.ToInt64(b10) / Convert.ToInt64(Math.Pow(64, i)));
                b64 = b64 + b64Value[d];
                b10 = b10 - Convert.ToInt64(Math.Pow(64, i)) * d;
            }
            return b64;
        }
    }
}
