﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace INPS.V3.Resip
{

    static class ResipAPIMethods
    {
        internal const string DllName = "BCBUKMFC.dll";

        [DllImport(DllName)]
        internal static extern Int32 dataVersion(Int32 typeDB, ref string version);

        [DllImport(DllName)]
        internal static extern Int32 apiVersion(ref string date, ref string version);

        [DllImport(DllName)]
        internal static extern Int32 connectDatabase(string ipDB, string loginDB, string passwordDB, string schemaDB, string nameDB, Int32 typeDB);

        [DllImport(DllName)]
        internal static extern Int32 closeDatabases();

        [DllImport(DllName)]
        //internal static extern Int32 productRDC(string productID, ref ResipAPI.Type_Rdc[] TabOutput);
        internal static extern Int32 productRDC(string productID, [MarshalAs(UnmanagedType.SafeArray)] out byte[,] TabOutput);
        //internal static extern Int32 productRDC(string productID, [MarshalAs(UnmanagedType.SafeArray)] out System.Array TabOutput);

        [DllImport(DllName)]
        internal static extern Int32 readCode([MarshalAs(UnmanagedType.LPStr)] string productID, [MarshalAs(UnmanagedType.LPStr)] ref string rCode);

        [DllImport(DllName)]
        internal static extern Int32 toxicityWarning([MarshalAs(UnmanagedType.LPStr)] string productID, [MarshalAs(UnmanagedType.LPStr)] out string warning);

        [DllImport(DllName)]
        internal static extern Int32 productFlags(string productID, ref ResipAPI.TypeFlags flags);

        [DllImport(DllName)]
        internal static extern Int32 GetProductsControlStatus(string productID, out byte[,] TabOutput);

        [DllImport(DllName)]
        internal static extern Int32 posology(string productID, ref ResipAPI.TypePosology posologyData);

        [DllImport(DllName)]
        internal static extern Int32 labelWarning(string productID, ref ResipAPI.TypeCaution cautionData);

        [DllImport(DllName)]
        //internal static extern Int32 produitCompo(string productCode, ref ResipAPI.Type_ExpressedBy[] TabOutputExpressedBy, ref ResipAPI.Type_Ingredients[] TabOutputComponents, ref ResipAPI.Type_Excipients[] TabOutputExcipients, ref Int32 nbComponents, ref Int32 nbExcipients);
        internal static extern Int32 produitCompo(string productCode, ref byte[] TabOutputExpressedBy, ref byte[] TabOutputComponents, ref byte[] TabOutputExcipients, ref Int32 nbComponents, ref Int32 nbExcipients);
    }

    public class ResipAPI
    {
        /*public static Int32 Connect(string host, string user, string password, string database, string dataSchema, string etrSchema)
        {
            ResipAPIMethods.closeDatabases();
            return ResipAPIMethods.connectDatabase(host, user, password, database, dataSchema, 1)
                + ResipAPIMethods.connectDatabase(host, user, password, database, etrSchema, 2);
        }

        public static Int32 Close()
        {
            return ResipAPIMethods.closeDatabases();
        }

        public static string GetReadCode(string dmdCode)
        {
            string readCode = null;
            Int32 ret = ResipAPIMethods.readCode(dmdCode, ref readCode);
            return readCode.Trim();
        }*/

        public static string GetReadCode(System.Data.DataRow data)
        {
            return data["read_code"].ToString().Trim();
        }

        public static string GetShortName(System.Data.DataRow data)
        {
            return data["abbreviated_name"].ToString().Trim();
        }

        /*public static string GetWarning(string dmdCode)
        {
            string warnings = null;
            Int32 ret = ResipAPIMethods.toxicityWarning(dmdCode, out warnings);
            if (ret == 0) warnings = null;
            return warnings;
        }*/

        public static string GetWarning(System.Data.DataRow data)
        {
            return data["toxique_commentaire"].ToString().Trim();
        }

        public static DateTime GetAPIDate()
        {
            string apiVersion = "", apiDate = "";
            ResipAPIMethods.apiVersion(ref apiVersion, ref apiDate);
            return Convert.ToDateTime(apiDate);
        }

        public static string GetAPIVersion()
        {
            string apiVersion = "", apiDate = "";
            ResipAPIMethods.apiVersion(ref apiVersion, ref apiDate);
            return apiVersion;
        }

        public static string GetdataVersion()
        {
            string dataVersion = "";
            Int32 ret = ResipAPIMethods.dataVersion(2, ref dataVersion);
            return dataVersion;
        }

        /*public static Type_Rdc[] GetRdc(string dmdCode)
        {
                //System.Array rdc = new System.Array[100];
                byte[,] rdc = new byte[100, 10];
                IntPtr pnt = IntPtr.Zero;
                Type_Rdc[] codes = null;
                Type_Rdc r = new Type_Rdc();
                Int32 ret = ResipAPIMethods.productRDC(dmdCode, out rdc);
                try
                 {
                    if (ret == 0 || rdc == null)
                    {
                        return null;
                    }
                    pnt = Marshal.AllocHGlobal(3);
                    //Marshal.Copy(rdc[0], 0, pnt, 3);
                    r = (Type_Rdc)Marshal.PtrToStructure(pnt, typeof(Type_Rdc));
                    // process data...
                }
                finally
                {
                    Marshal.FreeHGlobal(pnt);
                }
                return codes;
        }*/

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct Type_Rdc
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            string code;
            Int32 flagMain;
            string label;
        }

        /*public static TypeFlags? GetFlags(string dmdCode)
        {
            TypeFlags r = new TypeFlags();
            Int32 ret = ResipAPIMethods.productFlags(dmdCode, ref r);
            return r;
        }*/

        public static TypeFlags? GetFlags(System.Data.DataRow data)
        {
            TypeFlags r = new TypeFlags();
            if (data != null)
            {
                r.CountrySpecificitiesEngland = Convert.ToInt32(data["COUNTRY_SPECIFICITIES_ENGLAND"]);
                r.CountrySpecificitiesWales = Convert.ToInt32(data["COUNTRY_SPECIFICITIES_WALES"]);
                r.CountrySpecificitiesNireland = Convert.ToInt32(data["COUNTRY_SPECIFICITIES_NIRELAND"]);
                r.CountrySpecificitiesScotland = Convert.ToInt32(data["COUNTRY_SPECIFICITIES_SCOTLAND"]);
                r.IndependentNursePrescriber = Convert.ToInt32(data["PRESCRIBING_ROLE"]);
                r.CommunityNursePrescriber = Convert.ToInt32(data["COMMUNITY_NURSE_PRESCRIBER"]);
                r.ScottishNurseFormulary = Convert.ToInt32(data["SCOTTISH_NURSE_FORMULARY"]);
                r.Toxic = Convert.ToInt32(data["TOXIQUE"]);
                r.LifeState = Convert.ToInt32(data["LIFE_STATE"]);
                r.BlackTriangle = Convert.ToInt32(data["BLACK_TRIANGLE"]);
                r.Extemp = Convert.ToInt32(data["EXTEMPORANEOUS"]);
                r.UseBrand = Convert.ToInt32(data["USE_BRAND"]);
                r.SlsEngland = Convert.ToInt32(data["SLS_ENGLAND"]);
                r.SlsWales = Convert.ToInt32(data["SLS_WALES"]);
                r.SlsScotland = Convert.ToInt32(data["SLS_SCOTLAND"]);
                r.SlsNireland = Convert.ToInt32(data["SLS_NIRELAND"]);
                r.AcbsEngland = Convert.ToInt32(data["ACBS_ENGLAND"]);
                r.AcbsWales = Convert.ToInt32(data["ACBS_WALES"]);
                r.AcbsScotland = Convert.ToInt32(data["ACBS_SCOTLAND"]);
                r.AcbsNireland = Convert.ToInt32(data["ACBS_NIRELAND"]);
            }
            return r;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct TypeFlags
        {
            private Int32 countrySpecificitiesEngland;
            private Int32 countrySpecificitiesWales;
            private Int32 countrySpecificitiesScotland;
            private Int32 countrySpecificitiesNireland;
            private Int32 independentNursePrescriber;
            private Int32 communityNursePrescriber;
            private Int32 scottishNurseFormulary;
            private Int32 appliances;
            private Int32 abcs;
            private Int32 brand;
            private Int32 useBrand;
            private Int32 useGeneric;
            private Int32 blackListDrugEnglandWales;
            private Int32 blackListDrugScotland;
            private Int32 blackListDrugNireland;
            private Int32 homeopathicProduct;
            private Int32 toxic;
            private Int32 lifeState;
            private Int32 blackTriangle;
            private Int32 extemp;
            private Int32 slsEngland;
            private Int32 slsWales;
            private Int32 slsScotland;
            private Int32 slsNireland;
            private Int32 acbsEngland;
            private Int32 acbsWales;
            private Int32 acbsScotland;
            private Int32 acbsNireland;

            public Int32 CountrySpecificitiesEngland
            {
                get { return countrySpecificitiesEngland; }
                set { countrySpecificitiesEngland = value; }
            }

            public Int32 CountrySpecificitiesWales
            {
                get { return countrySpecificitiesWales; }
                set { countrySpecificitiesWales = value; }
            }
           
            public Int32 CountrySpecificitiesScotland
            {
                get { return countrySpecificitiesScotland; }
                set { countrySpecificitiesScotland = value; }
            }
            
            public Int32 CountrySpecificitiesNireland
            {
                get { return countrySpecificitiesNireland; }
                set { countrySpecificitiesNireland = value; }
            }

            public Int32 IndependentNursePrescriber
            {
                get { return independentNursePrescriber; }
                set { independentNursePrescriber = value; }
            }

            public Int32 CommunityNursePrescriber
            {
                get { return communityNursePrescriber; }
                set { communityNursePrescriber = value; }
            }

            public Int32 ScottishNurseFormulary
            {
                get { return scottishNurseFormulary; }
                set { scottishNurseFormulary = value; }
            }

            public Int32 Appliances
            {
                get { return appliances; }
                set { appliances = value; }
            }

            public Int32 Abcs
            {
                get { return abcs; }
                set { abcs = value; }
            } 

            public Int32 Brand
            {
                get { return brand; }
                set { brand = value; }
            } 

            public Int32 UseBrand
            {
                get { return useBrand; }
                set { useBrand = value; }
            } 

            public Int32 UseGeneric
            {
                get { return useGeneric; }
                set { useGeneric = value; }
            } 

            public Int32 BlackListDrugEnglandWales
            {
                get { return blackListDrugEnglandWales; }
                set { blackListDrugEnglandWales = value; }
            } 

            public Int32 BlackListDrugScotland
            {
                get { return blackListDrugScotland; }
                set { blackListDrugScotland = value; }
            } 

            public Int32 BlackListDrugNireland
            {
                get { return blackListDrugNireland; }
                set { blackListDrugNireland = value; }
            }

            public Int32 SlsEngland
            {
                get { return slsEngland; }
                set { slsEngland = value; }
            }

            public Int32 SlsWales
            {
                get { return slsWales; }
                set { slsWales= value; }
            }

            public Int32 SlsScotland
            {
                get { return slsScotland; }
                set { slsScotland = value; }
            }

            public Int32 SlsNireland
            {
                get { return slsNireland; }
                set { slsNireland = value; }
            }

            public Int32 AcbsEngland
            {
                get { return acbsEngland; }
                set { acbsEngland = value; }
            }

            public Int32 AcbsWales
            {
                get { return acbsWales; }
                set { acbsWales = value; }
            }

            public Int32 AcbsScotland
            {
                get { return acbsScotland; }
                set { acbsScotland = value; }
            }

            public Int32 AcbsNireland
            {
                get { return acbsNireland; }
                set { acbsNireland = value; }
            } 

            public Int32 HomeopathicProduct
            {
                get { return homeopathicProduct; }
                set { homeopathicProduct = value; }
            } 

            public Int32 Toxic
            {
                get { return toxic; }
                set { toxic = value; }
            }

            public Int32 LifeState
            {
                get { return lifeState; }
                set { lifeState = value; }
            }

            public Int32 BlackTriangle
            {
                get { return blackTriangle; }
                set { blackTriangle = value; }
            }

            public Int32 Extemp
            {
                get { return extemp; }
                set { extemp = value; }
            }
        }

        /*public static TypeControlStatus? GetControlStatus(string dmdCode)
        {
            //System.Array rdc = new System.Array[100];
            byte[,] rdc = new byte[100, 10];
            IntPtr pnt = IntPtr.Zero;
            TypeControlStatus[] codes = null;
            TypeControlStatus r = new TypeControlStatus();
            Int32 ret = ResipAPIMethods.GetProductsControlStatus(dmdCode, out rdc);
            try
            {
                if (ret == 0 || rdc == null)
                {
                    return null;
                }
                pnt = Marshal.AllocHGlobal(3);
                //Marshal.Copy(rdc[0], 0, pnt, 3);
                r = (TypeControlStatus)Marshal.PtrToStructure(pnt, typeof(TypeControlStatus));
                // process data...
            }
            finally
            {
                Marshal.FreeHGlobal(pnt);
            }
            return codes[0];
        }*/

        public static TypeControlStatus? GetControlStatus(DataTable table)
        {
            TypeControlStatus controlStatus = new TypeControlStatus();
            if (table == null) return controlStatus;
            foreach (DataRow row in table.Rows)
            {
                switch (row["type_data"].ToString())
                {
                    case "2": controlStatus.ContraIndication = Convert.ToInt32(row["type_data_etat"]); break;
                    case "3": controlStatus.Precautions = Convert.ToInt32(row["type_data_etat"]); break;
                    case "5": controlStatus.Ingredients = Convert.ToInt32(row["type_data_etat"]); break;
                    case "12": controlStatus.RDC = Convert.ToInt32(row["type_data_etat"]); break;
                    case "14": controlStatus.PrescriberWarnings = Convert.ToInt32(row["type_data_etat"]); break;
                }
            }
            return controlStatus;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct TypeControlStatus
        {
             long generalState;
             long indication;
             long contraIndication;     //CONTRAINDICATION
             long precautions;     //PRECAUTION
             long effetIndesirable;
             long ingredients;         //INGREDIENTS
             long identifiant;
             long toxique;
             long posologie;
             long warnings;
             long rdc;                  //RDC
             long prescriberWarnings;              //PRESCRIBER WARNINGS
             long read_code;
             long sources_infos;
             long flags;
             long prevent_back_population;

             public long ContraIndication
             {
                 get { return contraIndication; }
                 set { contraIndication = value; }
             }
             public long Precautions
             {
                 get { return precautions; }
                 set { precautions = value; }
             }
             public long Ingredients
             {
                 get { return ingredients; }
                 set { ingredients = value; }
             }
             public long RDC
             {
                 get { return rdc; }
                 set { rdc = value; }
             }
             public long PrescriberWarnings
             {
                 get { return prescriberWarnings; }
                 set { prescriberWarnings = value; }
             }
        }

        public static SortedList<Int32, TypeSpecificDosage> GetSpecificDosage(DataTable data)
        {
            SortedList<Int32, TypeSpecificDosage> dosages = new SortedList<Int32, TypeSpecificDosage>();
            foreach (DataRow row in data.Rows)
            {
                TypeSpecificDosage d = new TypeSpecificDosage();
                d.ProductID = Convert.ToString(row["ID_PRODUCT"]);
                d.DosageNumber = Convert.ToInt32(row["DOSAGE_NUMBER"]);
                d.Description = Convert.ToString(row["DESCRIPTION"]);
                d.ByDefault = Convert.ToInt32(row["BY_DEFAULT"]);
                dosages.Add(d.DosageNumber, d);
            }
            return dosages;
        }

        public struct TypeSpecificDosage
        {
            private string productID;
            private Int32 dosageNumber;
            private string description;
            private Int32 byDefault;

            public string ProductID
            {
                get { return productID; }
                set { productID = value; }
            }
            public Int32 DosageNumber
            {
                get { return dosageNumber; }
                set { dosageNumber = value; }
            }
            public string Description
            {
                get { return description; }
                set { description = value; }
            }
            public Int32 ByDefault
            {
                get { return byDefault; }
                set { byDefault = value; }
            }
        }

        /*public static TypePosology? GetPosology(string dmdCode)
        {
            TypePosology p = new TypePosology();
            Int32 ret = ResipAPIMethods.posology(dmdCode, ref p);
            return p;
        }*/

        public static TypePosology? GetPosology(System.Data.DataRow data)
        {
            TypePosology r = new TypePosology();
            r.DosageAdvice = data["POSOLOGIE"] == System.DBNull.Value ? " " : Convert.ToString(data["POSOLOGIE"]);
            r.Dose = data["PRESCRIPTION_DOSAGE"] == System.DBNull.Value ? " " : Convert.ToString(data["PRESCRIPTION_DOSAGE"]);
            r.Period = data["DAYS_SUPPLY"] == System.DBNull.Value ? 0 : Convert.ToInt32(data["DAYS_SUPPLY"]);
            r.DayQuantity = data["EXPECTED_QUANTITY_PER_DAY"] == System.DBNull.Value ? 0f : (float)Convert.ToDouble(data["EXPECTED_QUANTITY_PER_DAY"]);
            r.SuppliedQuantity = data["ACTUAL_TOTAL_QUANTITY"] == System.DBNull.Value ? 0f : (float)Convert.ToDouble(data["ACTUAL_TOTAL_QUANTITY"]);
            r.Unit = data["QUANTITY_UNIT"] == System.DBNull.Value ? 0 : Convert.ToInt64(data["QUANTITY_UNIT"]);
            return r;
        }

        public struct TypePosology
        {
            private string dosage;
            private string dose;
            private Int32 dailySupply;
            private float quantityPerDay;
            private float actualTotalQuantity;
            private Int64 quantityUnit;

            public float SuppliedQuantity 
            {
                get { return actualTotalQuantity; }
                set { actualTotalQuantity = value; }
            }

            public float DayQuantity
            {
                get { return quantityPerDay; }
                set { quantityPerDay = value; }
            }

            public Int32 Period
            {
                get { return dailySupply; }
                set { dailySupply = value; }
            }

            public Int64 Unit
            {
                get { return quantityUnit; }
                set { quantityUnit = value; }
            }

            public string Dose
            {
                get { return dose; }
                set { dose = value; }
            }

            public string DosageAdvice
            {
                get { return dosage; }
                set { dosage = value; }
            }
        }

        /*public static TypeCaution? GetCautions(string dmdCode)
        {
            TypeCaution p = new TypeCaution();
            Int32 ret = ResipAPIMethods.labelWarning(dmdCode, ref p);
            return p;
        }*/

        public static string[] GetCautions(System.Data.DataTable data)
        {
            string[] cautions = new string[data.Rows.Count];
            int i = 0;
            foreach (System.Data.DataRow row in data.Rows)
            {
                cautions[i] = row["description"].ToString();;
                i++;
            }
            return cautions;
        }

        public struct TypeCaution
        {
            //private int id;
            private string warning;

            public string Warning
            {
                get { return warning; }
                set { warning = value; }
            }
        }

        public static TypeRoute[] GetRoutes(System.Data.DataTable data)
        {
            TypeRoute[] routes = new TypeRoute[data.Rows.Count];
            int i = 0;
            foreach (System.Data.DataRow row in data.Rows)
            {
                TypeRoute route = new TypeRoute();
                route.Id = row["route_code"].ToString();
                route.Label = row["description"].ToString();
                routes[i] = route;
                i++;
            }
            return routes;
        }

        public struct TypeRoute
        {
            private string _id;
            private string _label;

            public string Id
            {
                get { return _id; }
                set { _id = value; }
            }

            public string Label
            {
                get { return _label; }
                set { _label = value; }
            }
        }
    }
}