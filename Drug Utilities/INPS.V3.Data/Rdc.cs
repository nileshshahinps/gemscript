﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace INPS.V3.Data
{
    public struct RdcType
    {
        private UInt32 _code;
        private bool _main;

        public RdcType(UInt32 code, bool main)
        {
            _code = code;
            _main = main;
        }

        public UInt32 Code
        {
            get
            {
                return _code;
            }
        }

        public bool Main
        {
            get
            {
                return _main;
            }
        }
    }
    static public class Rdc
    {
        static public ConceptDictionary.CommonConcepts GetRdcCategory(uint rdcCode) //todo confirm this mapping
        {
            ConceptDictionary.CommonConcepts category = ConceptDictionary.CommonConcepts.Medication;
            uint level1 = rdcCode / 1000000;
            uint level2 = rdcCode / 10000;
            if (level1 >= 70)
            {
                if (level2 == 7154)
                {
                    category = ConceptDictionary.CommonConcepts.Reagent;
                }
                else
                {
                    category = ConceptDictionary.CommonConcepts.Appliance;
                }
            }
            else if (level2 == 306)
            {
                category = ConceptDictionary.CommonConcepts.Oxygen;
            }

            return category;
        }

        static public bool IsContraception(List<RdcType> rdcCodes)
        {
            foreach (RdcType code in rdcCodes)
                if (code.Code / 10000 == 703)
                    return true;
            return false;
        }

        static public bool IsHomeopathic(List<RdcType> rdcCodes)
        {
            foreach (RdcType code in rdcCodes)
                if (code.Code / 10000 == 5101)
                    return true;
            return false;
        }

        static public bool IsCytotoxic(List<RdcType> rdcCodes)
        {
            foreach (RdcType code in rdcCodes)
            {
                if (code.Code / 10000 == 801)
                    return true;
                if (code.Code / 100 == 40653)
                    return true;
            }
            return false;
        }

        static public bool IsOxygen(List<RdcType> rdcCodes) 
        {
            foreach (RdcType code in rdcCodes)
            {
                if (GetRdcCategory(code.Code) == ConceptDictionary.CommonConcepts.Oxygen)
                    return true;
            }
            return false;
        }
    }
}
