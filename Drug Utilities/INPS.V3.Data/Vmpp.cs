﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace INPS.V3.Data
{
    public class VmppDetails : DmdProductDetails
    {
        private VmpDetails _parentVmp;
        private Concept _UOM;
        private List<AmppDetails> _ampps;
        private DataRowReader _drugTariffRow;
        private Concept _legalStatus;

        private bool? _dohApproved;
        private bool? _isInvalid = null;
        private bool? _isDiscontinued = null;
        private bool? _isAvailable= null;

        public VmppDetails(DataRow source, VmpDetails parentVmp)
            : base(source)
        {
            _parentVmp = parentVmp;
        }

        public override VmpDetails Vmp
        {
            get { return _parentVmp; }
        }

        public List<AmppDetails> Ampps
        {
            get
            {
                if (_ampps == null)
                {
                    // Find all ampps with this vmpp as a parent
                    _ampps = new List<AmppDetails>();
                    foreach (AmpDetails amp in Vmp.Amps)
                    {
                        foreach (AmppDetails ampp in amp.Ampps)
                        {
                            if (ampp.Vmpp == this)
                            {
                                _ampps.Add(ampp);
                            }
                        }
                    }
                }
                return _ampps;
            }
        }

        public override UInt64 PrevDmdCode
        {
            get { return 0; }
        }

        public override string Name
        {
            get { return ProductRow.GetString("full_name"); }
        }

        public double Size
        {
            get { return ProductRow.GetDouble("quantity_value"); }
        }


        /// <summary>
        /// True if all AMPPs are also discontinued
        /// </summary>
        public override bool IsDiscontinued
        {
            get 
            { 
                if (_isDiscontinued == null)
                    _isDiscontinued = Ampps.All(ampp => ampp.IsDiscontinued || ampp.IsInvalid != IsInvalid);
                return (bool)_isDiscontinued;
            }
        }

        public override bool IsFp10MDA
        {
            get { return Ampps.Any(ampp => ampp.IsFp10MDA && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsDohApproved
        {
            get
            {
                if (!_dohApproved.HasValue)
                {
                    _dohApproved = DrugTariffRow.HasData;
                }
                return _dohApproved.Value;
            }
        }

        public override bool IsAcbs
        {
            get { return Ampps.Any(ampp => ampp.IsAcbs && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsSls
        {
            get { return Ampps.Any(ampp => ampp.IsSls && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsBlacklisted
        {
            get { return Ampps.All(ampp => ampp.IsBlacklisted || ampp.IsInvalid != IsInvalid || ampp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsBlackTriangle
        {
            get { return Ampps.Any(ampp => ampp.IsBlackTriangle && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsPa
        {
            get { return Ampps.Any(ampp => ampp.IsPa && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsNurseFormulary
        {
            get { return Ampps.Any(ampp => ampp.IsNurseFormulary && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsSpecial
        {
            get { return Ampps.All(ampp => ampp.IsSpecial || ampp.IsInvalid != IsInvalid || ampp.IsDiscontinued != IsDiscontinued); }
        }

        public bool IsMarkedInvalid
        {
            get { return ProductRow.GetBoolean("invalid"); }
        }

        public override bool IsInvalid
        {
            get 
            { 
                if (_isInvalid == null)
                    _isInvalid = Ampps.All(ampp => ampp.IsInvalid /*|| ampp.IsDiscontinued != IsDiscontinued*/);
                return (bool)_isInvalid;
            }
        }

        public override bool IsActive
        {
            //don't check whther amp is active, as may noe vbe active but atill be available as a special - in such case active ampp will only appear when special selected in Vision
            get { return !IsInvalid && (IsAvailable); }
        }

        public bool IsComponentOnlyPack
        {
            get { return ProductRow.GetInt("combination_pack") == 2; }
        }

        public bool IsDivisible
        {
            get { return Ampps.Any(ampp => ampp.IsDivisible && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsParallelImport
        {
            get { return Vmp.IsParallelImport; }
        }

        public override bool IsExtemp
        {
            get { return Vmp.IsExtemp; }
        }

        public override bool IsImported
        {
            get { return Vmp.IsImported; }
        }

        public override bool IsSecondaryCare
        {
            get { return Ampps.All(ampp => ampp.IsSecondaryCare || ampp.IsInvalid != IsInvalid || ampp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsReimbursableDevice
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDevice && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceEngland
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceEngland && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceWales
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceWales && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceNI
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceNI && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceScotland
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceScotland && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsMedicine
        {
            get { return Ampps.Any(ampp => ampp.IsMedicine && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsUnlicensed
        {
            get { return Ampps.Any(ampp => ampp.IsUnlicensed && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsAvailable
        {
            get 
            { 
                if (_isAvailable == null)
                    _isAvailable = !IsComponentOnlyPack;
                return (bool)_isAvailable;
            }
        }

        public override bool IsScottishNurseFormulary
        {
            get { return Vmp.IsScottishNurseFormulary; }
        }

        public override bool IsReconciledGemscriptProduct
        {
            //returns true if product is reconciled
            get
            {
                return Ampps.All(ampp => ampp.IsReconciledGemscriptProduct);
            }
        }

        public override bool UseGeneric
        {
            get { return Vmp.UseGeneric; }
        }

        public override bool UseBrand
        {
            get { return Vmp.UseBrand; }
        }

        public int Price
        {
            get 
            {
                int vmppPrice = DrugTariffRow.GetInt("price");
                if (vmppPrice != 0) return vmppPrice;
                if (Ampps.Count == 0) return 0;
                int minPrice = 0; //Ampps.First<AmppDetails>().Price;
                foreach (AmppDetails ampp in Ampps)
                    if (ampp.IsDiscontinued  == IsDiscontinued && ampp.IsInvalid == IsInvalid && ampp.Price != 0 && (minPrice > ampp.Price || minPrice == 0)) minPrice = ampp.Price;
                return minPrice; 
            }
        }

        public int MaxPrice
        {
            get
            {
                int vmppPrice = DrugTariffRow.GetInt("price");
                if (vmppPrice != 0) return vmppPrice;
                if (Ampps.Count == 0) return 0;
                int maxPrice = 0;
                foreach (AmppDetails ampp in Ampps)
                    if (ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid && maxPrice < ampp.Price) maxPrice = ampp.Price;
                return maxPrice;
            }
        }

        public Concept LegalStatus
        {
            get
            {
                if (_legalStatus == null)
                {
                    SortedSet<string> legalCats = new SortedSet<string>();
                    foreach (AmppDetails ampp in Ampps)
                    {
                        if (ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid && !legalCats.Contains(ampp.LegalStatus.Description))
                            legalCats.Add(ampp.LegalStatus.Description);
                    }
                    if (legalCats.Count > 1)
                    {
                        legalCats.Remove("Not Applicable");
                        StringBuilder sBuilder = new StringBuilder();
                        foreach (string s in legalCats) sBuilder.Append(s + ", ");
                        _legalStatus = Concepts.CreateInpsConcept(ConceptDictionary.CommonConcepts.LegalStatus, sBuilder.ToString().TrimEnd(' ', ','));
                    }
                    else if (legalCats.Count == 1) _legalStatus = Concepts.CreateInpsConcept(ConceptDictionary.CommonConcepts.LegalStatus, legalCats.First<string>());
                }
                return _legalStatus;
            }
        }

        public Concept UOM
        {
            get
            {
                GenerateConcept(ref _UOM, "quantity_unit", ConceptDictionary.CommonConcepts.Unit);
                return _UOM;
            }
        }

        protected DataRowReader DrugTariffRow
        {
            get
            {
                ReadRelatedRow(ref _drugTariffRow, String.Format(@"select * from bcb_inps.vmpp_drug_tariff where vmpp_id = {0}", DmdCode));
                return _drugTariffRow;
            }
        }

    }
}
