﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Xml.Linq;
using INPS.V3.Resip;

namespace INPS.V3.Data
{
    public struct TypeIngredient
    {
        private Concept _ingredient;

        public Concept Ingredient
        {
            get { return _ingredient; }
            set { _ingredient = value; }
        }
        private Double _strength;

        public Double Strength
        {
            get { return _strength; }
            set { _strength = value; }
        }
        private Concept _unit;

        public Concept Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }
    }

    /// <summary>
    /// Base class for VMP, AMP, VMPP and AMPP
    /// </summary>
    public abstract class DmdProductDetails
    {
        protected static DateTime DISCONTINUED_CUTOFF_DATE= Convert.ToDateTime("01/06/2011");

        private static Dictionary<string, string> _formMap;
        private static ConceptDictionary _concepts = new ConceptDictionary();
        private static BcbConnection _bcbConnection = new BcbConnection();
        private static BcbDataConnection _bcbDataConnection = new BcbDataConnection();
        private static LexDrugConnection _lexConnection = new LexDrugConnection();
        private static InpsDrugConnection _inpsDrugConnection = new InpsDrugConnection();
        private DataRowReader _productRow;
        private DataRowReader _reconciledDmdProductRow;
        private DataRowReader _reconciledAuthoredGemscriptRow;

        private Boolean? _england = null;
        private Boolean? _ni = null;
        private Boolean? _scotland = null;
        private Boolean? _wales = null;

        //used to indicate where an inactive drug is added with the same code as an active drug to deal with cases where a Mulilex drug has previously been prescribed although not available in that area
        private Boolean _isCountryViewDuplicate = false;

        public Boolean IsCountryViewDuplicate
        {
            get { return _isCountryViewDuplicate; }
            set { _isCountryViewDuplicate = value; }
        }

        private ResipData _rData;

        private bool? _isAcbs;

        private uint productCode = 0;
        //used in handling of the situation where more than 1 mlex drug maps to a single dm+d drug
        private bool _mlex2DmdDuplicate = false;

        public bool IsMlex2DmdDuplicate
        {
            get { return _mlex2DmdDuplicate; }
            set { _mlex2DmdDuplicate = value; }
        }

        protected DmdProductDetails(DataRow source)
        {
            _productRow = new DataRowReader(source); 
            if (_formMap == null)
            {
                #region Existing Lex Forms
                // "aerosol",
                //"capsule", 
                //"cream",    
                //"crystals",  
                //"drops",      
                //"elixir",      
                //"gel",          
                //"granules",      
                //"ice",            
                //"infusion",        
                //"inhaler",          
                //"injection",         
                //"kit",                
                //"linctus",             
                //"liquid",               
                //"lotion",                
                //"lozenge",                
                //"mixture",                 
                //"oil",                      
                //"ointment",                  
                //"pad",                        
                //"paint",                       
                //"paste",                        
                //"patch",                         
                //"pen",                            
                //"pessary",                         
                //"powder",                           
                //"sachet",                            
                //"shampoo",                            
                //"solution",                            
                //"spansule",                             
                //"spirit",                                
                //"spray",                                  
                //"suppository",                              
                //"suspension",                               
                //"syrup",                                     
                //"tablet",                                     
                //"tincture",                                    
                //"vaccine",                                      
                //"wax",
                #endregion
                _formMap = new Dictionary<string,string>();
                _formMap.Add("not applicable", "n/a");
                _formMap.Add("dose", "inh"); // todo check all these mappings
            }
        }

        protected DataRowReader ProductRow
        {
            get {return _productRow;}
        }

        protected DataRowReader ReconciledDmdProductRow
        {
            get
            {
                if (_reconciledDmdProductRow == null)
                {
                    string command = String.Format(@"SELECT * FROM products_reconciliations WHERE id_product = {0}", DmdCode);
                    _reconciledDmdProductRow = new DataRowReader(BcbDataConnection.GetFirstRow(command));
                }
                return _reconciledDmdProductRow;
            }
        }

        protected DataRowReader ReconciledAuthoredGemscriptRow
        {
            get
            {
                if (_reconciledAuthoredGemscriptRow == null)
                {
                    string command = String.Format(@"SELECT * FROM products_reconciliations WHERE id_reconciled_product = {0}", DmdCode);
                    _reconciledAuthoredGemscriptRow = new DataRowReader(BcbDataConnection.GetFirstRow(command));
                }
                return _reconciledAuthoredGemscriptRow;
            }
        }

        public Dictionary<UInt32, string> AlternativeDrugs
        {
            get
            {
                //returns list of all LexIDs for drug - name string required to ascertain best match
                Dictionary<UInt32, string> allIds = null;
                string command = String.Format(@"select * from historical_drug.historical_drug where drug_code={0}", VisionCode);
                DataTable res = LexDrugConnection.GetTable(command);
                if (res != null && res.Rows.Count > 0)
                {
                    allIds = new Dictionary<UInt32, string>();
                    foreach (DataRow row in res.Rows) allIds.Add(Convert.ToUInt32(row["drugcode"]), row["name"].ToString());
                }
                return allIds;
            }
        }

        public ResipData RData
        {
            get
            {
                if (_rData == null)
                    _rData = new ResipData(this);
                return _rData;
            }
        }

        static public ConceptDictionary Concepts
        {
            get { return _concepts; }
        }

        protected BcbConnection BcbConnection
        {
            get {
                if (!_bcbConnection.Valid) _bcbConnection.Connect();
                return _bcbConnection; 
            }
        }

        protected BcbDataConnection BcbDataConnection
        {
            get { if (!_bcbDataConnection.Valid) _bcbDataConnection.Connect(); 
                return _bcbDataConnection; }
        }

        protected LexDrugConnection LexDrugConnection
        {
            get
            {
                if (!_lexConnection.Valid) _lexConnection.Connect();
                return _lexConnection;
            }
        }

        protected InpsDrugConnection InpsDrugConnection
        {
            get
            {
                if (!_inpsDrugConnection.Valid) _inpsDrugConnection.Connect();
                return _inpsDrugConnection;
            }
        }

        public UInt64 DmdCode
        {
            get { return ProductRow.GetConceptId("ID"); }
        }

        public UInt64 CombinationProduct
        {
            get { return ProductRow.GetConceptId("ID"); }
        }

        public uint VisionCode
        {
            
            get { return productCode; }
            set { productCode = value; }
        }

        public abstract UInt64 PrevDmdCode
        {
            get;
        }

        public abstract string Name
        {
            get;
        }

        public virtual Concept Form
        {
            get { return Vmp.Form; }
        }

        public string ReadCode
        {
            get {
                return RData.ReadCode == null || RData.ReadCode == "" ? RData.ReadCode : RData.ReadCode.PadRight(7, '0');
            }
        }

        public virtual int ScheduleNumber
        {
            get { return Vmp.ScheduleNumber; }
        }

        public abstract VmpDetails Vmp
        {
            get;
        }

        public virtual Concept Vtm
        {
            get { return Vmp.Vtm; }
        }

        public string KeyForm
        {
            get
            {
                string keyForm = "";
                Concept form = Form;
                if (form != null)
                {
                    string formText = form.Description.ToLower();
                    _formMap.TryGetValue(formText, out keyForm);
                    if (keyForm == null)
                    {
                        // No explicit mapping
                        // Use the first three characters of the last word
                        formText = formText.Split().Last();
                        keyForm = formText.PadRight(3).Substring(0, 3);
                    }
                }
                return keyForm;
            }
        }

        public virtual Concept Manufacturer
        {
            get { return null; }
        }

        public virtual Concept Flavour
        {
            get { return null; }
        }

        public virtual Concept Strength
        {
            get { return Vmp.Strength; }
        }

        public virtual float NormalisedStrength
        {
            get { return Vmp.NormalisedStrength; }
        }

        public Concept HighRiskWarning
        {
            get
            {
                //if (_highRiskWarning == null && Vtm != null && Vtm.Id == 68887009) //todo temp code
                if (RData.Flags == null || RData.Flags.Value.Toxic == 0) return null;
                if (RData.Warning == "" && RData.Flags.Value.Toxic == 1)
                {
                    return GenerateConcept(new Concept("This product is high risk. Details of risk unavailable"), ConceptDictionary.CommonConcepts.HighRiskWarning);
                }
                else
                {
                    return GenerateConcept(new Concept(RData.Warning), ConceptDictionary.CommonConcepts.HighRiskWarning);
                }
            }
        }

        /*private bool ReadRdcCodes()
        {
            if (_rdcCodes == null)
            {
                _rdcCodes = new List<RdcType>();
                string command = String.Format(@"select * from products_rdc_codes where id_product = {0}", DmdCode.ToString());
                DataTable results = BcbDataConnection.GetTable(command);
                foreach (DataRow row in results.Rows)
                {
                    _rdcCodes.Add(new RdcType(Convert.ToUInt32(row["id_rdc"]), Convert.ToBoolean(row["main_rdc"])));
                }
            }
            return _rdcCodes.Count > 0;
        }*/

        public virtual float StrengthValue
        {
            get { return Vmp.StrengthValue; }
        }

        public virtual string SearchKey
        {
            get
            {
                if (Vtm != null && Name.Length > 0)
                {
                    // Components of VTM may not be in the same order as the components of the VMP.
                    // Ensure that the first word of the VTM matches the first word of the VMP
                    bool vtmWordMatch = false;
                    StringBuilder result = new StringBuilder();
                    string[] vtmWords = Vtm.Description.ToUpper().Split('+');
                    string[] vmpWords = Name.ToUpper().Split(' ').SkipWhile(element => element.Equals("GENERIC")).ToArray();
                    string separator = vtmWords.Length > 1 ? " / " : "";
                    
                    foreach (string vtmWord in vtmWords)
                    {
                        string vtmTrimmedWord = vtmWord.Trim();
                        if (vtmTrimmedWord.StartsWith(vmpWords[0])) 
                        {
                            result.Insert(0, vtmTrimmedWord);
                            vtmWordMatch = true;
                        }
                        else
                        {
                            result.Append(separator);
                            result.Append(vtmTrimmedWord);
                        }
                    }

                    if (vtmWordMatch)
                        return result.ToString();
                }
                if (Name.ToUpper().StartsWith("GENERIC ")) 
                    return Name.ToUpper().Substring(8);
                return Name.ToUpper();
            }
        }

        public abstract bool IsInvalid
        {
            get;
        }

        public abstract bool IsActive
        {
            get;
        }

        public virtual bool IsBrandedGeneric
        {
            get { return true; }
        }

        public abstract bool UseBrand
        {
            get;
        }

         public abstract bool UseGeneric
        {
            get;
        }

        public virtual bool England
        {
            get 
            {
                if (_england != null) return (Boolean)_england;
                return IsFp10England == IsFp10 && IsAcbsEngland == IsAcbs; 
            }
            set {_england = value;}
        }

        public virtual bool Wales
        {
            get 
            {
                if (_wales != null) return (Boolean)_wales;
                return IsFp10Wales == IsFp10 && IsAcbsWales == IsAcbs; 
            }
            set { _wales = value; }
        }

        public virtual bool Scotland
        {
            get 
            {
                if (_scotland != null) return (Boolean)_scotland;
                return IsFp10Scotland == IsFp10 && IsAcbsScotland == IsAcbs; 
            }
            set { _scotland = value; }
        }

        public virtual bool Ni
        {
            get 
            {
                if (_ni != null) return (Boolean)_ni;
                return IsFp10NI == IsFp10 && IsAcbsNI == IsAcbs;
            }
            set { _ni = value; }
        }

        public virtual bool IsAuthoredDiscontinued
        {
            get { return (RData.Flags.Value.LifeState == 3); }
        }

        public abstract bool IsDiscontinued
        {
            get;
        }

        public virtual bool IsFp10
        {
            get { return Vmp.IsFp10; }
            set { ; }
        }

        public virtual bool IsFp10England
        {
            get { return Vmp.IsFp10England; }
        }

        public virtual bool IsFp10Wales
        {
            get { return Vmp.IsFp10Wales; }
        }

        public virtual bool IsFp10Scotland
        {
            get { return Vmp.IsFp10Scotland; }
        }

        public virtual bool IsFp10NI
        {
            get { return Vmp.IsFp10NI; }
        }

        public abstract bool IsFp10MDA
        {
            get;
        }

        public abstract bool IsDohApproved
        {
            get;
        }

        public virtual bool IsControlled
        {
            get { return Vmp.IsControlled; }
        }

        public virtual bool IsAcbs
        {
            get
            {
                if (_isAcbs != null) return (Boolean)_isAcbs;
                return IsAcbsEngland;
            }
            set { _isAcbs = value; }
        }

        public virtual bool IsAcbsEngland
        {
            get { return RData.Flags.Value.AcbsEngland == 1; }
        }

        public virtual bool IsAcbsWales
        {
            get { return RData.Flags.Value.AcbsWales == 1; }
        }

        public virtual bool IsAcbsScotland
        {
            get { return RData.Flags.Value.AcbsScotland == 1; }
        }

        public virtual bool IsAcbsNI
        {
            get { return RData.Flags.Value.AcbsNireland == 1; }
        }

        public abstract bool IsSls
        {
            get;
        }

        public abstract bool IsBlacklisted
        {
            get;
        }

        public abstract bool IsBlackTriangle
        {
            get;
        }

        public abstract bool IsPa
        {
            get;
        }

        public abstract bool IsNurseFormulary
        {
            get;
        }

        public virtual bool IsScottishNurseFormulary
        {
            get { return (RData.Flags.Value.ScottishNurseFormulary == 1); }
        }

        public virtual bool IsIndependentPrescriberFormulary
        {
            get { return !(ScheduleNumber >= 1 && ScheduleNumber <= 5 && RData.Flags != null && !(RData.Flags.Value.IndependentNursePrescriber == 1)); }
        }

        public abstract bool IsSpecial
        {
            get;
        }
        
        public virtual bool IsContraception
        {
            get { return Rdc.IsContraception(RData.RdcCodes); }
        }

        public virtual bool IsCytotoxic
        {
            get { return Rdc.IsCytotoxic(RData.RdcCodes); }
        }

        public virtual bool IsHandwritten
        {
            get { return IsDiscontinued || !IsFp10 || Rdc.IsOxygen(RData.RdcCodes); }
        }

        public abstract bool IsParallelImport
        {
            get;
        }

        public abstract bool IsExtemp
        {
            get;
        }

        public abstract bool IsImported
        {
            get;
        }

        public abstract bool IsSecondaryCare
        {
            get;
        }

        public abstract bool IsReimbursableDevice
        {
            get;
        }

        public abstract bool IsReimbursableDeviceEngland
        {
            get;
        }

        public abstract bool IsReimbursableDeviceWales
        {
            get;
        }

        public abstract bool IsReimbursableDeviceNI
        {
            get;
        }

        public abstract bool IsReimbursableDeviceScotland
        {
            get;
        }

        public abstract bool IsMedicine
        {
            get;
        }

        public abstract bool IsUnlicensed
        {
            get;
        }

        public abstract bool IsAvailable
        {
            get;
        }

        public virtual bool IsIssuable
        {
            get { return IsAvailable && !(IsSecondaryCare || IsExtemp || IsParallelImport); }
        }

        public  bool IsDmd
        {
            get { return !(new ConceptCode(DmdCode).IsInps); }
        }

        public virtual bool IsReconciledDmdProduct
        {
            //returns true if product is reconciled
            get
            {
                return ReconciledDmdProductRow.HasData;
            }
        }

        public virtual bool IsReconciledGemscriptProduct
        {
            //returns true if product is reconciled
            get
            {
                return ReconciledAuthoredGemscriptRow.HasData;
            }
        }

        public virtual UInt64 ReconciledGemscriptId
        {
            get
            {
                if (!IsReconciledDmdProduct)
                    return 0;
                return ReconciledDmdProductRow.GetConceptId("id_reconciled_product");
            }
        }

        public virtual bool IsInteractionControlled
        {
            get { return true; }
        }

        protected bool GenerateConcept(ref Concept concept, string bcbField, ConceptDictionary.CommonConcepts parentConcept)
        {
            if (concept == null)
            {
                concept = GenerateConcept(ProductRow.GetConceptId(bcbField), parentConcept);
            }
            return concept != null;
        }

        protected Concept GenerateConcept(UInt64 id, ConceptDictionary.CommonConcepts parentConcept)
        {
            Concept result = null;
            if (id > 0)
            {
                Concept bcbConcept = Concepts.GetCachedBcbConcept(id);
                if (bcbConcept == null)
                {
                    bcbConcept = Concepts.GetBcbConcept(id, parentConcept);
                }

                result = GenerateConcept(bcbConcept, parentConcept);
            }
            return result;
        }

        protected Concept GenerateConcept(Concept newConcept, ConceptDictionary.CommonConcepts parentConcept)
        {
            Concept result = null;
            if (newConcept != null)
            {
                if (ConceptDictionary.IsSnomedInSource(parentConcept))
                {
                    result = Concepts.ImportExternalConcept(parentConcept, newConcept);
                }
                else
                {
                    result = Concepts.CreateInpsConcept(parentConcept, newConcept.Description);
                }
            }
            return result;
        }

        /*protected bool GenerateLinkedConcepts(ref List<Concept> linkedConcepts, ConceptDictionary.CommonConcepts parentConcept, List<string> concepts)
        {
            if (linkedConcepts == null)
            {
                // Generate test data
                linkedConcepts = new List<Concept>();
                foreach (string s in concepts)
                {
                    Concept newConcept = GenerateConcept(new Concept(s), parentConcept);
                    if (newConcept != null)
                        linkedConcepts.Add(newConcept);
                }
            }
            return linkedConcepts.Count > 0;
        }*/

        protected bool ReadRelatedRow(ref DataRowReader rowReader, string command)
        {
            return ReadRelatedRow(ref rowReader, command, BcbConnection);
        }

        protected bool ReadRelatedRow(ref DataRowReader rowReader, string command, DrugConnection connection)
        {
            if (rowReader == null)
            {
                //connection.Connect();
                rowReader = new DataRowReader(connection.GetFirstRow(command));
                //connection.Close();
            }

            return rowReader != null;
        }

        public XElement XmlDosage
        {
            get
            {
                XElement x = null;
                if (RData.Dosage.Count > 0)
                {
                    x = new XElement("dosages");
                    foreach (ResipAPI.TypeSpecificDosage d in RData.Dosage.Values)
                    {
                        x.Add(new XElement("dosage", new XAttribute("direction", d.Description)));
                    }
                }
                return x;
            }
        }

        public String Dose
        {
            get
            {
                string s = RData.Posology.Value.Dose;
                if (RData.Dosage.Count > 0)
                {
                    s = RData.Dosage[1].Description;
                    foreach (ResipAPI.TypeSpecificDosage d in RData.Dosage.Values)
                    {
                        if (d.ByDefault == 1) s =  d.Description;
                    }
                }
                return s;
            }
        }

        public class ResipData
        {
            private ResipAPI.TypeFlags? _flags;
            private ResipAPI.TypePosology? _posology;
            private ResipAPI.TypeControlStatus? _controlStatus;
            private SortedList<Int32, ResipAPI.TypeSpecificDosage> _dosage;
            private string _readCode;
            private string _shortName;
            private string _warning;
            private string[] _labelCautions;
            private List<RdcType> _rdcCodes = new List<RdcType>();
            private ResipAPI.TypeRoute[] _routes;

            public ResipAPI.TypeFlags? Flags
            {
                get {return _flags;}
            }

            public ResipAPI.TypePosology? Posology
            {
                get { return _posology; }
            }

            public ResipAPI.TypeControlStatus? ControlStatus
            {
                get { return _controlStatus; }
            }

            public SortedList<Int32, ResipAPI.TypeSpecificDosage> Dosage
            {
                get { return _dosage; }
            }

            public string ReadCode
            {
                get { return _readCode; }
            }

            public string ShortName
            {
                get { return _shortName; }
                set { _shortName = value; }
            }

            public string Warning
            {
                get { return _warning; }
            }

            public string[] LabelCautions
            {
                get { return _labelCautions; }
            }

            public virtual List<RdcType> RdcCodes
            {
                get { return _rdcCodes; }
            }

            public virtual ResipAPI.TypeRoute[] Routes
            {
                get { return _routes; }
            }

            public ResipData(DmdProductDetails drug)
            {
              /* ResipAPI.Connect(DrugDataBuilder.Settings["host"].ToString(), DrugDataBuilder.Settings["user"].ToString(),
                   DrugDataBuilder.Settings["password"].ToString(), DrugDataBuilder.Settings["database"].ToString(),
                   "bcb_datas", "bcb_etr");*/
                string command;
                DataTable results;
               // _flags = ResipAPI.GetFlags(drug.DmdCode.ToString());
                command = string.Format("select * from bcb_datas.products_datas where \"ID_PRODUCT\" = '{0}'", drug.DmdCode.ToString());
                DataRow r = drug.BcbDataConnection.GetFirstRow(command);
                _flags = ResipAPI.GetFlags(r);
                if (drug.GetType().Name == "VmpDetails" || drug.GetType().Name == "AmpDetails")
                {
                    if (r != null)
                    {  
                        _posology = ResipAPI.GetPosology(r);
                        //_posology = ResipAPI.GetPosology(drug.DmdCode.ToString());
                        _readCode = ResipAPI.GetReadCode(r);
                        //_readCode = ResipAPI.GetReadCode(drug.DmdCode.ToString());
                        _warning = ResipAPI.GetWarning(r);
                        //_warning = ResipAPI.GetWarning(drug.DmdCode.ToString());
                        _shortName = ResipAPI.GetShortName(r);
                    }
                    command = string.Format("SELECT w.description from np_product_warnings p join np_warning_codes w on w.code = p.warning_code " +
                        "where p.id_product = '{0}'", drug.DmdCode.ToString());
                    results = drug.BcbDataConnection.GetTable(command);
                    if (results != null)
                        _labelCautions = ResipAPI.GetCautions(results);
                    command = String.Format(@"select * from products_rdc_codes where id_product = {0}", drug.DmdCode.ToString());
                    results = drug.BcbDataConnection.GetTable(command);
                    foreach (DataRow row in results.Rows)
                    {
                        _rdcCodes.Add(new RdcType(Convert.ToUInt32(row["id_rdc"]), Convert.ToBoolean(row["main_rdc"])));
                    }
                    command = String.Format(@"select * from products_specific_dosage where ""ID_PRODUCT"" = '{0}'", drug.DmdCode.ToString());
                    _dosage = ResipAPI.GetSpecificDosage(drug.BcbDataConnection.GetTable(command));
                }

                command = string.Format("SELECT pcd.* FROM products_control pc JOIN products_control_data pcd ON pc.id_control = pcd.id_control WHERE pc.id_product = '{0}' and " +
                                        "pc.id_product = (SELECT max(id_product) FROM products_control WHERE id_product = '{0}')", drug.DmdCode.ToString());
                DataTable table = _bcbDataConnection.GetTable(command);
                _controlStatus = ResipAPI.GetControlStatus(table);
                //_controlStatus = ResipAPI.GetControlStatus(drug.DmdCode.ToString())[0];
                command = string.Format("SELECT pv.*, r.description FROM ident_produits_voies_dmd pv JOIN bcb_inps.lk_administration_route r ON pv.route_code = r.code WHERE pv.id_product = '{0}'", drug.DmdCode.ToString());
                table = _bcbDataConnection.GetTable(command);
                _routes = ResipAPI.GetRoutes(table);
                //ResipAPI.Close();
            }
        }
    }

    //class added to deal with new version of various flags (is_active, is_available) following changes to requirements.
    //intention to prevent drug dictionary and DLM code change being tied together
    public abstract class Flags
    {
        private Boolean? _england = null;
        private Boolean? _ni = null;
        private Boolean? _scotland = null;
        private Boolean? _wales = null;

        public abstract bool IsActive
        {
            get;
        }

        public abstract bool IsAvailable
        {
            get;
        }

        public abstract bool IsIssuable
        {
            get;
        }

        public abstract bool IsSecondaryCare
        {
            get;
        }

        public abstract bool IsImported
        {
            get;
        }

        public virtual bool England
        {
            get
            {
                if (_england != null) return (Boolean)_england;
                return IsFp10England == IsFp10;
            }
            set { _england = value; }
        }

        public virtual bool Wales
        {
            get
            {
                if (_wales != null) return (Boolean)_wales;
                return IsFp10Wales == IsFp10;
            }
            set { _wales = value; }
        }

        public virtual bool Scotland
        {
            get
            {
                if (_scotland != null) return (Boolean)_scotland;
                return IsFp10Scotland == IsFp10;
            }
            set { _scotland = value; }
        }

        public virtual bool Ni
        {
            get
            {
                if (_ni != null) return (Boolean)_ni;
                return IsFp10NI == IsFp10;
            }
            set { _ni = value; }
        }

        public abstract bool IsFp10
        {
            get;
            set;
        }

        public abstract bool IsFp10England
        {
            get;
        }

        public abstract bool IsFp10Wales
        {
            get;
        }

        public abstract bool IsFp10Scotland
        {
            get;
        }

        public abstract bool IsFp10NI
        {
            get;
        }

        public abstract bool UseGeneric
        {
            get;
        }

         public abstract bool UseBrand
        {
            get;
        }
    }
}

