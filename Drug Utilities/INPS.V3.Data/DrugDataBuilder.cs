﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using INPS.V3.Resip;
using Npgsql;
using NpgsqlTypes;
using System.Runtime.InteropServices;

namespace INPS.V3.Data
{
    public class StopWatch
    {

        private DateTime startTime;
        private DateTime stopTime;
        private bool running = false;


        public void Start()
        {
            this.startTime = DateTime.Now;
            this.running = true;
        }


        public void Stop()
        {
            this.stopTime = DateTime.Now;
            this.running = false;
        }


        // elaspsed time in milliseconds
        public double GetElapsedTime()
        {
            TimeSpan interval;

            if (running)
                interval = DateTime.Now - startTime;
            else
                interval = stopTime - startTime;

            return interval.TotalMilliseconds;
        }


        // elaspsed time in seconds
        public double GetElapsedTimeSecs()
        {
            TimeSpan interval;

            if (running)
                interval = DateTime.Now - startTime;
            else
                interval = stopTime - startTime;

            return interval.TotalSeconds;
        }

        // sample usage
        public static void Main(String[] args)
        {
            StopWatch s = new StopWatch();
            s.Start();
            // code you want to time goes here
            s.Stop();
            Console.WriteLine("elapsed time in milliseconds: " + s.GetElapsedTime());
        }
    }

    public class DrugDataBuilder
    {
        protected static string DEFAULT_LABEL_WARNING = "No label warning required";

        private UInt32 _nextProductCode = 1;
        private UInt32 _productCodeForm = 20;
        private InpsDrugConnection _inpsConnection = new InpsDrugConnection();
        private LexDrugConnection _lexConnection = new LexDrugConnection();
        private BcbConnection _bcbConnection = new BcbConnection();
        private BcbDataConnection _bcbDataConnection = new BcbDataConnection();
        private string releasePath = _settings["root_folder"].ToString();
        private TableRow _drugRow;
        private TableRow _drugFlagsRow;
        private TableRow _rdcLinkRow;
        private TableRow _packRow;
        private TableRow _drugTermRow;
        private TableRow _drugCautionRow;
        private TableRow _cautionRow;
        private TableRow _dictionaryRow;
        private TableRow _dictionaryChangeRow;
        private TableRow _replaceRow;
        private TableRow _ingredientsRow;
        private TableRow _unitsRow;
        private TableRow _excipientsRow;
        private TableRow _controlStatusRow;

        private string _dictionaryVersion;
        private Dictionary<ulong, int> Scoittish_Drug_Prices = new Dictionary<ulong, int>();
        private Dictionary<ulong, int> Scoittish_Drug_Max_Prices = new Dictionary<ulong, int>();

        public string DictionaryVersion
        {
            get { return _dictionaryVersion; }
        }
        
        private static Dictionary<string, string> _settings = new Dictionary<string,string>();

        public static Dictionary<string, string> Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        string _traceText = "";
        string _errorText = "";

        public DrugDataBuilder()
        {
            if (!_inpsConnection.Connect()) throw new Exception("Cannot connect to inps_drug");
            if (!_bcbConnection.Connect()) throw new Exception("Cannot connect to bcb_inps");
            if (!_lexConnection.Connect()) throw new Exception("Cannot connect to historical_drug");
            if (!_bcbDataConnection.Connect()) throw new Exception("Cannot connect to bcb_datas");

            TableDefinition drugTableDef = new TableDefinition("drug_gemscript");
            drugTableDef.AddAttribute(new TableAttribute("is_active", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_dmd", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_replaced", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("term_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("name", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("form", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("form_text", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("strength", NpgsqlDbType.Real));
            drugTableDef.AddAttribute(new TableAttribute("normalised_strength", NpgsqlDbType.Real));
            drugTableDef.AddAttribute(new TableAttribute("dmd_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("dmd_type", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("read_code", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("vtm", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("high_risk_warning", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("order_number", NpgsqlDbType.Varchar, 16));
            drugTableDef.AddAttribute(new TableAttribute("schedule", NpgsqlDbType.Smallint));
            drugTableDef.AddAttribute(new TableAttribute("sub_schedule", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("strength_text", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("category", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("is_generic", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_branded_generic", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_fp10", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_fp10_mda", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_doh_approved", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_controlled", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_acbs", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_triangle", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_sls", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_contraception", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_pa", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("use_brand", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("use_generic", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_available", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_independent_prescriber_formulary", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_nurse_formulary", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_scottish_nurse_formulary", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("special", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("manufacturer", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("flavour", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("vmp_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("is_discontinued", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("can_have_af_endorsement", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("search_key", NpgsqlDbType.Text));
            drugTableDef.AddAttribute(new TableAttribute("order_key", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("is_issuable", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("england", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("wales", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("scotland", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("ni", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_parallel_import", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("dmd_prescribing_status", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("dmd_availability_restrictions", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("label_term_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("has_pom", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_unlicensed", NpgsqlDbType.Boolean));
            _drugRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("drug_flags_v2");
            drugTableDef.AddAttribute(new TableAttribute("is_active", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("dmd_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("is_fp10", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("use_brand", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("use_generic", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_available", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_issuable", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("england", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("wales", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("scotland", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("ni", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_secondary_care", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_imported", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("sls_england", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("sls_wales", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("sls_scotland", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("sls_ni", NpgsqlDbType.Boolean));
            _drugFlagsRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("drug_class_mapping");
            drugTableDef.AddAttribute(new TableAttribute("drug_class_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("main", NpgsqlDbType.Boolean));
            _rdcLinkRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("pack");
            drugTableDef.AddAttribute(new TableAttribute("drug_id", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("source_id", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("is_active", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_replaced", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_discontinued", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("size", NpgsqlDbType.Double));
            drugTableDef.AddAttribute(new TableAttribute("uom", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("price", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("price_max", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("legal", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("order_number", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("sub_pack", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("is_nurse_formulary", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_scottish_nurse_formulary", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("is_divisible", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("price_scotland", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("price_wales", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("price_ni", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("price_max_scotland", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("price_max_wales", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("price_max_ni", NpgsqlDbType.Integer));
            _packRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("gemscript_drug_term_table");
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("term_id", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("shortened", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("term_text", NpgsqlDbType.Varchar));
            _drugTermRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("drug_dictionary.drug_dictionary");
            drugTableDef.AddAttribute(new TableAttribute("live", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("build_date", NpgsqlDbType.Date));
            drugTableDef.AddAttribute(new TableAttribute("inps_version", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("dmd_version", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("min_icf", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("api_version", NpgsqlDbType.Date));
            drugTableDef.AddAttribute(new TableAttribute("np_data_version", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("np_struct_version", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("icd10_version", NpgsqlDbType.Varchar));
            /*drugTableDef.AddAttribute(new TableAttribute("read_version", NpgsqlDbType.Varchar));*/
            _dictionaryRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("drug_cautions");
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("caution_id", NpgsqlDbType.Integer));
            _drugCautionRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("cautions");
            drugTableDef.AddAttribute(new TableAttribute("caution", NpgsqlDbType.Varchar));
            _cautionRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("dictionary_change");
            drugTableDef.AddAttribute(new TableAttribute("rec_counter", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("drugcode", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("old_drug_class_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("new_drug_class_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("version", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("read_code", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("change_date", NpgsqlDbType.Date));
            _dictionaryChangeRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("replace");
            drugTableDef.AddAttribute(new TableAttribute("date", NpgsqlDbType.Date));
            drugTableDef.AddAttribute(new TableAttribute("prod_to", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("prod_from", NpgsqlDbType.Integer));
            _replaceRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("control_status");
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("allergy", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("doubling", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("contraindication", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("precaution", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("prescriber_warning", NpgsqlDbType.Boolean));
            drugTableDef.AddAttribute(new TableAttribute("interaction", NpgsqlDbType.Boolean));
            _controlStatusRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("compo_cip_pa");
            drugTableDef.AddAttribute(new TableAttribute("ID_PRODUCT", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("ORDRE", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("CODE_PA", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("COMMENTAIRE", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("DMD_CODE", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("QUANTITE", NpgsqlDbType.Real));
            drugTableDef.AddAttribute(new TableAttribute("CODE_UNITE", NpgsqlDbType.Bigint));
            _ingredientsRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("bcb_etr.compo_pa_unites");
            drugTableDef.AddAttribute(new TableAttribute("CODE_UNITE", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("LIBELLE_UNITE", NpgsqlDbType.Varchar));
            _unitsRow = new TableRow(drugTableDef);

            drugTableDef = new TableDefinition("compo_cip_excipients");
            drugTableDef.AddAttribute(new TableAttribute("ID_PRODUCT", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("ORDRE", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("CODE_EXCIPIENT", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("COMMENTAIRE", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("DMD_CODE", NpgsqlDbType.Bigint));
            _excipientsRow = new TableRow(drugTableDef);

            try
            {
                DmdProductDetails.Concepts.InitialiseBaseConcepts();
            }
            catch (Exception e)
            {
                throw new Exception("Cannot initialise concepts\n", e);
            }
            InitialiseIdCounter();
            //restore authored data
        }

        public string TraceText
        {
            get { return _traceText; }
        }

        public string ErrorText
        {
            get { return _errorText; }
        }

        public void BuildAllSetUp()
        {
            //delete/rename tables from previous version
            NpgsqlCommand command = new NpgsqlCommand();
      
            //check NexPhase data is available
            string databasePath = Settings["root_folder"] + "\\" + _dictionaryRow.Values["inps_version"].ToString().Substring(0, 7) + "\\NexPhase";
            if (!Directory.Exists(databasePath))
                throw new Exception("Nexphase data at not found at " + databasePath);
            command = new NpgsqlCommand("CREATE TABLE old_drug AS SELECT * FROM drug_gemscript", _inpsConnection.Connection);
            try
            {
                command.ExecuteNonQuery();
                command = new NpgsqlCommand("CREATE INDEX \"old_drug_Index_1\" ON inps_drug.old_drug USING btree (drug_code)", _inpsConnection.Connection);
                command.ExecuteNonQuery();
                command = new NpgsqlCommand("CREATE INDEX \"old_drug_Index_2\" ON inps_drug.old_drug USING btree (dmd_code)", _inpsConnection.Connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " old_drug table already exists - this version will be used");
            }
            command.CommandText = "";

            String[] setupCommands = {"DELETE FROM drug_gemscript", "DELETE FROM pack", "DELETE FROM inps_drug.drug_flags_v2",
                                         "DELETE FROM posology", "DELETE FROM drug_class_mapping", "DELETE FROM icd10_map"," DELETE FROM drug_cautions"," DELETE FROM dictionary_change", "DELETE FROM control_status",
                                         "DELETE FROM relationships WHERE relationship = 45011000027129 or relationship = 55011000027126 or relationship = 115011000027128", "DELETE FROM unit_mapping",
                                         "ALTER TABLE drug_gemscript DROP CONSTRAINT \"PK_drug\"", "ALTER TABLE drug_gemscript ADD CONSTRAINT \"PK_drug\" PRIMARY KEY (drug_code, is_fp10, is_acbs)"
                                         /*, "DELETE FROM replace"*/};
            foreach (String s in setupCommands)
            {
                command = new NpgsqlCommand(s, _inpsConnection.Connection);
                command.ExecuteNonQuery();
                command.CommandText = "";
            }
            String[] setupBcbData = {"DELETE FROM bcb_datas.compo_cip_pa where \"COMMENTAIRE\" = 'Added from dm+d'", "DELETE FROM bcb_datas.compo_cip_excipients where \"COMMENTAIRE\" = 'Added from dm+d'",
                                    "DELETE FROM bcb_datas.compo_cip_pa where \"COMMENTAIRE\" like 'Added from multilex%'",
                                    "DELETE FROM bcb_datas.compo_cip_pa where \"COMMENTAIRE\" like 'Added from historical%'"};
            foreach (String s in setupBcbData)
            {
                command = new NpgsqlCommand(s, _bcbDataConnection.Connection);
                command.ExecuteNonQuery();
                command.CommandText = "";
            }
            //_bcbConnection.RestoreDatabase(databasePath);

            // Create Country specific price lists
            ImportScotishPricesForDrugPacks();
            
        }

        public void setDictionaryVersion(Dictionary<String, String> version)
        {
            //reset dictionary
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            TableUpdater dictionaryUpdater = new TableUpdater(_dictionaryRow.Definition);
            _dictionaryRow.Clear();
            _dictionaryRow.SetValue("live", false);
            dictionaryUpdater.UpdateInTable(command, _dictionaryRow);
            _dictionaryRow.Clear();
            string query = @"select inps_version from drug_dictionary.drug_dictionary order by version_id DESC limit 1";
            DataRow result = _inpsConnection.GetFirstRow(query);
            string inpsVersionDate = version["year"] + "." + version["month"];
            int versionNumber = 1;
            if (result != null)
            {
                string previousInpsVersion = Convert.ToString(result["inps_version"]);
                if (result != null && previousInpsVersion.Contains(inpsVersionDate))
                    versionNumber = Convert.ToInt16(previousInpsVersion.Substring(previousInpsVersion.LastIndexOf(".") + 1)) + 1;
            }
            _dictionaryVersion = inpsVersionDate + "." + versionNumber.ToString("00");
            _dictionaryRow.SetValue("live", true);
            _dictionaryRow.SetValue("build_date", DateTime.Now);
            _dictionaryRow.SetValue("inps_version", _dictionaryVersion);
            _dictionaryRow.SetValue("api_version", DateTime.Parse("2011-12-14"));

            _bcbDataConnection.DropSchema("bcb_datas");
            _bcbDataConnection.DropSchema("bcb_etr");
            _bcbDataConnection.DropSchema("bcb_inps");
            //restore new backup
            string databasePath = Settings["root_folder"] + "\\" + _dictionaryRow.Values["inps_version"].ToString().Substring(0, 7) + "\\Authored\\vision.backup";
            if (!File.Exists(databasePath))
                throw new Exception("Restore cannot be executed. " + databasePath + " not found.");
            else
                _inpsConnection.RestoreDatabase(databasePath);
            //need to look up dmd version in sys_audit table
            query = @"select * from sys_audit where source = 'dm+d' order by id DESC limit 1";
            result = _bcbConnection.GetFirstRow(query);
            string dmdVersionPath = Convert.ToString(result["version"]);
            string dmdVersion = dmdVersionPath.Substring(dmdVersionPath.LastIndexOf("/") + 1);
            _dictionaryRow.SetValue("dmd_version", dmdVersion);
            //get read version from bcb_inps.read_dictionary - read orders upper then lower case postgres order aAbB etc default - better way to do this?
            /*query = @"select code, term from read_dictionary order by code DESC limit 1";
            DataRow resultAll = _bcbConnection.GetFirstRow(query);
            query = @"select * from bcb_inps.read_dictionary where lower(code) = code order by code DESC limit 1";
            DataRow resultLowercase = _bcbConnection.GetFirstRow(query);
            string term;
            int compare = resultAll["term"].ToString().Substring(0, 3).CompareTo(resultLowercase["term"].ToString().Substring(0, 3));
            if (compare > 0) term = resultAll["term"].ToString();
            else term = resultLowercase["term"].ToString();
            _dictionaryRow.SetValue("read_version", term);*/
        }

        public void BuildAllTidyUp()
        {
            SetPOMAvailableFlag(); // Do this before views are created 

            //delete/rename tables from previous version and cluster final tables + recreate country views
            NpgsqlCommand command = new NpgsqlCommand();
            List<String> setupCommands = new List<String> (){"DROP TABLE old_drug CASCADE", "CLUSTER \"PK_cautions\" on cautions", 
                                        "CLUSTER concepts_pkey on concepts", "CLUSTER order_key on drug_gemscript",
                                        "CLUSTER \"PK_drug_cautions\" on drug_cautions", "CLUSTER \"PK_drug_class\" on drug_class",
                                        "CLUSTER \"drug_mapping_Index_2\" on drug_class_mapping", "CLUSTER \"PK_drug_term\" on gemscript_drug_term_table",
                                        "CLUSTER \"PK_icd10_map\" on icd10_map", "CLUSTER \"PK_np_interactions\" on np_interactions",
                                        "CLUSTER \"PK_np_products\" on np_products", "CLUSTER \"PK_np_results\" on np_results",
                                        "CLUSTER \"PK_pack\" on pack", "CLUSTER \"PK_posology\" on posology",
                                        "CLUSTER \"PK_relationships\" on relationships", 
                                        "DROP VIEW IF EXISTS vw_drug_england", "CREATE VIEW vw_drug_england AS SELECT * FROM drug_gemscript WHERE england = true", 
                                        "DROP VIEW IF EXISTS vw_drug_ni", "CREATE VIEW vw_drug_ni AS SELECT * FROM drug_gemscript WHERE ni = true", 
                                        "DROP VIEW IF EXISTS vw_drug_scotland", "CREATE VIEW vw_drug_scotland AS SELECT * FROM drug_gemscript WHERE scotland = true",
                                        "DROP VIEW IF EXISTS vw_drug_wales", "CREATE VIEW vw_drug_wales AS SELECT * FROM drug_gemscript WHERE wales = true",
                                        "DROP VIEW IF EXISTS vw_drug_england_v2", 
                                        "CREATE VIEW vw_drug_england_v2 AS SELECT *, false as is_imported, false as is_secondary_care FROM drug_gemscript WHERE england = true", 
                                        "DROP VIEW IF EXISTS vw_drug_ni_v2", 
                                        "CREATE VIEW vw_drug_ni_v2 AS SELECT *, false as is_imported, false as is_secondary_care FROM drug_gemscript WHERE ni = true", 
                                        "DROP VIEW IF EXISTS vw_drug_scotland_v2", 
                                        "CREATE VIEW vw_drug_scotland_v2 AS SELECT *, false as is_imported, false as is_secondary_care FROM drug_gemscript WHERE scotland = true",
                                        "DROP VIEW IF EXISTS vw_drug_wales_v2", 
                                        "CREATE VIEW vw_drug_wales_v2 AS SELECT *, false as is_imported, false as is_secondary_care FROM drug_gemscript WHERE wales = true"}
                                     ;
            string[] countries = {"england", "scotland", "wales", "ni"};
            foreach (string country in countries)
            {
                setupCommands.Add(string.Format("DROP VIEW IF EXISTS inps_drug.vw_drug_{0}_v2", country));
                setupCommands.Add(string.Format("CREATE VIEW inps_drug.vw_drug_{0}_v2 " +
                    "AS SELECT df.drug_code, d.name, d.form, d.strength, d.term_code, df.dmd_code, d.dmd_type, d.vtm, d.order_number, d.schedule, d.sub_schedule, " +
                    "d.form_text, d.strength_text, d.read_code, d.manufacturer, d.flavour, d.vmp_code, d.high_risk_warning, d.category, d.search_key, d.order_key, " +
                    "df.is_active, df.is_issuable, d.england, d.scotland, d.wales, d.ni, d.is_discontinued, df.use_brand, df.use_generic, d.is_dmd, d.is_replaced, " +
                    "d.is_generic, d.is_branded_generic, df.is_fp10, d.is_fp10_mda, d.is_doh_approved, d.is_controlled, df.is_available, d.is_acbs, d.is_triangle, " +
                    "df.sls_{0} AS is_sls, d.is_contraception, d.is_pa, d.is_nurse_formulary, d.is_scottish_nurse_formulary, d.is_independent_prescriber_formulary, " +
                    "d.can_have_af_endorsement, d.special, d.is_parallel_import, d.dmd_prescribing_status, d.dmd_availability_restrictions, d.label_term_code, " +
                    "df.is_secondary_care, df.is_imported, d.has_pom, d.is_unlicensed " +
                    "FROM inps_drug.drug_flags_v2 df " +
                    "INNER JOIN inps_drug.drug_gemscript d ON (df.drug_code = d.drug_code AND d.{0}=true AND df.{0}=true)", country));
            }
            foreach (String s in setupCommands)
            {
                command = new NpgsqlCommand(s, _inpsConnection.Connection);
                command.ExecuteNonQuery();
                command.CommandText = "";
            }

            string dictionaryVersion = _dictionaryRow.Values["inps_version"].ToString();
            //Backup gemscript
            _inpsConnection.RenameSchema("inps_drug", "bkup_inps_drug");
            _bcbDataConnection.RenameSchema("bcb_datas", "bkup_bcb_datas");
            _bcbDataConnection.RenameSchema("bcb_etr", "bkup_bcb_etr");
            String dir = Settings["root_folder"] + "\\" + dictionaryVersion + "\\Vision\\download\\Gemscript";
            string[] schemas = { "bkup_bcb_datas", "bkup_bcb_etr", "bkup_inps_drug" };
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            try
            {
                _bcbConnection.BackupDatabase(dir + "\\Gemscript.backup", schemas);
            }
            catch (Exception ex)
            {
                _errorText = _errorText + "\n" + "Create back up failed: " + ex.Message;
                if (ex.InnerException != null)
                    _errorText = _errorText + ex.InnerException.Message;
            }
            _inpsConnection.RenameSchema("bkup_inps_drug", "inps_drug");
            _bcbDataConnection.RenameSchema("bkup_bcb_datas", "bcb_datas");
            _bcbDataConnection.RenameSchema("bkup_bcb_etr", "bcb_etr");
            //create backup of old version
            string databasePath = Settings["root_folder"] + "\\" + dictionaryVersion + "\\Vision\\download\\Gemscript\\Gemscript.backup";
            if (!File.Exists(databasePath))
                throw new Exception("Restore cannot be executed. " + databasePath + " not found.");
            _inpsConnection.RestoreDatabase(databasePath);
            _inpsConnection.CopySchema("drug_dictionary.drug_dictionary", "bkup_inps_drug.drug_dictionary");
            _inpsConnection.RunCommand("INSERT INTO bkup_inps_drug.drug_dictionary SELECT * FROM drug_dictionary.drug_dictionary");
            _inpsConnection.RenameSchema("bkup_inps_drug", @"""" + dictionaryVersion.Replace(".", "_") + @"_inps_drug""");
            _bcbDataConnection.RenameSchema("bkup_bcb_datas", @"""" + dictionaryVersion.Replace(".", "_") + @"_bcb_datas""");
            _bcbDataConnection.RenameSchema("bkup_bcb_etr", @"""" + dictionaryVersion.Replace(".", "_") + @"_bcb_etr""");
        }

        public void BuildAll()
        {
            //Debug.WriteLine("METHOD: BuildAll");
            //Debug.Indent();
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            
            //add _GP2GP_CODE row
            //add '5' code
            string update = "INSERT INTO inps_drug.drug_gemscript VALUES(5,'_GP2GP_DRUG','',0,null,305765001000027102,NULL,NULL,'',0,NULL,NULL,NULL,'',NULL,NULL,0,NULL,NULL," +
                "'_GP2GP_DRUG',null,false,true,true,true,true,true,true,false,false,false,false,true,true,false,false,false,true,false,false,false,false,false,false," +
                "false,false,true,NULL,false,false,NULL,NULL,null)";
            command.CommandText = update;
            command.ExecuteNonQuery();
            update = "INSERT INTO inps_drug.drug_flags_v2 VALUES(5,305765001000027102,false,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false)";
            command.CommandText = update;
            command.ExecuteNonQuery();

            //add _CONVERTED row
            //add '2' code
            update = "INSERT INTO inps_drug.drug_gemscript VALUES(2,'_CONVERTED DRUG','',0,null,299275001000027104,NULL,NULL,'',0,NULL,NULL,NULL,'',NULL,NULL,0,NULL,NULL," +
                "'_GP2GP_DRUG',null,false,true,true,true,true,true,true,false,false,false,false,true,true,false,false,false,true,false,false,false,false,false,false," +
                "false,false,true,NULL,false,false,NULL,NULL,null)";
            command.CommandText = update;
            command.ExecuteNonQuery();
            update = "INSERT INTO inps_drug.drug_flags_v2 VALUES(2,299275001000027104,false,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false)";
            command.CommandText = update;
            command.ExecuteNonQuery();


            string query = @"select v.""ID"", prev_id, vtm_id, invalid, full_name, combination_product_indicator, prescribing_status, cdc_code from vmp v left join vmp_control_drug_cat cdc on cdc.vmp_id = ""ID""";
            DataSet results = _bcbConnection.GetData("vmps", query);

            int numVMP = 0;
            int numAMP = 0;
            StringBuilder currentVmp = new StringBuilder();//useful for debugging
            StringBuilder currentAmp = new StringBuilder();//useful for debugging
            //int limit = 600;
            try
            {
                DataTable t = results.Tables["vmps"];
                foreach (DataRow sourceRow in t.Rows)
                {
                    if (numVMP == 1223)
                        Debug.WriteLine(DateTime.Now.ToLongTimeString() + "...numVMP: " + numVMP + " " + sourceRow["full_name"]);
                        //break; // for testing
                    
                    if (numVMP % 100 == 0) Debug.WriteLine(DateTime.Now.ToLongTimeString() + "...numVMP: " + numVMP + " " + sourceRow["full_name"]);
                    numVMP++;
                    VmpDetails vmp = new VmpDetails(sourceRow);
                    currentVmp.Clear().Append(vmp.Name);
                    if (vmp.IsReconciledGemscriptProduct) continue;
                    /*ResipAPI.Connect(DrugDataBuilder.Settings["host"].ToString(), DrugDataBuilder.Settings["user"].ToString(),
                            DrugDataBuilder.Settings["password"].ToString(), DrugDataBuilder.Settings["database"].ToString(),
                            "bcb_datas", "bcb_etr");*/
                    DmdProductDetails.ResipData r = vmp.RData;
                    foreach (AmpDetails a in vmp.Amps)
                    {
                        r = a.RData;
                    }
                    //ResipAPI.Close();
                    WriteVmpDetails(command, _drugRow, vmp);
                    WriteVmpFlagDetails(command, _drugFlagsRow, vmp);
                    //Debug.WriteLine(string.Format("flags: isMarkedInvalid {0}; isInvalid {1}, isDiscontinued{2}, isParallelImport{3}, isExtemp{4}, isSpecial {5}, isImported {6}, isSecondaryCare {7}, isBlacklist {8}",
                                    //vmp.IsMarkedInvalid, vmp.IsInvalid, vmp.IsDiscontinued, vmp.IsParallelImport, vmp.IsExtemp, vmp.IsSpecial, vmp.IsImported, vmp.IsSecondaryCare, vmp.IsBlacklisted));
                    WritePackDetails(command, _packRow, vmp);
                    WriteRdcDetails(command, _rdcLinkRow, vmp);
                    WritePosologyDetails(command, vmp);
                    WriteLabelCautions(command, vmp);
                    WriteShortName(command, vmp);
                    WriteControlStatus(command, vmp);
                    AddAmps(vmp, currentAmp, ref numAMP, command);

                    //write separate records if fp10 or acbs status varies between countries (first row written is always true for England
                    if (!(vmp.England == vmp.Scotland && vmp.England == vmp.Ni && vmp.England == vmp.Wales))
                    {
                        //one or both flags may differ from English version, and may be different between countries
                        //check Scottish flags
                        if (!vmp.England == vmp.Scotland)
                        {
                            vmp.England = false;
                            vmp.Scotland = true;
                            //Wales and NI match Scotland if flags are same
                            vmp.Wales = (vmp.IsAcbsWales == vmp.IsAcbsScotland && vmp.IsFp10Wales == vmp.IsFp10Scotland) ? vmp.Scotland : !vmp.Scotland;
                            vmp.Ni = (vmp.IsAcbsNI == vmp.IsAcbsScotland && vmp.IsFp10NI == vmp.IsFp10Scotland);
                            vmp.IsFp10 = vmp.IsFp10Scotland;
                            vmp.IsAcbs = vmp.IsAcbsScotland;
                            vmp.IsCountryViewDuplicate = true;
                            WriteVmpDetails(command, _drugRow, vmp);
                            WriteShortName(command, vmp);
                        }
                        //check Welsh flags - output row if not same as England or Scotland
                        //note cannot use vmp.England/vmp.Scotland flags to check this now as have been reset
                        if (!(vmp.IsAcbsWales == vmp.IsAcbsEngland && vmp.IsFp10Wales == vmp.IsFp10England) 
                            && !(vmp.IsAcbsWales == vmp.IsAcbsScotland && vmp.IsFp10Wales == vmp.IsFp10Scotland))
                        {
                            vmp.England = false;
                            vmp.Scotland = false;
                            vmp.Wales = true;
                            //NI match Wales if flags are same
                            vmp.Ni = (vmp.IsAcbsNI == vmp.IsAcbsWales && vmp.IsFp10NI == vmp.IsFp10Wales);
                            vmp.IsFp10 = vmp.IsFp10Wales;
                            vmp.IsAcbs = vmp.IsAcbsWales;
                            vmp.IsCountryViewDuplicate = true;
                            WriteVmpDetails(command, _drugRow, vmp);
                            WriteShortName(command, vmp);
                        }
                        //finally check NI flags - output row if not same as England, Scotland or Wales
                        //note cannot use vmp.England/vmp.Scotland etc flags to check this now as have been reset
                        if (!(vmp.IsAcbsNI == vmp.IsAcbsEngland && vmp.IsFp10Wales == vmp.IsFp10England) 
                            && !(vmp.IsAcbsWales == vmp.IsAcbsScotland && vmp.IsFp10Wales == vmp.IsFp10Scotland)
                            && !(vmp.IsAcbsWales == vmp.IsAcbsNI && vmp.IsFp10Wales == vmp.IsFp10NI))
                        {
                            vmp.England = false;
                            vmp.Scotland = false;
                            vmp.Wales = false;
                            //NI match Wales if flags are same
                            vmp.Ni = true;
                            vmp.IsFp10 = vmp.IsFp10NI;
                            vmp.IsAcbs = vmp.IsAcbsNI;
                            vmp.IsCountryViewDuplicate = true;
                            WriteVmpDetails(command, _drugRow, vmp);
                            WriteShortName(command, vmp);
                        }
                    }
                    //Acbs data is not part of seperate flags table so does not need to be taken into account here
                    if (!(vmp.Flags.England == vmp.Flags.Scotland && vmp.Flags.England == vmp.Flags.Ni && vmp.Flags.England == vmp.Flags.Wales))
                    {
                        vmp.Flags.England = !vmp.Flags.England;
                        vmp.Flags.Wales = !vmp.Flags.Wales;
                        vmp.Flags.Ni = !vmp.Flags.Ni;
                        vmp.Flags.Scotland = !vmp.Flags.Scotland;
                        vmp.Flags.IsFp10 = !vmp.Flags.IsFp10;
                        WriteVmpFlagDetails(command, _drugFlagsRow, vmp);
                    }
                    
                    foreach (AmpDetails a in vmp.Amps)
                    {
                        a.Ampps.Clear();
                        //vmp.Amps[i] = null;
                    }
                    vmp.Vmpps.Clear();
                    vmp.Amps.Clear();
                    vmp = null;
                    sourceRow.Delete();
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + string.Format(" Error while processing VMP {0} / AMP {1} ({2} {3})", currentVmp, currentAmp, numVMP, numAMP));
                Trace.WriteLine(ex.Message);
                _errorText = ex.Message;
                throw ex;
            }
            _traceText = String.Format("{0} VMPs added. {1} AMPs added", numVMP.ToString(), numAMP.ToString()); 
            //Debug.Unindent();
        }

        public void ReconcileDrugTables()
        {
            //adds authored drugs which have been reconciled to dm+d drug already included in previous build
            List<Int32> drugs = new List<Int32>();
            string query = "SELECT * FROM old_drug od WHERE NOT(EXISTS (SELECT * FROM drug_gemscript d WHERE d.drug_code = od.drug_code))";
            DataSet results  = _inpsConnection.GetData("result", query);
            DataTable t = results.Tables["result"];
            foreach (DataRow sourceRow in t.Rows)
            {
                Trace.WriteLine("Reconciling drug " + sourceRow["name"] + " " + sourceRow["drug_code"]);
                //if already replaced (ie is_replaced = true) simply copy row across
                if (Convert.ToBoolean(sourceRow["is_replaced"]) == true)
                {
                    foreach (DataColumn item in sourceRow.Table.Columns)
                    {
                        _drugRow.SetValue(item.ColumnName, sourceRow[item.ColumnName]);
                    }
                    _drugRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection));
                    _drugRow.Clear();
                }
                else
                {
                    //check if dmd_code is now mapped to multilex code - if so update row and add to replace
                    query = string.Format("SELECT * FROM drug_gemscript WHERE dmd_code = {0} AND is_replaced = false", Convert.ToInt64(sourceRow["dmd_code"]));
                    DataRow data = _inpsConnection.GetFirstRow(query);
                    if (data != null)
                    {
                        query = string.Format("SELECT od.dmd_code FROM drug_gemscript d JOIN old_drug od ON od.drug_code = d.drug_code WHERE d.dmd_code = {0}", Convert.ToInt64(sourceRow["dmd_code"]));
                        DataRow inpsDmd = _inpsConnection.GetFirstRow(query);
                        foreach (DataColumn item in sourceRow.Table.Columns)
                        {
                            //reset dmd code if a previous code was created from the multilex drug
                            if (item.ColumnName == "dmd_code" && inpsDmd != null)
                                _drugRow.SetValue("dmd_code", inpsDmd["dmd_code"]);
                            else if (item.ColumnName == "is_replaced")
                                _drugRow.SetValue(item.ColumnName, true);
                            else if (item.ColumnName == "is_active")
                                _drugRow.SetValue(item.ColumnName, false);
                            else if (item.ColumnName == "is_dmd")
                                _drugRow.SetValue(item.ColumnName, false);
                            else
                                _drugRow.SetValue(item.ColumnName, sourceRow[item.ColumnName]);
                        }
                        _drugRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection));
                        _drugRow.Clear();
                        //add to replace table
                        query = string.Format("SELECT * FROM replace WHERE prod_to = {0} AND prod_from = {1}", sourceRow["drug_code"], data["drug_code"]);
                        if (_inpsConnection.GetFirstRow(query) == null)
                        {
                            TableUpdater replaceUpdater = new TableUpdater(_replaceRow.Definition);
                            _replaceRow.Clear();
                            _replaceRow.SetValue("date", DateTime.Now.Date);
                            _replaceRow.SetValue("prod_to", sourceRow["drug_code"]);
                            _replaceRow.SetValue("prod_from", data["drug_code"]);
                            _replaceRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection));
                        }

                    }
                    if (data == null)
                    {
                        //check if reconciled drug
                        query = string.Format("SELECT * FROM bcb_datas.products_reconciliations WHERE id_reconciled_product = {0}", Convert.ToInt64(sourceRow["dmd_code"]));
                        data = _inpsConnection.GetFirstRow(query);
                        if (data != null)
                        {
                            Trace.WriteLine(string.Format("Reconciling authored drug {0} to dmd drug {1}", data["id_reconciled_product"], 
                                data["id_product"]));
                            query = string.Format("SELECT drug_code FROM drug_gemscript WHERE dmd_code = {0}", Convert.ToInt64(data["id_product"]));
                            DataRow newDrugcode = _inpsConnection.GetFirstRow(query);
                            foreach (DataColumn item in sourceRow.Table.Columns)
                            {
                                //reset dmd code if a previous code was created from the multilex drug
                                /*if (item.ColumnName == "dmd_code" && inpsDmd != null)
                                    _drugRow.SetValue("dmd_code", inpsDmd["dmd_code"]);
                                else*/ if (item.ColumnName == "is_replaced")
                                    _drugRow.SetValue(item.ColumnName, true);
                                else if (item.ColumnName == "is_active")
                                    _drugRow.SetValue(item.ColumnName, false);
                                else if (item.ColumnName == "is_dmd")
                                    _drugRow.SetValue(item.ColumnName, false);
                                else
                                    _drugRow.SetValue(item.ColumnName, sourceRow[item.ColumnName]);
                            }
                            _drugRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection));
                            _drugRow.Clear();
                            //add to replace table if not already there
                            
                            query = string.Format("SELECT * FROM replace where prod_to = {0} AND prod_from = {1}", newDrugcode["drug_code"], sourceRow["drug_code"]);
                            if (_inpsConnection.GetFirstRow(query) == null)
                            {
                                TableUpdater replaceUpdater = new TableUpdater(_replaceRow.Definition);
                                _replaceRow.Clear();
                                _replaceRow.SetValue("date", DateTime.Now.Date);
                                _replaceRow.SetValue("prod_to", sourceRow["drug_code"]);
                                _replaceRow.SetValue("prod_from", newDrugcode["drug_code"]);
                                _replaceRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection));
                            }

                        }
                        else
                        {
                            drugs.Add(Convert.ToInt32(sourceRow["drug_code"]));
                            Trace.WriteLine(string.Format("Drug code found in previous dictionary but not new version: drug code: {0}, drug name: {1}, dmd code {2}", sourceRow["drug_code"],
                                sourceRow["name"], sourceRow["dmd_code"]));
                        }
                    }
                }
                //add drugs to drug_flags_v2
                _drugFlagsRow.SetValue("drug_code", Convert.ToInt32(sourceRow["drug_code"]));
                _drugFlagsRow.SetValue("dmd_code", Convert.ToInt64(sourceRow["dmd_code"]));
                _drugFlagsRow.SetValue("is_active", false);
                _drugFlagsRow.SetValue("is_issuable", false);
                _drugFlagsRow.SetValue("england", true);
                _drugFlagsRow.SetValue("scotland", true);
                _drugFlagsRow.SetValue("wales", true);
                _drugFlagsRow.SetValue("ni", true);
                _drugFlagsRow.SetValue("use_brand", false);
                _drugFlagsRow.SetValue("use_generic", false);
                _drugFlagsRow.SetValue("is_fp10", false);
                _drugFlagsRow.SetValue("is_available", false);
                _drugFlagsRow.SetValue("is_secondary_care", false);
                _drugFlagsRow.SetValue("is_imported", false);
                _drugFlagsRow.SetValue("sls_england", false);
                _drugFlagsRow.SetValue("sls_wales", false);
                _drugFlagsRow.SetValue("sls_scotland", false);
                _drugFlagsRow.SetValue("sls_ni", false);
                if (!_drugFlagsRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection)))
                    Trace.WriteLine("Error adding reconciled drug to drug_flags_v2 " + sourceRow["name"]);
            }
            //should exception be thrown here?
            if (drugs.Count > 0) if (drugs != null) throw (new Exception(drugs.Count + " unmatched drugs found in previous version of drug dictionary. See log for details."));
            //also check all drugs in historical_drug are in inps_drug
            query = "select * from historical_drug.historical_drug ld where not(exists( select * from inps_drug.drug_gemscript d where ld.drug_code = d.drug_code))";
            DataSet missingDrugs = _inpsConnection.GetData("result", query);
            DataTable missingDrugsTable = missingDrugs.Tables["result"];
            if (missingDrugsTable.Rows.Count > 0)
            {
                _traceText = _traceText + ("\nWARNING: drugs have been found in historical_drug.historical_drug which have not been added to inps_drug.drug. This should be investigated");
                foreach (DataRow sourceRow in missingDrugsTable.Rows)
                    Trace.WriteLine(string.Format("Drug code found in previous historical_drug.historical_drug but not in inps_drug.drug_gemscript: drug code: {0}, drug name: {1}, dmd code {2}", sourceRow["drugcode"],
                        sourceRow["name"], sourceRow["dmdcode"]));
            }
        }

        private void AddAmps(VmpDetails vmp, StringBuilder currentAmp, ref int numAMP, NpgsqlCommand command)
        {
            for (int i = 0; i < vmp.Amps.Count; i++)
            {
                AmpDetails amp = vmp.Amps[i];
                //if (numAMP == 17)
                //break; // for testing
               // Debug.WriteLine(DateTime.Now.ToLongTimeString() + "...numAMP: " + numAMP + " " + amp.Name);
                currentAmp.Clear().Append(amp.Name);
                numAMP++;
                if (numAMP > 0)
                {
                    WriteAmpDetails(command, _drugRow, amp);
                    WriteAmpFlagDetails(command, _drugFlagsRow, amp);
                    WritePackDetails(command, _packRow, amp);
                    WriteRdcDetails(command, _rdcLinkRow, amp);
                    WritePosologyDetails(command, amp);
                    WriteLabelCautions(command, amp);
                    WriteShortName(command, amp);
                    WriteControlStatus(command, amp);

                    if (amp.DmdCode == 671711000001106)
                        Debug.WriteLine(amp.Name);

                    //write separate records if fp10 or acbs status varies between countries (first row written is always true for England
                    if (!(amp.England == amp.Scotland && amp.England == amp.Ni && amp.England == amp.Wales))
                    {
                        //one or both flags may differ from English version, and may be different between countries
                        //check Scottish flags
                        if (!amp.England == amp.Scotland)
                        {
                            amp.England = false;
                            amp.Scotland = true;
                            //Wales and NI match Scotland if flags are same
                            amp.Wales = (amp.IsAcbsWales == amp.IsAcbsScotland && amp.IsFp10Wales == amp.IsFp10Scotland) ? amp.Scotland : !amp.Scotland;
                            amp.Ni = (amp.IsAcbsNI == amp.IsAcbsScotland && amp.IsFp10NI == amp.IsFp10Scotland);
                            amp.IsFp10 = amp.IsFp10Scotland;
                            amp.IsAcbs = amp.IsAcbsScotland;
                            amp.IsCountryViewDuplicate = true;
                            WriteAmpDetails(command, _drugRow, amp);
                            WriteShortName(command, amp);
                        }
                        //check Welsh flags - output row if not same as England or Scotland
                        //note cannot use amp.England/amp.Scotland flags to check this now as have been reset
                        if (!(amp.IsAcbsWales == amp.IsAcbsEngland && amp.IsFp10Wales == amp.IsFp10England)
                            && !(amp.IsAcbsWales == amp.IsAcbsScotland && amp.IsFp10Wales == amp.IsFp10Scotland))
                        {
                            amp.England = false;
                            amp.Scotland = false;
                            amp.Wales = true;
                            //NI match Wales if flags are same
                            amp.Ni = (amp.IsAcbsNI == amp.IsAcbsWales && amp.IsFp10NI == amp.IsFp10Wales);
                            amp.IsFp10 = amp.IsFp10Wales;
                            amp.IsAcbs = amp.IsAcbsWales;
                            amp.IsCountryViewDuplicate = true;
                            WriteAmpDetails(command, _drugRow, amp);
                            WriteShortName(command, amp);
                        }
                        //finally check NI flags - output row if not same as England, Scotland or Wales
                        //note cannot use amp.England/amp.Scotland etc flags to check this now as have been reset
                        if (!(amp.IsAcbsNI == amp.IsAcbsEngland && amp.IsFp10Wales == amp.IsFp10England)
                            && !(amp.IsAcbsWales == amp.IsAcbsScotland && amp.IsFp10Wales == amp.IsFp10Scotland)
                            && !(amp.IsAcbsWales == amp.IsAcbsNI && amp.IsFp10Wales == amp.IsFp10NI))
                        {
                            amp.England = false;
                            amp.Scotland = false;
                            amp.Wales = false;
                            //NI match Wales if flags are same
                            amp.Ni = true;
                            amp.IsFp10 = amp.IsFp10NI;
                            amp.IsAcbs = amp.IsAcbsNI;
                            vmp.IsCountryViewDuplicate = true;
                            WriteAmpDetails(command, _drugRow, amp);
                            WriteShortName(command, amp);
                        }
                    }
                    if (!(amp.Flags.England == amp.Flags.Scotland && amp.Flags.England == amp.Flags.Ni && amp.Flags.England == amp.Flags.Wales))
                    {
                        amp.Flags.England = !amp.Flags.England;
                        amp.Flags.Wales = !amp.Flags.Wales;
                        amp.Flags.Ni = !amp.Flags.Ni;
                        amp.Flags.Scotland = !amp.Flags.Scotland;
                        amp.Flags.IsFp10 = !amp.Flags.IsFp10;
                        amp.IsCountryViewDuplicate = true;
                        WriteAmpFlagDetails(command, _drugFlagsRow, amp);
                    }
                }
                //amp.Ampps.Clear();
                //vmp.Amps[i] = null;
            }
        }

        public void ImportICD10()
        {
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Importing ICD10");
            //use line by line approach to allow for special characters and to remove unwanted fields
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            
            StreamReader file = new StreamReader(releasePath + @"\Reference\ICD10\Read2ICD10.txt", Encoding.GetEncoding(1252));
            string line = "";
            try
            {
                while ((line = file.ReadLine()) != null)
                {
                    string[] p = line.Split(new Char[] { '\t' });
                    String query = String.Format(@"insert into icd10_map values('{0}', '{1}')", p[0], p[2]);
                    command.CommandText = query;
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (NpgsqlException ne)
                    {
                        if (ne.Message.Contains("duplicate key value violates unique constraint"))
                        {
                            Trace.WriteLine("WARNING: error thrown loading read2icd10 map: " + line);
                            Trace.WriteLine(ne.Message);
                        }
                        else throw ne;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                file.Close();
            }
        }

        public void ImportUnmappedLex(bool initialBuild)
        {
            //copy across 007 codes
            string update = "INSERT INTO inps_drug.drug_gemscript SELECT * FROM inps_drug.old_drug WHERE drug_code % 100 = 7";
            _inpsConnection.RunCommand(update);

            //need to also insert into drug_flags_v2
            update = "INSERT INTO inps_drug.drug_flags_v2 SELECT drug_code, dmd_code, is_active, is_issuable, england, scotland, wales, ni, use_brand, use_generic, is_fp10, is_available, false, false, is_sls, is_sls, is_sls, is_sls "
                + "FROM inps_drug.old_drug WHERE drug_code % 100 = 7";
            _inpsConnection.RunCommand(update);
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            //no cautions or RDC for vamp

            //historical mlex
            string query = "SELECT * from historical_drug.historical_drug";
            DataTable table = _lexConnection.GetTable(query);
            foreach (DataRow row in table.Rows)
            {
                LexProductDetails lex = new LexProductDetails(row, null);
                WriteLexDetails(command, _drugRow, lex);
                WriteLexFlagDetails(command, _drugFlagsRow, lex);
                if (lex.RdcCodes.Count > 0)
                {
                    foreach (RdcType rdc in lex.RdcCodes)
                    {
                        _rdcLinkRow.Clear();
                        _rdcLinkRow.SetValue("drug_class_code", rdc.Code);
                        _rdcLinkRow.SetValue("drug_code", lex.LexCode);
                        _rdcLinkRow.SetValue("main", rdc.Main);
                        _rdcLinkRow.AppendToTable(command);
                    }
                }
            }
            WriteHistoricalIngredients();
        }

        private void WriteHistoricalIngredients()
        {
            Trace.WriteLine("WriteHistoricalIngredients");
            NpgsqlCommand command = new NpgsqlCommand("", _bcbDataConnection.Connection);
            string query = "SELECT DISTINCT(drug_code) FROM historical_drug.historical_ingredients";
            try
            {
                DataTable data =  _inpsConnection.GetTable(query);
                foreach (DataRow row in data.Rows)
                {
                    int order = 0;
                    query = string.Format("SELECT d.dmd_code, i.* FROM historical_drug.historical_ingredients i JOIN inps_drug.drug_gemscript d ON d.drug_code = i.drug_code " +
                        "WHERE d.drug_code = {0} " +
                        "AND NOT(EXISTS(SELECT * FROM historical_drug.historical_ingredients WHERE drug_code = {0} AND mapped = false))", row["drug_code"]);
                    DataTable ingredients = _inpsConnection.GetTable(query);
                    foreach (DataRow i in ingredients.Rows)
                    {                          
                        if (!i["dmd_code"].ToString().Contains("1000027"))
                        {
                            Debug.WriteLine(string.Format("Mapping exists for drug code {0} ({1})", i["drug_code"], (i["dmd_code"].ToString())));
                            break;
                        }
                        else
                        {
                                if (i["bcb_code"].ToString() != "0")
                                {
                                    _ingredientsRow.Clear();
                                    _ingredientsRow.SetValue("ID_PRODUCT", Convert.ToInt64(i["dmd_code"]));
                                    //_ingredientsRow.SetValue("ID_PRODUCT", Convert.ToInt64(lines[0][0]));
                                    _ingredientsRow.SetValue("CODE_PA", i["bcb_code"]);
                                    _ingredientsRow.SetValue("ORDRE", ++order);
                                    _ingredientsRow.SetValue("COMMENTAIRE", "Added from historical drug " + i["drug_code"]);
                                    _ingredientsRow.SetValue("DMD_CODE", i["dmd_code"]);
                                    _ingredientsRow.AppendToTable(command);
                                }
                            //update control status TODO
                            
                        }
                    }
                    if (ingredients != null && ingredients.Rows.Count > 0)
                    {
                        _controlStatusRow.Clear();
                        _controlStatusRow.SetValue("drug_code", row["drug_code"]);
                        _controlStatusRow.SetValue("allergy", true);
                        _controlStatusRow.SetValue("doubling", true);
                        _controlStatusRow.SetValue("contraindication", false);
                        _controlStatusRow.SetValue("precaution", false);
                        _controlStatusRow.SetValue("prescriber_warning", false);
                        _controlStatusRow.SetValue("interaction", false);
                        _controlStatusRow.AppendToTable(new NpgsqlCommand("", _inpsConnection.Connection));
                    }
                    else 
                        Debug.WriteLine("No ingredients");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void ImportUnitMappingData()
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command = new NpgsqlCommand("", _inpsConnection.Connection);
            command.CommandText = "DELETE FROM unit_mapping";
            command.ExecuteNonQuery();
            command.CommandText = "";

            TableDefinition mappingTableDef = new TableDefinition("unit_mapping");
            mappingTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Integer));
            mappingTableDef.AddAttribute(new TableAttribute("lex_pack", NpgsqlDbType.Varchar));
            mappingTableDef.AddAttribute(new TableAttribute("uom", NpgsqlDbType.Bigint));
            mappingTableDef.AddAttribute(new TableAttribute("ratio", NpgsqlDbType.Double));
            TableRow destinationRow = new TableRow(mappingTableDef);

            List<string> lines = new List<string>();
            //import Multilex data
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Importing Multilex unit mapping");
            DbfAccessor duAccessor = new DbfAccessor(releasePath + "/Reference/Multilex");
            string query = "select Lexcode, Lexdesc, Uomcode, Ltodratio  from drugunit";
            DataTable data = duAccessor.getData(query);

            foreach (DataRow row in data.Rows)
            {
                Int32 drugCode = Convert.ToInt32(row["Lexcode"]);
                drugCode = (drugCode % 1000000) * 1000 + (drugCode / 1000000);
                //remove duplicates
                string line = drugCode + "\t" + row["Lexdesc"] + "\t" + row["Uomcode"] + "\t" + row["Ltodratio"];
                if (!lines.Contains(line))
                {
                    destinationRow.SetValue("drug_code", drugCode);
                    destinationRow.SetValue("lex_pack", row["Lexdesc"]);
                    destinationRow.SetValue("uom", row["Uomcode"]);
                    destinationRow.SetValue("ratio", row["Ltodratio"]);
                    destinationRow.AppendToTable(command);
                    lines.Add(line);
                }
                //else Debug.WriteLine("Duplicate line: " + line);
            }
            //import gemscript unit mapping data
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Importing gemscript unit mapping");
            StreamReader file = new StreamReader(releasePath + @"\Reference\UnitMapping\gemscript_unit_mapping.txt");
            try
            {
                string line;

                TableUpdater drugUpdater = new TableUpdater(_drugRow.Definition);
                while ((line = file.ReadLine()) != null)
                {
                    if (!lines.Contains(line))
                    {
                        string[] p = line.Split(new Char[] { '\t' });
                        destinationRow.SetValue("drug_code", Convert.ToInt32(p[0]));
                        destinationRow.SetValue("lex_pack", p[1]);
                        destinationRow.SetValue("uom", p[2]);
                        destinationRow.SetValue("ratio", p[3]);
                        destinationRow.AppendToTable(command);
                        lines.Add(line);
                    }
                    //else Debug.WriteLine("Duplicate line: " + line);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                lines.Clear();
                file.Close();
            }
        }

        public void ImportLexDrugData()
        {
            NpgsqlCommand command = new NpgsqlCommand();
            command = new NpgsqlCommand("", _lexConnection.Connection);
            command.CommandText = "DELETE FROM drug_all";
            command.ExecuteNonQuery();
            command.CommandText = "";

            TableDefinition lexDrugTableDef = new TableDefinition("drug_all");
            lexDrugTableDef.AddAttribute(new TableAttribute("checksum", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("version", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("drugcode", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("readcode", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("name", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("suppress", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("form", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("strength", NpgsqlDbType.Double));
            lexDrugTableDef.AddAttribute(new TableAttribute("drugeffect", NpgsqlDbType.Double));
            lexDrugTableDef.AddAttribute(new TableAttribute("fp10", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("discontinu", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("generic", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("gencode", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("controlled", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("usegeneric", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("usebrand", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("dohapprove", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("special", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("nonprint", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("acbs", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("triangle", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("oneitem", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("contracep", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("external", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("practice", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("oldvamp", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("bnfcode", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("formulary", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("ordno", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("main", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("fastkey", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("keyword", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("namelen", NpgsqlDbType.Numeric));
            lexDrugTableDef.AddAttribute(new TableAttribute("nameorder", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("term_code", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("schedule", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("scheme", NpgsqlDbType.Varchar));
            lexDrugTableDef.AddAttribute(new TableAttribute("toxic", NpgsqlDbType.Char));
            lexDrugTableDef.AddAttribute(new TableAttribute("dmd_code", NpgsqlDbType.Varchar));
            TableRow destinationRow = new TableRow(lexDrugTableDef);

            DbfAccessor duAccessor = new DbfAccessor(releasePath + "/Reference/Multilex");
            string query = "select drugcode, checksum, version, readcode, name, suppress, form, TRIM(str(strength)), drugeffect, fp10, discontinu, generic, gencode, controlled, usegeneric, usebrand, dohapprove, special, nonprint, " + 
                            "acbs, triangle, oneitem, contracep, external, practice, oldvamp, bnfcode, formulary, ordno, main, fastkey, keyword, namelen, nameorder, term_code, schedule, scheme, toxic, dmd_code " +
                            @" from drug where not(drugcode = ""00000101"")";
            DataTable data = duAccessor.getData(query);
            string d = "";
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Importing mlex drug data");
            foreach (DataRow row in data.Rows)
            {
                if (row["drugcode"].ToString() == "ZZZZZZZZ")
                    Debug.WriteLine("test");
                if (!(d.Equals(row["drugcode"])))
                {
                    //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Adding data for mlex drug " + row["drugcode"].ToString());
                    foreach (DataColumn col in data.Columns)
                        destinationRow.SetValue(col.ColumnName, row[col.ColumnName].ToString().Trim());
                    if (!destinationRow.AppendToTable(command))
                        Trace.WriteLine(DateTime.Now + "Error adding " + row["drugcode"].ToString() + " to drug_all");
                    d = row["drugcode"].ToString();
                }
            }

            command = new NpgsqlCommand("", _lexConnection.Connection);
            command.CommandText = "DELETE FROM drug_term";
            command.ExecuteNonQuery();
            command.CommandText = "";

            TableDefinition lexDrugTermTableDef = new TableDefinition("drug_term");
            lexDrugTermTableDef.AddAttribute(new TableAttribute("drugcode", NpgsqlDbType.Integer));
            lexDrugTermTableDef.AddAttribute(new TableAttribute("term_id", NpgsqlDbType.Varchar));
            lexDrugTermTableDef.AddAttribute(new TableAttribute("shortened", NpgsqlDbType.Char));
            lexDrugTermTableDef.AddAttribute(new TableAttribute("term_text", NpgsqlDbType.Varchar));
            destinationRow = new TableRow(lexDrugTermTableDef);

            query = "select drugcode, term_id, shortened, term_text from drugterm";
            data = duAccessor.getData(query);
            int total = data.Rows.Count;
            foreach (DataRow row in data.Rows)
            {
                foreach (DataColumn col in data.Columns)
                    destinationRow.SetValue(col.ColumnName, row[col.ColumnName]);
                destinationRow.AppendToTable(command);
                //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " " + total--);
            }
        }

        public void BuildRdc()
        {
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Building RDC");
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            command.CommandText = "delete from drug_class";
            command.ExecuteNonQuery();
            command.CommandText = "";
            TableDefinition drugTableDef = new TableDefinition("drug_class");
            drugTableDef.AddAttribute(new TableAttribute("drug_class_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("level", NpgsqlDbType.Numeric));
            drugTableDef.AddAttribute(new TableAttribute("description", NpgsqlDbType.Text));
            TableRow destinationRow = new TableRow(drugTableDef);

            string query = @"select * from rdc_codes";
            DataTable results = _bcbDataConnection.GetTable(query);
            foreach (DataRow row in results.Rows)
            {
                int level = 0;
                int rdcCode = Convert.ToInt32(row["id_rdc"]);
                if (rdcCode % 100 > 0)
                    level = 3;
                else if (rdcCode % 10000 > 0)
                    level = 2;
                else if (rdcCode % 1000000 > 0)
                    level = 1;
                destinationRow.SetValue("drug_class_code", rdcCode);
                destinationRow.SetValue("level", level);
                destinationRow.SetValue("description", row["description"].ToString());
                destinationRow.AppendToTable(command);
            }
        }

        void InitialiseIdCounter()
        {
            // Drug code have the form pppppfff where ppppp is the product id and fff is the form id (all numeric)
            // Lex codes have this form and they will be maintained where possible. If a lex code is not available,
            // then a code will be generated. INPS generated codes have the form ppppp020 where ppppp starts at one 
            // and is incremented by one for each new code. (the form id 20 is not used by any extant Multilex drug)
            //as all products now have gemscript codes 20 will not have enough values, therefore move on to 21
            string query = string.Format(@"select max(drug_code) as max_code from inps_drug.drug_gemscript where mod(drug_code, 1000) = {0}", _productCodeForm);
            DataRow result = _inpsConnection.GetFirstRow(query);
            if (result != null && !result.IsNull("max_code") && Convert.ToUInt32(result["max_code"]) == 99999000 + _productCodeForm)
            {
                _productCodeForm = _productCodeForm+1;
                query = string.Format(@"select max(drug_code) as max_code from inps_drug.drug_gemscript where mod(drug_code, 1000) = {0}", _productCodeForm);
                result = _inpsConnection.GetFirstRow(query);
            }
            if (result != null && !result.IsNull("max_code"))
            {
                uint lastProductCode = Convert.ToUInt32(result["max_code"]);
                _nextProductCode = (lastProductCode/1000)+1;
            }
            Trace.WriteLine("Counter initialised as " + _nextProductCode + "0" + _productCodeForm);
        }

        UInt32 getNextGemscriptCode()
        {
            if (_nextProductCode > 99999)
            {
                _nextProductCode = 1;
                _productCodeForm++;
                if (_productCodeForm > 21) throw new Exception("Warning: Maximum expected from code reached, cannot generate Gemscript code");
            }
            return (_nextProductCode++ * 1000) + _productCodeForm;
        }

        void WriteProductDetailsToRow(TableRow destinationRow, DmdProductDetails product)
        {
            //Debug.WriteLine("METHOD: WriteProductDetailsToRow");
            //Debug.Indent();
            destinationRow.Clear();
            if (product.VisionCode == 0)
            {
                //look in old_drug table
                DataRow existingDrugRow = null;
                uint lexCode = 0;
                string query;
                existingDrugRow = _inpsConnection.GetFirstRow(String.Format(@"select drug_code from old_drug where dmd_code={0}", product.DmdCode));
                    //check if drug is reconciled to (ie associated with authored dm+d gemscript drug)
                if (existingDrugRow == null &&product.PrevDmdCode != 0)
                {
                    query = String.Format(@"select drug_code from old_drug where dmd_code = {0} order by is_active desc",
                                                product.PrevDmdCode);
                    existingDrugRow = _inpsConnection.GetFirstRow(query);
                }
                if (existingDrugRow == null && product.ReconciledGemscriptId != 0)
                {
                    query = String.Format(@"select drug_code from old_drug where dmd_code = {0} order by is_active desc",
                                            product.ReconciledGemscriptId);
                    existingDrugRow = _inpsConnection.GetFirstRow(query);
                }
                if (existingDrugRow != null && lexCode == 0)
                {
                    uint lc = Convert.ToUInt32(existingDrugRow["drug_code"]);
                    lexCode = lc;
                }

                //if drugcode still not found generate new code
                if (lexCode == 0)
                    lexCode = getNextGemscriptCode();

                product.VisionCode = lexCode;
                //update new mapping table
                
            }
            destinationRow.SetValue("is_active", product.IsActive);
            destinationRow.SetValue("is_dmd", product.IsDmd);
            destinationRow.SetValue("is_replaced", false);
            destinationRow.SetValue("drug_code", product.VisionCode);
            destinationRow.SetValue("name", product.Name);
            destinationRow.SetValue("form", product.KeyForm);
            destinationRow.SetValue("form_text", product.Form);
            destinationRow.SetValue("read_code", product.ReadCode);
            destinationRow.SetValue("schedule", product.Vmp.ScheduleNumber);
            destinationRow.SetValue("category", product.Vmp.Category);
            destinationRow.SetValue("sub_schedule", product.Vmp.Schedule);
            destinationRow.SetValue("vmp_code", product.Vmp.VisionCode);
            destinationRow.SetValue("vtm", product.Vmp.Vtm);
            destinationRow.SetValue("high_risk_warning", product.HighRiskWarning);
            destinationRow.SetValue("manufacturer", product.Manufacturer);
            destinationRow.SetValue("flavour", product.Flavour);
            destinationRow.SetValue("strength", product.StrengthValue);
            destinationRow.SetValue("normalised_strength", product.NormalisedStrength);
            destinationRow.SetValue("strength_text", product.Strength);
            destinationRow.SetValue("is_discontinued", product.IsDiscontinued);
            destinationRow.SetValue("use_brand", product.UseBrand);
            destinationRow.SetValue("use_generic", product.UseGeneric);
            destinationRow.SetValue("is_fp10", product.IsFp10);
            destinationRow.SetValue("is_fp10_mda", product.IsFp10MDA);
            destinationRow.SetValue("is_doh_approved", product.IsDohApproved);
            destinationRow.SetValue("is_controlled", product.IsControlled);
            destinationRow.SetValue("is_acbs", product.IsAcbs);
            destinationRow.SetValue("is_triangle", product.IsBlackTriangle);
            destinationRow.SetValue("is_sls", product.IsSls);
            destinationRow.SetValue("is_contraception", product.IsContraception);
            destinationRow.SetValue("is_pa", product.IsPa);
            destinationRow.SetValue("is_nurse_formulary", product.IsNurseFormulary);
            destinationRow.SetValue("is_scottish_nurse_formulary", product.IsScottishNurseFormulary);
            destinationRow.SetValue("is_available", product.IsAvailable);
            destinationRow.SetValue("is_independent_prescriber_formulary", product.IsIndependentPrescriberFormulary);
            destinationRow.SetValue("special", product.IsSpecial); //check imported as part of IsSpecial check
            destinationRow.SetValue("search_key", product.SearchKey);
            destinationRow.SetValue("is_issuable", product.IsIssuable);
            destinationRow.SetValue("england", product.England);
            destinationRow.SetValue("wales", product.Wales);
            destinationRow.SetValue("scotland", product.Scotland);
            destinationRow.SetValue("ni", product.Ni);
            destinationRow.SetValue("is_parallel_import", product.IsParallelImport);
            destinationRow.SetValue("is_unlicensed", product.IsUnlicensed);
            //Debug.Unindent();
        }

        void WriteVmpDetails(NpgsqlCommand command, TableRow destinationRow, VmpDetails vmp)
        {
            //Debug.WriteLine("METHOD: WriteVmpDetails");
            //Debug.Indent();
            if (vmp.IsReconciledGemscriptProduct)
            {
                Trace.WriteLine(String.Format("Reconciled Gemscript product: {0} ({1})", vmp.Name, vmp.DmdCode));
                return;
            }
            WriteProductDetailsToRow(destinationRow, vmp);
            //WriteLinkedConcepts(vmp.ActiveIngredients, ConceptDictionary.CommonConcepts.HasActiveIngredient, vmp);
            //destinationRow.SetValue("is_active", vmp.IsValid);
            destinationRow.SetValue("dmd_code", vmp.DmdCode);
            destinationRow.SetValue("dmd_type", ConceptCode.ToNum(ConceptDictionary.CommonConcepts.Vmp));
            destinationRow.SetValue("is_branded_generic", false);
            destinationRow.SetValue("is_generic", true);
            destinationRow.SetValue("order_number", "");
            destinationRow.SetValue("can_have_af_endorsement", vmp.CanHaveAfEndorsement);
            destinationRow.SetValue("is_replaced", vmp.IsReplaced);
            destinationRow.SetValue("dmd_prescribing_status", vmp.PrescribingStatusConcept);

            if (!destinationRow.AppendToTable(command))
                Trace.WriteLine(DateTime.Now + " Adding VMP to inps_drug.drug_gemscript failed: " + vmp.VisionCode);
            if (!vmp.IsCountryViewDuplicate)
            {
                WriteLinkedConcepts(vmp.Routes, ConceptDictionary.CommonConcepts.HasRoute, vmp);
                List<Concept> ingredients = new List<Concept>();
                foreach (TypeIngredient ingredient in vmp.ActiveIngredients)
                    ingredients.Add(ingredient.Ingredient);
                WriteLinkedConcepts(ingredients, ConceptDictionary.CommonConcepts.HasActiveIngredient, vmp);
                //also add ingredients to bcb_datas
                WriteIngredients(vmp, vmp.ActiveIngredients);
            }
            //Debug.Unindent();
        }

        public void WriteVmpFlagDetails(NpgsqlCommand command, TableRow destinationRow, VmpDetails vmp)
        {
            destinationRow.SetValue("drug_code", vmp.VisionCode);
            destinationRow.SetValue("dmd_code", vmp.DmdCode);
            destinationRow.SetValue("is_active", vmp.Flags.IsActive);
            destinationRow.SetValue("is_issuable", vmp.Flags.IsIssuable);
            destinationRow.SetValue("england", vmp.Flags.England);
            destinationRow.SetValue("scotland", vmp.Flags.Scotland);
            destinationRow.SetValue("wales", vmp.Flags.Wales);
            destinationRow.SetValue("ni", vmp.Flags.Ni);
            destinationRow.SetValue("use_brand", vmp.Flags.UseBrand);
            destinationRow.SetValue("use_generic", vmp.Flags.UseGeneric);
            destinationRow.SetValue("is_fp10", vmp.Flags.IsFp10);
            destinationRow.SetValue("is_available", vmp.Flags.IsAvailable);
            destinationRow.SetValue("is_secondary_care", vmp.Flags.IsSecondaryCare);
            destinationRow.SetValue("is_imported", vmp.Flags.IsImported);
            destinationRow.SetValue("sls_england", vmp.Flags.IsSlsEngland);
            destinationRow.SetValue("sls_scotland", vmp.Flags.IsSlsScotland);
            destinationRow.SetValue("sls_wales", vmp.Flags.IsSlsWales);
            destinationRow.SetValue("sls_ni", vmp.Flags.IsSlsNi);
            if (!destinationRow.AppendToTable(command))
                Trace.WriteLine(DateTime.Now + " Adding VMP to inps_drug.drug_flags_v2 failed: " + vmp.VisionCode);
        }

        void WriteAmpDetails(NpgsqlCommand command, TableRow destinationRow, AmpDetails amp)
        {
            //Debug.WriteLine("METHOD: WriteAmpDetails");
            //Debug.Indent();
            if (amp.IsReconciledGemscriptProduct)
            {
                Trace.Write(String.Format("Reconciled Gemscript product: {0} ({1})", amp.Name, amp.DmdCode));
                return;
            }
            WriteProductDetailsToRow(destinationRow, amp);
            //WriteLinkedConcepts(amp.Excipients, ConceptDictionary.CommonConcepts.HasExcipient, amp);
            //destinationRow.SetValue("is_active", amp.IsValid);
            destinationRow.SetValue("dmd_code", amp.DmdCode);
            destinationRow.SetValue("dmd_type", ConceptCode.ToNum(ConceptDictionary.CommonConcepts.Amp));
            destinationRow.SetValue("is_branded_generic", amp.Name.StartsWith(amp.Vmp.Name));
            destinationRow.SetValue("is_generic", false);
            destinationRow.SetValue("order_number", amp.OrderNumber);
            destinationRow.SetValue("can_have_af_endorsement", amp.CanHaveAfEndorsement);
            destinationRow.SetValue("dmd_availability_restrictions", amp.AvailabilityRestrictionConcept);
            if (!destinationRow.AppendToTable(command))
                Trace.WriteLine(DateTime.Now + " Adding AMP to inps_drug.drug_gemscript failed: " + amp.VisionCode);
            if (!amp.IsCountryViewDuplicate)
            {
                WriteLinkedConcepts(amp.Routes, ConceptDictionary.CommonConcepts.HasRoute, amp);
                WriteLinkedConcepts(amp.Excipients, ConceptDictionary.CommonConcepts.HasExcipient, amp);
                WriteExcipients(amp, amp.Excipients);
                WriteIngredients(amp, amp.ActiveIngredients);
            }
            //Debug.Unindent();
        }

        public void WriteAmpFlagDetails(NpgsqlCommand command, TableRow destinationRow, AmpDetails amp)
        {
            if (amp.IsReconciledGemscriptProduct) return;
            destinationRow.SetValue("drug_code", amp.VisionCode);
            destinationRow.SetValue("dmd_code", amp.DmdCode);
            destinationRow.SetValue("is_active", amp.Flags.IsActive);
            destinationRow.SetValue("is_issuable", amp.Flags.IsIssuable);
            destinationRow.SetValue("england", amp.Flags.England);
            destinationRow.SetValue("scotland", amp.Flags.Scotland);
            destinationRow.SetValue("wales", amp.Flags.Wales);
            destinationRow.SetValue("ni", amp.Flags.Ni);
            destinationRow.SetValue("use_brand", amp.Flags.UseBrand);
            destinationRow.SetValue("use_generic", amp.Flags.UseGeneric);
            destinationRow.SetValue("is_fp10", amp.Flags.IsFp10);
            destinationRow.SetValue("is_available", amp.Flags.IsAvailable);
            destinationRow.SetValue("is_secondary_care", amp.Flags.IsSecondaryCare);
            destinationRow.SetValue("is_imported", amp.Flags.IsImported);
            destinationRow.SetValue("sls_england", amp.Flags.IsSlsEngland);
            destinationRow.SetValue("sls_scotland", amp.Flags.IsSlsScotland);
            destinationRow.SetValue("sls_wales", amp.Flags.IsSlsWales);
            destinationRow.SetValue("sls_ni", amp.Flags.IsSlsNi);
            if (!destinationRow.AppendToTable(command))
                Trace.WriteLine(DateTime.Now + " Adding AMP to inps_drug.drug_flags_v2 failed: " + amp.VisionCode);
        }

        void WriteLexDetails(NpgsqlCommand command, TableRow destinationRow, LexProductDetails lex)
        {
            //Debug.WriteLine(DateTime.Now.ToLongTimeString() + "METHOD: WriteLexDetails");
            //Debug.Indent();
            //if (lex.VisionCode == 28566005)
            //Debug.WriteLine("here");
            destinationRow.Clear();
            destinationRow.SetValue("is_active", false);
            destinationRow.SetValue("is_issuable", false);
            destinationRow.SetValue("england", true);
            destinationRow.SetValue("scotland", true);
            destinationRow.SetValue("wales", true);
            destinationRow.SetValue("ni", true);
            //get new Vision code from mapping file for now - will eventually take all unmapped drug details direct from old_drug
            destinationRow.SetValue("drug_code", lex.LexCode);
            destinationRow.SetValue("name", lex.Name);
            destinationRow.SetValue("form", lex.KeyForm);
            destinationRow.SetValue("strength_text", lex.StrengthText);
            destinationRow.SetValue("strength", lex.Strength);
            //term_code - added later
            destinationRow.SetValue("dmd_code", lex.DmdCode);
            destinationRow.SetValue("dmd_type", null);
            destinationRow.SetValue("vtm", null);
            destinationRow.SetValue("order_number", lex.OrderNumber);
            destinationRow.SetValue("schedule", null);
            destinationRow.SetValue("sub_schedule", null);
            destinationRow.SetValue("form_text", lex.FormText);
            //TODO need strength_text look up here
            destinationRow.SetValue("read_code", null);
            destinationRow.SetValue("manufacturer", lex.Manufacturer);
            destinationRow.SetValue("flavour", null);
            //need new code for generic here TODO
            destinationRow.SetValue("vmp_code", lex.Generic);
            destinationRow.SetValue("high_risk_warning", null);
            //TODO need catgeory - from mapping file
            destinationRow.SetValue("is_discontinued", true);
            destinationRow.SetValue("use_brand", false);
            destinationRow.SetValue("use_generic", false);
            destinationRow.SetValue("is_dmd", false);
            destinationRow.SetValue("is_replaced", false);
            destinationRow.SetValue("is_generic", lex.IsGeneric);
            destinationRow.SetValue("is_branded_generic", false);
            destinationRow.SetValue("is_fp10", false);
            destinationRow.SetValue("is_fp10_mda", false);
            destinationRow.SetValue("is_doh_approved", false);
            destinationRow.SetValue("is_controlled", false);
            destinationRow.SetValue("is_available", false);
            destinationRow.SetValue("is_acbs", false);
            destinationRow.SetValue("is_triangle", false);
            destinationRow.SetValue("is_sls", false);
            destinationRow.SetValue("is_contraception",false);
            destinationRow.SetValue("is_pa", false);
            destinationRow.SetValue("is_nurse_formulary", false);
            destinationRow.SetValue("is_scottish_nurse_formulary",false);
            destinationRow.SetValue("is_independent_prescriber_formulary", false);
            destinationRow.SetValue("search_key", lex.SearchKey);
            //destinationRow.SetValue("order_key", lex.SearchKey);
            destinationRow.SetValue("can_have_af_endorsement", null);
            destinationRow.SetValue("special", false);
            destinationRow.SetValue("category", lex.Category);
            destinationRow.SetValue("is_parallel_import", false);

            //Debug.WriteLine(DateTime.Now.ToLongTimeString() + "Drug code " + lex.VisionCode);
            if (!destinationRow.AppendToTable(command))
                Trace.WriteLine(DateTime.Now + " Adding lex drug to inps_drug.drug_gemscript failed: " + lex.LexCode + "/" + lex.LexCode);
            //Debug.Unindent();
        }

        public void WriteLexFlagDetails(NpgsqlCommand command, TableRow destinationRow, LexProductDetails lex)
        {
            destinationRow.SetValue("drug_code", lex.LexCode);
            destinationRow.SetValue("dmd_code", lex.DmdCode);
            destinationRow.SetValue("is_active", false);
            destinationRow.SetValue("is_issuable", false);
            destinationRow.SetValue("england", true);
            destinationRow.SetValue("scotland", true);
            destinationRow.SetValue("wales", true);
            destinationRow.SetValue("ni", true);
            destinationRow.SetValue("use_brand", false);
            destinationRow.SetValue("use_generic", false);
            destinationRow.SetValue("is_fp10", false);
            destinationRow.SetValue("is_available", false);
            destinationRow.SetValue("is_secondary_care", false);
            destinationRow.SetValue("is_imported", false);
            destinationRow.SetValue("sls_england", false);
            destinationRow.SetValue("sls_scotland", false);
            destinationRow.SetValue("sls_wales", false);
            destinationRow.SetValue("sls_ni", false);
            if (!destinationRow.AppendToTable(command))
                Trace.WriteLine(DateTime.Now + " Adding lex drug to inps_drug.drug_flags_v2 failed: " + lex.LexCode + "/" + lex.LexCode);
        }

        void WriteIngredients(DmdProductDetails product, List<TypeIngredient> ingredients)
        {
            //reorder by strength?
            //get current ingredients
            string query = "SELECT * FROM compo_cip_pa WHERE \"ID_PRODUCT\" = '" + product.DmdCode + "'";
            DataTable table = _bcbDataConnection.GetTable(query);
            int order = table != null ? table.Rows.Count : 0;
            NpgsqlCommand command = new NpgsqlCommand("", _bcbDataConnection.Connection);
            IOrderedEnumerable<TypeIngredient> sortedIngredients = ingredients.OrderByDescending(l => l.Strength);

            foreach (TypeIngredient ingredient in sortedIngredients)
            {
                //try to get mapping for pa code
                query = "SELECT bcb_code from dmd_bcb_ingredients_mapping where dmd_code = " + ingredient.Ingredient.Id;
                DataRow result = _bcbDataConnection.GetFirstRow(query);
                Int32 bcbCode = 0;
                if (result != null) bcbCode = Convert.ToInt32(result["bcb_code"]);
                if (bcbCode == 0) Trace.WriteLine(string.Format("Mapping not found for dm+d ingredient {0} ({1})", ingredient.Ingredient.Description, ingredient.Ingredient.Id));
                //need to add ordre field - should be done in order of strength..
                _ingredientsRow.Clear();
                _ingredientsRow.SetValue("ID_PRODUCT", product.DmdCode);
                _ingredientsRow.SetValue("ORDRE", ++order);
                _ingredientsRow.SetValue("COMMENTAIRE", "Added from dm+d");
                _ingredientsRow.SetValue("CODE_PA", bcbCode);
                _ingredientsRow.SetValue("QUANTITE", (float) ingredient.Strength);
                if (ingredient.Unit != null)
                    _ingredientsRow.SetValue("CODE_UNITE", ingredient.Unit.Id);
                _ingredientsRow.SetValue("DMD_CODE", ingredient.Ingredient.Id);
                _ingredientsRow.AppendToTable(command);

                if (ingredient.Unit != null)
                {
                    query = "SELECT * from bcb_etr.compo_pa_unites where \"CODE_UNITE\" = " + ingredient.Unit.Id;
                    result = _bcbDataConnection.GetFirstRow(query);
                    //if (result == null) query = string.Format("INSERT INTO bcb_etr.compo_pa_unites SET \"CODE_UNITE\" = {0} and \"LIBELLE_UNITE\" = {1}", ingredient.Unit.Id, ingredient.Unit.Description);
                    //_bcbDataConnection.;
                    if (result == null)
                    {
                        _unitsRow.Clear();
                        _unitsRow.SetValue("CODE_UNITE", ingredient.Unit.Id);
                        _unitsRow.SetValue("LIBELLE_UNITE", ingredient.Unit.Description);
                        _unitsRow.AppendToTable(command);
                    }
                }
            }
        }

        void WriteExcipients(DmdProductDetails product, List<Concept> excipients)
        {
            //use for ingredients and excipients?
            //get current excipients
            string query = "SELECT * FROM compo_cip_excipients WHERE \"ID_PRODUCT\" = '" + product.DmdCode + "'";
            DataTable table = _bcbDataConnection.GetTable(query);
            int order = table != null ? table.Rows.Count : 0;
            //add current excipient codes to list - ensure not added twice
            List<Int64> authoredExcipients = new List<long>();
            foreach (DataRow row in table.Rows)
                authoredExcipients.Add(Convert.ToInt64(row["CODE_EXCIPIENT"]));
            NpgsqlCommand command = new NpgsqlCommand("", _bcbDataConnection.Connection);
            foreach (Concept excipient in excipients)
            {
                //try to get mapping for pa code
                query = "SELECT bcb_code from dmd_bcb_ingredients_mapping where dmd_code = " + excipient.Id;
                DataRow result = _bcbDataConnection.GetFirstRow(query);
                Int32 bcbCode = 0;
                if (result != null) bcbCode = Convert.ToInt32(result["bcb_code"]);
                if (authoredExcipients.Contains(bcbCode)) 
                    Trace.WriteLine(String.Format("Duplicate excipient ({0}) will not be added to {1}", excipient.Description, product.Name));
                else
                {
                    if (bcbCode == 0) Trace.WriteLine(string.Format("Mapping not found for dm+d ingredient {0} ({1})", excipient.Description, excipient.Id));
                    //need to add order field - should be done in order of strength..
                    _excipientsRow.Clear();
                    _excipientsRow.SetValue("ID_PRODUCT", product.DmdCode);
                    _excipientsRow.SetValue("ORDRE", ++order);
                    _excipientsRow.SetValue("COMMENTAIRE", "Added from dm+d");
                    _excipientsRow.SetValue("CODE_EXCIPIENT", bcbCode);
                    _excipientsRow.SetValue("DMD_CODE", excipient.Id);
                    _excipientsRow.AppendToTable(command);
                }
            }
        }

        void WriteLinkedConcepts(List<Concept> concepts, ConceptDictionary.CommonConcepts hasRelationship, DmdProductDetails product)
        {
            //Debug.WriteLine("METHOD: WriteLinkedConcepts");
            //Debug.Indent();
            if (concepts != null)
            {
                foreach (Concept concept in concepts)
                {
                    DmdProductDetails.Concepts.AddRelationship(product.DmdCode, ConceptCode.ToNum(hasRelationship), concept.Id);
                }
            }
            //Debug.Unindent();
        }

        void WriteRdcDetails(NpgsqlCommand command, TableRow destinationRow, DmdProductDetails product)
        {
            //Debug.WriteLine("METHOD: WriteRdcDetails");
            //Debug.Indent();
            
            if (product.RData.RdcCodes.Count > 0)
            {
                foreach (RdcType rdcCode in product.RData.RdcCodes)
                {
                    destinationRow.Clear();
                    destinationRow.SetValue("drug_class_code", rdcCode.Code);
                    destinationRow.SetValue("drug_code", product.VisionCode);
                    destinationRow.SetValue("main", rdcCode.Main);
                    destinationRow.AppendToTable(command);
                }
            }
            //Debug.Unindent();
        }

        void WritePackDetails(NpgsqlCommand command, TableRow destinationRow, VmpDetails vmp)
        {
            //Debug.WriteLine("METHOD: WritePackDetails(vmp)");
            //Debug.Indent();
            foreach (VmppDetails vmpp in vmp.Vmpps)
            {
                if (!vmpp.IsReconciledGemscriptProduct)
                {
                    destinationRow.SetValue("drug_id", vmp.VisionCode);
                    destinationRow.SetValue("source_id", vmpp.DmdCode);
                    destinationRow.SetValue("is_active", vmpp.IsActive);
                    destinationRow.SetValue("is_replaced", false);
                    destinationRow.SetValue("is_discontinued", vmpp.IsDiscontinued);
                    destinationRow.SetValue("size", vmpp.Size);
                    destinationRow.SetValue("uom", vmpp.UOM);
                    destinationRow.SetValue("price", vmpp.Price);
                    destinationRow.SetValue("price_max", vmpp.MaxPrice);
                    if (vmpp.LegalStatus == null) destinationRow.SetValue("legal", 0);
                    else destinationRow.SetValue("legal", vmpp.LegalStatus);
                    destinationRow.SetValue("order_number", "");
                    destinationRow.SetValue("sub_pack", "");
                    destinationRow.SetValue("is_nurse_formulary", vmpp.IsNurseFormulary);
                    destinationRow.SetValue("is_scottish_nurse_formulary", vmpp.IsScottishNurseFormulary);
                    destinationRow.SetValue("is_divisible", vmpp.IsDivisible);
                    destinationRow.SetValue("price_scotland", GetScotishPricesForDrugPacks(vmpp.DmdCode, vmpp.Price));
                    destinationRow.SetValue("price_wales", vmpp.Price);
                    destinationRow.SetValue("price_ni", vmpp.Price);
                    destinationRow.SetValue("price_max_scotland", GetScotishMaxPricesForDrugPacks(vmpp.DmdCode, vmpp.MaxPrice));
                    destinationRow.SetValue("price_max_wales", vmpp.MaxPrice);
                    destinationRow.SetValue("price_max_ni", vmpp.MaxPrice);
                    destinationRow.AppendToTable(command);
                }
            }
            //Debug.Unindent();
        }

        void WritePackDetails(NpgsqlCommand command, TableRow destinationRow, AmpDetails amp)
        {
            //Debug.WriteLine("METHOD: WritePackDetails(amp)");
            //Debug.Indent();
            foreach (AmppDetails ampp in amp.Ampps)
            {
                destinationRow.SetValue("drug_id", amp.VisionCode);
                destinationRow.SetValue("source_id", ampp.DmdCode);
                destinationRow.SetValue("is_active", ampp.IsActive);
                destinationRow.SetValue("is_replaced", false);
                destinationRow.SetValue("is_discontinued", ampp.IsDiscontinued);
                if (ampp.Vmpp == null)
                {
                    destinationRow.SetValue("size", false);
                    destinationRow.SetValue("uom", null);
                }
                else
                {
                    destinationRow.SetValue("size", ampp.Vmpp.Size);
                    destinationRow.SetValue("uom", ampp.Vmpp.UOM);
                }
                destinationRow.SetValue("price", ampp.Price);
                destinationRow.SetValue("price_max", ampp.Price);
                destinationRow.SetValue("legal", ampp.LegalStatus);
                destinationRow.SetValue("order_number", ampp.OrderNumber);
                destinationRow.SetValue("sub_pack", ampp.SubPack);
                destinationRow.SetValue("is_nurse_formulary", ampp.IsNurseFormulary);
                destinationRow.SetValue("is_scottish_nurse_formulary", ampp.IsScottishNurseFormulary);
                destinationRow.SetValue("is_divisible", ampp.IsDivisible);
                destinationRow.SetValue("price_scotland", GetScotishPricesForDrugPacks(ampp.DmdCode, ampp.Price));
                destinationRow.SetValue("price_wales", ampp.Price);
                destinationRow.SetValue("price_ni", ampp.Price);
                destinationRow.SetValue("price_max_scotland", GetScotishMaxPricesForDrugPacks(ampp.DmdCode, ampp.Price));
                destinationRow.SetValue("price_max_wales", ampp.Price);
                destinationRow.SetValue("price_max_ni", ampp.Price);
                destinationRow.AppendToTable(command);
            }
            //Debug.Unindent();
        }

        void WritePosologyDetails(NpgsqlCommand command, DmdProductDetails product)
        {
            //Debug.WriteLine("METHOD: WritePosologyDetails");
            //Debug.Indent();
            TableRow destinationRow;
            TableDefinition  drugTableDef = new TableDefinition("posology");
            drugTableDef.AddAttribute(new TableAttribute("drug_code", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("supplied_quantity", NpgsqlDbType.Real));
            drugTableDef.AddAttribute(new TableAttribute("quantity_per_day", NpgsqlDbType.Real));
            drugTableDef.AddAttribute(new TableAttribute("period", NpgsqlDbType.Integer));
            drugTableDef.AddAttribute(new TableAttribute("unit", NpgsqlDbType.Bigint));
            drugTableDef.AddAttribute(new TableAttribute("dose", NpgsqlDbType.Varchar));
            drugTableDef.AddAttribute(new TableAttribute("dosage_advice", NpgsqlDbType.Text));
            drugTableDef.AddAttribute(new TableAttribute("specific_dosage_description", NpgsqlDbType.Xml));
            destinationRow = new TableRow(drugTableDef);

            if (product.RData.Posology != null)
            {
                TableUpdater posologyUpdater = new TableUpdater(destinationRow.Definition);
                destinationRow.Clear();
                destinationRow.SetValue("drug_code", product.VisionCode);
                destinationRow.SetValue("supplied_quantity", product.RData.Posology.Value.SuppliedQuantity);
                destinationRow.SetValue("quantity_per_day", product.RData.Posology.Value.DayQuantity);
                destinationRow.SetValue("period", product.RData.Posology.Value.Period);
                destinationRow.SetValue("unit", product.RData.Posology.Value.Unit);
                destinationRow.SetValue("dose", product.Dose);
                destinationRow.SetValue("dosage_advice", product.RData.Posology.Value.DosageAdvice);
                destinationRow.SetValue("specific_dosage_description", product.XmlDosage == null ? null : product.XmlDosage.ToString(System.Xml.Linq.SaveOptions.DisableFormatting));
                try
                {
                    posologyUpdater.AppendToTable(command, destinationRow);
                }
                catch (Exception e)
                {
                    Trace.TraceWarning(string.Format("WritePosologyDetails, Row not added to posology for {0}\n{1}", product.VisionCode, e.Message));
                }
            }
            //Debug.Unindent();
        }

        public void WriteDictionaryChangeDetails()
        {
            //copy product_change details
            /*string query = "INSERT INTO inps_drug.dictionary_change " +
                        "SELECT rec_counter, (SELECT drug_code from inps_drug.drug where dmd_code = id_product limit 1), cast(old_rdc_id as integer), " +
                        "cast(new_rdc_id as integer), dictionary_version, read_code, rec_date from bcb_datas.products_changes";

            _inpsConnection.RunCommand(query);*/
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            string query = "select * from bcb_datas.products_changes";
            DataTable result = _bcbDataConnection.GetTable(query);
            foreach (DataRow row in result.Rows)
            {
                //get drug code mapping - maybe multiple mapping - include all?
                query = string.Format("select drug_code from inps_drug.drug_gemscript where dmd_code = {0} and england = true", row["id_product"]);
                DataTable drugResult = _inpsConnection.GetTable(query);
                if (drugResult.Rows.Count == 0)
                {
                    //check in bcb_inps to see if pack or vtm
                    query = string.Format("select \"ID_PRODUCT_TYPE\" from bcb_inps.products where \"ID_PRODUCT\" = {0} and not(\"ID_PRODUCT_TYPE\" = 2 or \"ID_PRODUCT_TYPE\" = 4)", row["id_product"]);
                    DataRow match = _inpsConnection.GetFirstRow(query);
                    if (match == null)
                        Trace.WriteLine("No mapping found for " + row["id_product"]);
                }
                if (drugResult.Rows.Count > 1)
                {
                    //check if only on active
                    query = string.Format("select drug_code from inps_drug.drug_gemscript where dmd_code = {0} and is_active = true", row["id_product"]);
                    drugResult = _inpsConnection.GetTable(query);
                    if (drugResult.Rows.Count != 1)
                        Trace.WriteLine("Multiple mappings found for " + row["id_product"]);
                }
                foreach (DataRow drug in drugResult.Rows)
                {
                    TableUpdater changesUpdater = new TableUpdater(_dictionaryChangeRow.Definition);
                    _dictionaryChangeRow.Clear();
                    _dictionaryChangeRow.SetValue("rec_counter", row["rec_counter"]);
                    _dictionaryChangeRow.SetValue("drugcode", drug["drug_code"]);
                    _dictionaryChangeRow.SetValue("old_drug_class_code", row["old_rdc_id"].ToString().Trim() == "" ? null : row["old_rdc_id"]);
                    _dictionaryChangeRow.SetValue("new_drug_class_code", row["new_rdc_id"].ToString().Trim() == "" ? null : row["new_rdc_id"]);
                    _dictionaryChangeRow.SetValue("version", row["dictionary_version"]);
                    _dictionaryChangeRow.SetValue("read_code", row["read_code"]);
                    _dictionaryChangeRow.SetValue("change_date", row["rec_date"]);
                    if (!changesUpdater.AppendToTable(command, _dictionaryChangeRow)) Trace.WriteLine("Could not add dictionary change row " + drug["drug_code"]);
                }
            }
        }

        void WriteControlStatus(NpgsqlCommand command, DmdProductDetails product)
        {
            TableUpdater statusUpdater = new TableUpdater(_controlStatusRow.Definition);
            _controlStatusRow.Clear();
            _controlStatusRow.SetValue("drug_code", product.VisionCode);
            _controlStatusRow.SetValue("allergy", product.RData.ControlStatus.Value.Ingredients == 1);
            _controlStatusRow.SetValue("doubling", product.RData.ControlStatus.Value.Ingredients == 1 && product.RData.ControlStatus.Value.RDC == 1
                                        && product.RData.RdcCodes.Count > 0);
            _controlStatusRow.SetValue("contraindication", product.RData.ControlStatus.Value.ContraIndication == 1);
            _controlStatusRow.SetValue("precaution", product.RData.ControlStatus.Value.Precautions == 1);
            _controlStatusRow.SetValue("prescriber_warning", product.RData.ControlStatus.Value.PrescriberWarnings == 1);
            _controlStatusRow.SetValue("interaction", product.IsInteractionControlled);
            try
            {
                statusUpdater.AppendToTable(command, _controlStatusRow);
            }
            catch (Exception e)
            {
                Trace.TraceWarning(string.Format("WriteControlStatus, Row not added to posology for {0}\n{1}", product.VisionCode, e.Message));
            }
        }

        void WriteLabelCautions(NpgsqlCommand command, DmdProductDetails product)
        {
            string s1;
            if (product.RData.LabelCautions != null)
            {
                foreach (string s in product.RData.LabelCautions)
                {
                    if (!s.StartsWith(DEFAULT_LABEL_WARNING))
                    {
                        Debug.WriteLine("Caution: " + s);
                        //check if id already exists
                        string query;
                        if (s.Contains("'"))
                            s1 = s.Replace("'", "''");
                        else s1 = s;
                        query = "SELECT caution_id FROM cautions where caution like '" + s1 + "'";
                        DataRow result = _inpsConnection.GetFirstRow(query);
                        if (result == null)
                        {
                            TableUpdater cautionsUpdater = new TableUpdater(_cautionRow.Definition);
                            _cautionRow.Clear();
                            _cautionRow.SetValue("caution", s);
                            cautionsUpdater.AppendToTable(command, _cautionRow);
                            result = _inpsConnection.GetFirstRow("SELECT max(caution_id) from inps_drug.cautions");
                        }
                        //error when same caution entered twice for product
                        query = "SELECT * from drug_cautions where caution_id =" + result[0].ToString() + " and  drug_code = " + product.VisionCode;
                        if (_inpsConnection.GetFirstRow(query) == null)
                        {
                            TableUpdater drugCautionsUpdater = new TableUpdater(_drugCautionRow.Definition);
                            _drugCautionRow.Clear();
                            _drugCautionRow.SetValue("drug_code", product.VisionCode);
                            _drugCautionRow.SetValue("caution_id", Convert.ToInt32(result[0]));
                            drugCautionsUpdater.AppendToTable(command, _drugCautionRow);
                        }
                        else Trace.WriteLine(DateTime.Now + String.Format(" Caution '{0}' is repeated for drug code {1}", result[0].ToString(), product.VisionCode));
                    }
                }
            }
            else if (product is AmpDetails && product.IsBrandedGeneric) //get warnings for VMP if AMP has no warnings of its own
            {
                foreach (string s in product.Vmp.RData.LabelCautions)
                {
                    if (!s.StartsWith(DEFAULT_LABEL_WARNING))
                    {
                        //check if id already exists
                        string query = "SELECT caution_id FROM cautions where caution = $$" + s + "$$";
                        DataRow result = _inpsConnection.GetFirstRow(query);
                        if (result == null)
                        {
                            TableUpdater cautionsUpdater = new TableUpdater(_cautionRow.Definition);
                            _cautionRow.Clear();
                            _cautionRow.SetValue("caution", s);
                            cautionsUpdater.AppendToTable(command, _cautionRow);
                            result = _inpsConnection.GetFirstRow("SELECT max(caution_id) from inps_drug.cautions");
                        }
                        //error when same caution entered twice for product
                        query = "SELECT * from drug_cautions where caution_id =" + result[0].ToString() + " and  drug_code = " + product.VisionCode;
                        if (_inpsConnection.GetFirstRow(query) == null)
                        {
                            TableUpdater drugCautionsUpdater = new TableUpdater(_drugCautionRow.Definition);
                            _drugCautionRow.Clear();
                            _drugCautionRow.SetValue("drug_code", product.VisionCode);
                            _drugCautionRow.SetValue("caution_id", Convert.ToInt32(result[0]));
                            drugCautionsUpdater.AppendToTable(command, _drugCautionRow);
                        }
                        else Trace.WriteLine(DateTime.Now + String.Format(" Caution '{0}' is repeated for drug code {1}", result[0].ToString(), product.VisionCode));
                    }
                }
            }
        }

        public void WriteShortName(NpgsqlCommand command, DmdProductDetails product)
        {
            TableUpdater termUpdater = new TableUpdater(_drugTermRow.Definition);
            TableUpdater drugUpdater = new TableUpdater(_drugRow.Definition);

            if (product.RData.ShortName != null && product.RData.ShortName != "")
            {
                UInt32 drugCode = product.VisionCode;
                string labelTerm = product.RData.ShortName;
                uint min, max;
                min = 4001;
                max = 5000;

                string query = String.Format("SELECT * FROM gemscript_drug_term_table WHERE drug_code = {0} and term_text = $${1}$$ and term_id >= {2} and term_id < {3}", 
                    product.VisionCode.ToString(), product.RData.ShortName, min.ToString(), max.ToString());
                DataRow termRow = _inpsConnection.GetFirstRow(query);
                uint nextTermId = 0;
                if (termRow == null)
                {
                    //need new drug term
                    // Get next term id
                    nextTermId = min;
                    query = String.Format(@"select max(term_id) as max_id from gemscript_drug_term_table where drug_code = {0} and term_id >= {1} and term_id <{2}",
                        drugCode.ToString(), min.ToString(), max.ToString());
                    DataRow maxCodeRow = _inpsConnection.GetFirstRow(query);
                    if (maxCodeRow != null && !maxCodeRow.IsNull("max_id"))
                    {
                        nextTermId = Convert.ToUInt32(maxCodeRow["max_id"]) + 1;
                    }

                    // Append data
                    _drugTermRow.Clear();
                    _drugTermRow.SetValue("drug_code", drugCode);
                    _drugTermRow.SetValue("term_id", nextTermId);
                    _drugTermRow.SetValue("shortened", false);
                    _drugTermRow.SetValue("term_text", labelTerm);
                    termUpdater.AppendToTable(command, _drugTermRow);
                }
                else
                {
                    nextTermId = Convert.ToUInt32(termRow["term_id"]);
                }

                // Write code to drug record
                //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Adding term details for " + code);
                drugUpdater.WhereClause.Clear();
                drugUpdater.WhereClause.SetValue("drug_code", drugCode);
                _drugRow.Clear();
                _drugRow.SetValue("label_term_code", nextTermId);
                drugUpdater.UpdateInTable(command, _drugRow);
            }
        }

        public void WriteTermDetails()
        {
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + "METHOD: WriteTermDetails");
            Debug.Indent();
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            string query = @"select drug_code, name from drug_gemscript order by search_key, is_dmd desc, form, normalised_strength, dmd_type, is_fp10 desc, is_issuable desc, is_available desc, name";
            DataTable result = _inpsConnection.GetTable(query);

            TableUpdater termUpdater = new TableUpdater(_drugTermRow.Definition);
            TableUpdater drugUpdater = new TableUpdater(_drugRow.Definition);
            _drugRow.Clear();
            uint orderKey = 0;
            try
            {
                foreach (DataRow row in result.Rows)
                {
                    string name = row["name"].ToString();
                    uint nextTermId = 0;
                    uint code = Convert.ToUInt32(row["drug_code"]);
                    //use dollar quoted string constant to handle special characters such as '
                    query = String.Format(@"select * from gemscript_drug_term_table where drug_code = {0} and term_text = $${1}$$ and term_id < 4000 order by term_id desc", code.ToString(), name);
                    DataRow existingDrugTermRow = _inpsConnection.GetFirstRow(query);
                    //check if has Multilex short name - can remove after one build
                    bool newTermRequired = false;
                    if (existingDrugTermRow != null && name.Length > 60 && !(code.ToString().EndsWith("020") || code.ToString().EndsWith("007")))
                    {
                        //check existing short name
                        query = String.Format(@"select term_text from gemscript_drug_term_table where drug_code = {0} and term_id = {1} and shortened = true", code.ToString(), Convert.ToUInt32(existingDrugTermRow["term_id"]));
                        DataRow shortNameRow = _inpsConnection.GetFirstRow(query);
                        if (shortNameRow["term_text"].ToString().Length <= 57 || shortNameRow["term_text"].ToString().Substring(0, 57) != name.Substring(0, 57))
                            newTermRequired = true;
                    }
                    if (existingDrugTermRow == null || newTermRequired)
                    {
                        // Add new row...

                        // Get next term id
                        bool longName = name.Length > 60;
                        uint min, max;
                        if (longName)
                        {
                            min = 3001;
                            max = 4000;
                        }
                        else
                        {
                            min = 1001;
                            max = 2000;
                        }

                        nextTermId = min;
                        query = String.Format(@"select max(term_id) as max_id from gemscript_drug_term_table where drug_code = {0} and term_id >= {1} and term_id <{2}", code.ToString(), min.ToString(), max.ToString());
                        DataRow maxCodeRow = _inpsConnection.GetFirstRow(query);
                        if (maxCodeRow != null && !maxCodeRow.IsNull("max_id"))
                        {
                            nextTermId = Convert.ToUInt32(maxCodeRow["max_id"]) + 1;
                        }

                        // Append data
                        _drugTermRow.Clear();
                        _drugTermRow.SetValue("drug_code", code);
                        _drugTermRow.SetValue("term_id", nextTermId);
                        _drugTermRow.SetValue("shortened", false);
                        _drugTermRow.SetValue("term_text", name);
                        termUpdater.AppendToTable(command, _drugTermRow);
                        if (longName)
                        {
                            _drugTermRow.SetValue("shortened", true);
                            _drugTermRow.SetValue("term_text", name.Substring(0, 57) + "...");
                            termUpdater.AppendToTable(command, _drugTermRow);
                        }
                    }
                    else
                    {
                        nextTermId = Convert.ToUInt32(existingDrugTermRow["term_id"]);
                    }

                    // Write code to drug record
                    //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Adding term details for " + code);
                    drugUpdater.WhereClause.Clear();
                    drugUpdater.WhereClause.SetValue("drug_code", code);
                    _drugRow.SetValue("term_code", nextTermId.ToString().PadLeft(4, '0'));
                    _drugRow.SetValue("order_key", ++orderKey);
                    drugUpdater.UpdateInTable(command, _drugRow);
                }
            }
            catch (Exception ex)
            {
                _errorText = ex.Message;
            }
            finally
            {
                Debug.Unindent();
            }
        }

        public void WriteDictionaryDetails(Dictionary<String, String> version)
        {
            //Debug.WriteLine("METHOD: WriteDictionaryDetails");
            //Debug.Indent();
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            //need to reset previous entry current column to false
            
            //Set during ImportNexphaseData
            //_dictionaryRow.SetValue("np_data_version", "");
            //_dictionaryRow.SetValue("np_struct_version", "");
            if (version["icf"] != null && version["icf"]!= "")
                _dictionaryRow.SetValue("min_icf", version["icf"]);
            _dictionaryRow.SetValue("icd10_version", version["icd10"]);
            _dictionaryRow.AppendToTable(command);
            //write dictionary line to CSV
            string directory = releasePath + @"\Support Files";
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            FileStream file = new FileStream(directory + @"\DictVer.csv", FileMode.Create);
            string query = "select version_id, live, build_date, inps_version, dmd_version, api_version, np_data_version, np_struct_version, icd10_version, min_icf from drug_dictionary.drug_dictionary order by version_id DESC limit 1";
            CopyData copy = new CopyData(_inpsConnection.Connection);
            copy.CopyOutCsvFormat(query, file);
        }

        public string GetMinICF() 
        {
            string query = @"select min_icf from drug_dictionary.drug_dictionary order by version_id DESC limit 1";
            DataRow result = _inpsConnection.GetFirstRow(query);
            if (result != null)
                return Convert.ToString(result["min_icf"]);
            return "";
        }

        public string GetICD10()
        {
            string query = @"select icd10_version from drug_dictionary.drug_dictionary order by version_id DESC limit 1";
            DataRow result = _inpsConnection.GetFirstRow(query);
            if (result != null)
                return Convert.ToString(result["icd10_version"]);
            return "";
        }

        public void PopulateDrugInpsTable()
        {
            String query = "DELETE FROM drug_inps_table";
            NpgsqlCommand command = new NpgsqlCommand(query, _inpsConnection.Connection);
            command.ExecuteNonQuery();
            //insert header row
            string buildDate = Convert.ToString(_dictionaryRow.Values["build_date"]);
            buildDate = buildDate.Substring(6, 4) + buildDate.Substring(3, 2) + buildDate.Substring(0, 2);
            string inpsVersion = Convert.ToString(_dictionaryRow.Values["inps_version"]);
            string dmdVersion = Convert.ToString(_dictionaryRow.Values["dmd_version"]);
            query = string.Format("INSERT INTO drug_inps_table SELECT '{0}' AS version , " +
                        "'00000101' AS drug_code, " +
                        "'{1}' AS read_code, " +
                        "'{2}' AS name, " +
                        "'N' AS suppress, " +
                        "'ver' AS form,  " +
                        "0.0 AS strength, " +
                        "0.0 AS drug_effec,	" +
                        "'N' AS fp10, " +
                        "'Y' AS discontinu, " +
                        "'Y' AS isgeneric, " +
                        "'0' AS gen_code, " +
                        "'Y' AS controlled, " +
                        "'N' AS use_generi, " +
                        "'N' AS use_brand, " +
                        "'N' AS doh_approv, " +
                        "'N' AS special, " +	//careful. This field has changed meaning in the past. Do not use inps_drug.drug.special
                        "'N' AS non_print, " + //probably the closest value possible considering the legacy implementation
                        "'N' AS acbs, " +
                        "'N' AS triangle, " +
                        "'N' AS oneitem, " + //Not supported for a long while
                        "'N' AS contracept, " +
                        "'N' AS external, " + //Not supported in Drug.dbf since Gemscript
                        "'N' AS practice, " +
                        "'N' AS oldvamp, " + //Not really relevant anymore
                        "'00000000' AS bnf_code, " +
                        "'0000' AS formulary, " + //Not supported in Drug.dbf since Gemscript
                        "'' AS ordno, " +
                        "'Y' AS main, " +
                        "'' AS fasttrack, " +	//CALLED FASTKEY ON MY DBF - Not used for a long while.
                        "'' AS keyword, " +	//Not used for a long while
                        "'0' AS namelen, " +
                        "'{3}' AS nameorder, " +
                        "'0000' AS term_code, " +
                        "'0' AS schedule, " +
                        "'0000' AS scheme, " +	//Not supported in Drug.dbf since Gemscript
                        "'N' AS toxic, " +
                        "LPAD('0',11,'0') as dmd_code, " +
                         "'' AS validfrom, " +
                        "'' AS validuntil ", //needs to be convert to b64 - prac_live_999.dmdTob64(d.dmd_code) AS "dmd_code"	
                        buildDate, dmdVersion, "_DRUG DICTIONARY VERSION CONTROL " + buildDate, inpsVersion);
            command = new NpgsqlCommand(query, _inpsConnection.Connection);
            command.ExecuteNonQuery();

            query = "INSERT INTO drug_inps_table SELECT '20140101' AS version , " +
                        "CASE WHEN d.drug_code = 2 THEN 'ZZZZZZZZ' WHEN d.drug_code = 5 THEN '00000000' ELSE LPAD(d.drug_code::text, 8, '0') END AS drug_code, " +
                        "d.read_code, " +
                        "d.name, " +
                        "CASE WHEN (NOT d.is_active) THEN 'D' ELSE (CASE WHEN (d.is_active AND d.is_branded_generic) THEN 'Y' ELSE 'N' END) END AS suppress, " +
                        "d.form,  " +
                        "CAST(d.strength AS double precision) AS strength, " +
                        "CAST(0 AS double precision) AS drug_effec,	" +
                        "CASE WHEN d.is_fp10 THEN 'Y' ELSE 'N' END AS fp10, " +
                        "CASE WHEN d.is_discontinued THEN 'Y' ELSE 'N' END AS discontinu, " +
                        "CASE WHEN d.is_generic THEN 'Y' ELSE 'N' END AS isgeneric, " +
                        "LPAD(d.vmp_code::text,8,'0') AS gen_code, " +
                        "CASE WHEN d.is_controlled THEN 'Y' ELSE 'N' END AS controlled, " +
                        "CASE WHEN d.use_generic THEN 'Y' ELSE 'N' END AS use_generi, " +
                        "CASE WHEN d.use_brand THEN 'Y' ELSE 'N' END AS use_brand, " +
                        "CASE WHEN d.is_doh_approved THEN 'Y' ELSE 'N' END AS doh_approv, " +
                        "CASE WHEN d.is_sls THEN 'Y' ELSE 'N' END AS special, " +	//careful. This field has changed meaning in the past. Do not use inps_drug.drug.special
                        "CASE WHEN d.is_issuable THEN 'N' ELSE 'Y' END AS non_print, " + //probably the closest value possible considering the legacy implementation
                        "CASE WHEN d.is_acbs THEN 'Y' ELSE 'N' END AS acbs, " +
                        "CASE WHEN d.is_triangle THEN 'Y' ELSE 'N' END AS triangle, " +
                        "'N' AS oneitem, " + //Not supported for a long while
                        "CASE WHEN d.is_contraception THEN 'Y' ELSE 'N' END AS contracept, " +
                        "'N' AS external, " + //Not supported in Drug.dbf since Gemscript
                        "CASE WHEN d.is_pa THEN 'Y' ELSE 'N' END AS practice, " +
                        "CASE WHEN (d.drug_code % 1000 = 7) THEN 'Y' ELSE 'N' END AS oldvamp, " + //Not really relevant anymore
                        "CASE WHEN (dc.drug_class_code IS NULL) THEN '00000000' ELSE LPAD(dc.drug_class_code::text,8,'0') END AS bnf_code, " +
                        "'0000' AS formulary, " + //Not supported in Drug.dbf since Gemscript
                        "d.order_number AS ordno, " +
                        "CASE WHEN dc.main IS NULL THEN 'Y' WHEN dc.main THEN 'Y' else 'N' END AS main, " +
                        "'' AS fasttrack, " +	//CALLED FASTKEY ON MY DBF - Not used for a long while.
                        "'' AS keyword, " +	//Not used for a long while
                        "LENGTH(d.search_key) AS namelen, " +
                        "SUBSTRING(d.search_key FROM 1 FOR 60) AS nameorder, " +
                        "d.term_code::int AS term_code, " +
                        "d.schedule::char AS schedule, " +
                        "'0000' AS scheme, " +	//Not supported in Drug.dbf since Gemscript
                        "CASE WHEN (d.high_risk_warning IS NOT NULL) THEN 'Y' ELSE 'N' END AS toxic, " +
                        "LPAD(functions.tob64(dmd_code),11,'0') AS dmd_code, " + //needs to be convert to b64 - prac_live_999.dmdTob64(d.dmd_code) AS "dmd_code"	
                        "'' AS validfrom, " +
                        "'' AS validuntil " +
                        "FROM drug_gemscript d LEFT OUTER JOIN drug_class_mapping dc ON (d.drug_code=dc.drug_code) " +
                        "WHERE england = true;";
            command = new NpgsqlCommand(query, _inpsConnection.Connection);
            command.ExecuteNonQuery();
           
            //add b64 dmd_code
            //pad with '0' to 11 characters
            //add C# generated dm+d code just for initial check..
            /*query = "SELECT DISTINCT(drug_code), dmd_code FROM drug d";
            DataTable table = _inpsConnection.GetTable(query);
            foreach (DataRow row in table.Rows)
            {
                String b64DmdCode = INPS.V3.Multilex.Converter.getB64FromB10(Convert.ToInt64(row["dmd_code"])).ToString().PadLeft(11, '0');
                query = String.Format("UPDATE dictionary.drug_table SET dmd_code_gen = '{0}' WHERE drug_code = '{1}'", b64DmdCode, row["drug_code"].ToString().PadLeft(8, '0'));
                command = new NpgsqlCommand(query, _inpsConnection.Connection);
                command.ExecuteNonQuery();
            }*/
        }


        public void ImportNexphaseData()
        {
            Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Importing NexphaseData data");
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);           
            try
            {
                string directory = releasePath + @"\" + _dictionaryRow.Values["inps_version"].ToString().Substring(0, 7) + @"\Nexphase\";
                //check version
                //get np dm+d version data
                StreamReader npDmdReader = new StreamReader(directory + "/MASDMDVersion.tab");
                string line, npDmdVersion = "";
                while ((line = npDmdReader.ReadLine()) != null) 
                    npDmdVersion = line.Substring(0, line.IndexOf("\t"));
                npDmdReader.Close();
                string dmdVersion = _dictionaryRow.Values["dmd_version"].ToString();
                if (!npDmdVersion.Equals(dmdVersion)) Trace.WriteLine(DateTime.Now + String.Format(" WARNING: dm+d version used in NexPhase data is {0} and version in bcb data is {1}", npDmdVersion, dmdVersion));
                
                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Populating np tables");
                
                //load data direct from csv
                 foreach (String tableName in new[]{"np_products", "np_interactions", "np_results"})
                {
                    command.CommandText = String.Format("DELETE FROM {0}", tableName);
                    command.ExecuteNonQuery();

                    StringBuilder queryBuilder = new StringBuilder();
                    var file = new FileStream(directory + "/" + tableName + ".csv", FileMode.Open);
                    MemoryStream ms = new MemoryStream();
                    StreamWriter writer = new StreamWriter(ms);
                    using (StreamReader rdr = new StreamReader(file, Encoding.GetEncoding(1252)))
                    {
                        int i = 0;
                        while (!rdr.EndOfStream)
                        {
                            string s = rdr.ReadToEnd();
                            //remove header row
                            s = s.Substring(s.IndexOf("\n") + 1);
                            writer.Write(s);
                            writer.Flush();
                            i++;
                        }
                        file.Close();
                    }
                    ms.Seek(0, SeekOrigin.Begin);
                    //load data from file
                    CopyData copy = new CopyData(_inpsConnection.Connection);
                    copy.CopyIn(tableName, ms, tableName == "np_products" ? new[]{"dmd_code","family_code"}: null);
                }
                //write entry to dictionary?
                 StreamReader npVersionReader = new StreamReader(directory + "TRADBMasterDataVersion.tab");
                while ((line = npVersionReader.ReadLine()) != null)
                {
                    _dictionaryRow.SetValue("np_data_version", line.Split('\t')[1]);
                    _dictionaryRow.SetValue("np_struct_version", line.Split('\t')[2]);
                }
                npVersionReader.Close();
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void addVMPNexphaseDataToAMPs()
        {   
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            command.CommandText = "SELECT setval('inps_drug.np_products_id_seq', (SELECT MAX(id) FROM inps_drug.np_products));";
            command.ExecuteNonQuery();
            command.CommandText = "ALTER TABLE inps_drug.np_products ALTER COLUMN id SET DEFAULT nextval('inps_drug.np_products_id_seq');";
            command.ExecuteNonQuery();
            command.CommandText = "INSERT INTO inps_drug.np_products (dmd_code, family_code) " +
                                    "(" +
                                         "SELECT DISTINCT d_not_in_inter.dmd_code, gen_fam.family_code " +
                                         "FROM " +
                                         "(" +
                                               "SELECT dg.drug_code, dg.dmd_code " +
                                               "FROM inps_drug.drug_gemscript dg " +
                                               "WHERE dg.is_generic " +
                                               "AND EXISTS " +
                                               "(" +
                                                    "SELECT * " +
                                                    "FROM inps_drug.np_products n1 " +
                                                    "WHERE n1.dmd_code = dg.dmd_code " +
                                               ")" +
                                         ") as d_gen_in_inter " +
                                         "INNER JOIN " +
                                         "(" +
                                               "SELECT d1.dmd_code, d1.vmp_code " +
                                               "FROM inps_drug.drug_gemscript d1 " +
                                               "WHERE NOT EXISTS " +
                                               "(" +
                                                    "SELECT * " +
                                                    "FROM inps_drug.np_products n1 " +
                                                    "WHERE n1.dmd_code = d1.dmd_code" +
                                               ")" +
           
                                         ") AS d_not_in_inter ON d_gen_in_inter.drug_code = d_not_in_inter.vmp_code " +
                                         "INNER JOIN " +
                                         "(" +
                                               "SELECT dmd_code, family_code " +
                                               "FROM inps_drug.np_products " +
                                         ") AS gen_fam ON d_gen_in_inter.dmd_code = gen_fam.dmd_code" +
                                    ");";
            command.ExecuteNonQuery();
        }

        public void AddLabelTerms()
        {
            //add label tems where not imported from authored data
            TableUpdater termUpdater = new TableUpdater(_drugTermRow.Definition);
            TableUpdater drugUpdater = new TableUpdater(_drugRow.Definition);
            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            string query = "SELECT dt.* FROM drug_gemscript d JOIN gemscript_drug_term_table dt ON d.drug_code = dt.drug_code AND CAST(d.term_code AS integer) = dt.term_id WHERE label_term_code IS NULL AND (dt.term_id < 2001 OR dt.shortened = true)";
            DataTable data = _inpsConnection.GetTable(query);
            foreach (DataRow row in data.Rows)
            {
                Int32 drugCode = Convert.ToInt32(row["drug_code"]);
                string labelTerm = row["term_text"].ToString();
                if (labelTerm.Length > 60)
                    Debug.WriteLine("label too long " + labelTerm);
                uint min, max;
                min = 4001;
                max = 5000;

                query = String.Format("SELECT * FROM gemscript_drug_term_table WHERE drug_code = {0} and term_text = $${1}$$ and term_id >= {2} and term_id <{3}", drugCode.ToString(), labelTerm, min.ToString(), max.ToString());
                DataRow termRow = _inpsConnection.GetFirstRow(query);
                uint nextTermId = 0;
                if (termRow == null)
                {
                    //need new drug term
                    // Get next term id
                    nextTermId = min;
                    query = String.Format(@"select max(term_id) as max_id from gemscript_drug_term_table where drug_code = {0} and term_id >= {1} and term_id <{2}",
                        drugCode.ToString(), min.ToString(), max.ToString());
                    DataRow maxCodeRow = _inpsConnection.GetFirstRow(query);
                    if (maxCodeRow != null && !maxCodeRow.IsNull("max_id"))
                    {
                        nextTermId = Convert.ToUInt32(maxCodeRow["max_id"]) + 1;
                    }

                    // Append data
                    _drugTermRow.Clear();
                    _drugTermRow.SetValue("drug_code", drugCode);
                    _drugTermRow.SetValue("term_id", nextTermId);
                    _drugTermRow.SetValue("shortened", false);
                    _drugTermRow.SetValue("term_text", labelTerm);
                    termUpdater.AppendToTable(command, _drugTermRow);
                }
                else
                {
                    nextTermId = Convert.ToUInt32(termRow["term_id"]);
                }

                // Write code to drug record
                //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Adding term details for " + code);
                drugUpdater.WhereClause.Clear();
                drugUpdater.WhereClause.SetValue("drug_code", drugCode);
                _drugRow.Clear();
                _drugRow.SetValue("label_term_code", nextTermId);
                drugUpdater.UpdateInTable(command, _drugRow);
            }
        }

        public void CreatePreparationList()
        {            
            string directory = releasePath + "//" + Convert.ToString(_dictionaryRow.Values["inps_version"]);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            FileStream file = new FileStream(directory + @"\preparations.csv", FileMode.Create);
            string query = "SELECT DISTINCT LPAD(p.drug_id::text,8,'0'), c.description FROM inps_drug.pack p INNER JOIN inps_drug.concepts c ON p.uom=c.id WHERE p.is_active=true AND p.is_replaced=false"; 
            CopyData copy = new CopyData(_inpsConnection.Connection);
            copy.CopyOutCsvFormat(query, file);
            File.SetAttributes(directory + @"\preparations.csv", FileAttributes.ReadOnly);
        }

        public void CreateCodeList()
        {
            string directory = releasePath + "//" + Convert.ToString(_dictionaryRow.Values["inps_version"]);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            FileStream file = new FileStream(directory + @"\gemscript_drugs.csv", FileMode.Create);
            string query = "SELECT DISTINCT LPAD(drug_code::text,8,'0'), name, dmd_code, read_code, LPAD(vmp_code::text,8,'0') as gem_vmp_code FROM drug_gemscript";
            CopyData copy = new CopyData(_inpsConnection.Connection);
            copy.CopyOutPipelineFormat(query, file);
            File.SetAttributes(directory + @"\gemscript_drugs.csv", FileAttributes.ReadOnly);
        }

        public void CreateRecordCount()
        {
            string query = "SELECT COUNT(1) FROM inps_drug.drug_gemscript";
            var dataRow = _inpsConnection.GetFirstRow(query);
            

            string directory = releasePath + @"\Support Files";
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            string[] lines = { "[REC_COUNT]", "inps_drug.drug_gemscript=" + dataRow["count"].ToString() };
            File.SetAttributes(directory + @"\rec_count.ini", FileAttributes.Normal);
            File.WriteAllLines(directory + @"\rec_count.ini", lines);
            File.SetAttributes(directory + @"\rec_count.ini", FileAttributes.ReadOnly);
        }

        public void CreateDrugGemCSV()
        {
            string directory = releasePath + "//" + Convert.ToString(_dictionaryRow.Values["inps_version"]);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            Debug.WriteLine("Create drug_gem.csv at " + directory);
            //DbfAccessor duAccessor = new DbfAccessor(directory + "/Vision/install/data");
            //string query = "select * from drug_gem";
            String query = "SELECT * FROM drug_inps_table";
            DataTable data = _inpsConnection.GetTable(query);
            //DataTable data = duAccessor.getData(query);
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(directory + "/drug_gem_new.csv", false);
                // Write the headers. NB ignore checksum column (first column)
                int iColCount = data.Columns.Count;

                for (int i = 1; i < iColCount; i++)
                {
                    sw.Write(data.Columns[i].ToString().ToUpper());
                    if (i < iColCount - 1) sw.Write(",");
                }
                sw.Write(sw.NewLine);
                // Write rows.
                foreach (DataRow row in data.Rows)
                {
                    for (int i = 1; i < iColCount; i++)
                    {
                        if (!Convert.IsDBNull(row[i]))
                        {
                            if (row[i].ToString().Contains(','))
                                sw.Write("\"" + row[i].ToString().Trim() + "\"");
                            else sw.Write(row[i].ToString().Trim());
                        }
                        if (i < iColCount - 1) sw.Write(",");
                    }
                    sw.Write(sw.NewLine);
                    sw.Flush();
                }
               
            }
            catch (Exception e)
            {
                Trace.WriteLine("Error : " + e.StackTrace);
            }
            finally
            {
                if (sw != null) sw.Close();
                if (File.Exists(directory + "/drug_gem_new.csv"))
                    File.SetAttributes(directory + "/drug_gem_new.csv", FileAttributes.ReadOnly);
            }
        }

        public void CreateDrugMapXml()
        {
            //maps Gemscript code to dm+d code - to be used as lookup for xsl transforms
            XNamespace ns = "www.inps.co.uk";
            XDocument xml = new XDocument(new XElement(ns + "drugMap"));
            string directory = releasePath + "//" + Convert.ToString(_dictionaryRow.Values["inps_version"]) + @"\Vision\install\data\xml\Resources";
            string query = "SELECT DISTINCT ON (drug_code, dmd_code) drug_code, dmd_code FROM drug_gemscript ORDER BY drug_code";
            DataTable data = _inpsConnection.GetTable(query);
            XElement root = xml.Root;
            foreach (DataRow row in data.Rows)
                root.Add(new XElement(ns + "drug", new XAttribute("drug_code", row[0]), new XAttribute("dmd_code", row[1])));
            if (!Directory.Exists(directory)) Directory.CreateDirectory(directory );
            xml.Save(directory + @"\drug_map.xml", SaveOptions.DisableFormatting);
            File.SetAttributes(directory + @"\drug_map.xml", FileAttributes.ReadOnly);
        }

        public void ImportAuthoredNames()
        {
            //update lex-drug table will be enough for changes carried through with drug terms added?
           NpgsqlCommand command = new NpgsqlCommand("", _lexConnection.Connection);
            StreamReader file = new StreamReader(releasePath + @"\Reference\Multilex\drugdata.csv");
            try
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    //remove comma from quoted sections so comma can be used to split line
                    if (line.Contains("\""))
                    {
                        string TmpLine = "";
                        string NewLine = "";
                        bool swap = false;
                        int first = 0;
                        int last = 0;
                        for (int n = 0; n < line.Length; n++)
                        {
                            if (line.Substring(n, 1) == "\"" && swap == true)
                            {
                                swap = false;
                                last = n; //next instance
                                TmpLine = line.Substring(first, (last - first) + 1);//get the quoted section
                                NewLine = TmpLine.Replace(",", "~");
                                line = line.Replace(TmpLine, NewLine);
                                n++; //skip and extra 1
                            }

                            if (n < line.Length && line.Substring(n, 1) == "\"" && swap == false)
                            {
                                swap = true;
                                first = n; //first instance
                            }
                        }
                    }
                    string[] p = line.Split(new Char[] { ',' });
                    string drugcode = p[0].PadLeft(8, '0');
                    string currentDrugName = p[1].Replace('~', ',').Trim('"');
                    string newDrugName = p[2].Replace('~', ',').Trim('"');

                    command.CommandText = string.Format("UPDATE drug_all SET name = $${0}$$ WHERE name = $${1}$$ and drugcode = $${2}$$", newDrugName, currentDrugName, drugcode);
                    int i = command.ExecuteNonQuery();
                    command.CommandText = string.Format("UPDATE drug_gemscript SET name = $${0}$$ WHERE name = $${1}$$ and drugcode = $${2}$$", newDrugName, currentDrugName, drugcode);
                    int j = command.ExecuteNonQuery();
                    Trace.WriteLine(string.Format("Old name {0} changed to {1} in {2} cases in drug_all and {3} in drug", currentDrugName, newDrugName, i, j));
                }
            }
            finally
            {
                file.Close();
            }

        }

        public void ImportMultilexIngredients()
        {
            Trace.WriteLine("WriteMlexIngredients");
            NpgsqlCommand command = new NpgsqlCommand("", _bcbDataConnection.Connection);
            StreamReader file = new StreamReader(releasePath + @"\Reference\Multilex\MLexIngredients.csv");
            string line;
            string query = "DELETE FROM historical_drug.historical_ingredients";
            _lexConnection.RunCommand(query);
            try
            {  
                List<string[]> lines = new List<string[]>();
                List<string> exceptions = new List<string>();
                exceptions.Add("No active ingredients");
                exceptions.Add("None coded");
                exceptions.Add("None known");
                exceptions.Add("Homeopathic remedy");
                while ((line = file.ReadLine()) != null)
                {
                    Trace.WriteLine("MLEX ingredent: " + line);
                    //remove comma from quoted sections so comma can be used to split line
                    if (line.Contains("\""))
                    {
                        string TmpLine = "";
                        string NewLine = "";
                        bool swap = false;
                        int first = 0;
                        int last = 0;
                        for (int n = 0; n < line.Length; n++)
                        {
                            if (line.Substring(n, 1) == "\"" && swap == true)
                            {
                                swap = false;
                                last = n; //next instance
                                TmpLine = line.Substring(first, (last - first) + 1);//get the quoted section
                                NewLine = TmpLine.Replace(",", "~");
                                line = line.Replace(TmpLine, NewLine);
                                n++; //skip and extra 1
                            }

                            if (line.Substring(n, 1) == "\"" && swap == false)
                            {
                                swap = true;
                                first = n; //first instance
                            }
                        }
                    }
                    
                    string[] p = line.Split(new Char[] { ',' });
                    Int32 drugcode;
                    if (Int32.TryParse(p[0], out drugcode))
                    {
                        //mapped if bcb_code != 0 or if ingredient name one of listed exception
                        bool mapped = p[2].Length > 0 || exceptions.Contains(p[1]);
                        query = string.Format("INSERT INTO historical_drug.historical_ingredients VALUES({0}, {1}, {2}, {3})", drugcode, p[2].Length > 0 ? p[2] : "0", p[4].Length > 0 ? p[4] : "0", mapped);
                        try
                        {
                            _lexConnection.RunCommand(query);
                        }
                        catch (NpgsqlException ex)
                        {
                            if (ex.Code != "23505") throw ex;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                file.Close();
            }
        }

        public void ImportRoutesMappingTable()
        {

            //load from tab file for now
            string filePath = releasePath + @"\" + _dictionaryRow.Values["inps_version"].ToString().Substring(0, 7)
                + @"\YellowCardRouteMappings.tab";
            if (File.Exists(filePath))
            {
                NpgsqlCommand command = new NpgsqlCommand("DELETE FROM drug_routes_mapping_table", _inpsConnection.Connection);
                command.ExecuteNonQuery();
                command.CommandText = "";

                FileStream file = new FileStream(filePath, FileMode.Open);
                //need to replace nul charcter (0x0)
                MemoryStream ms = new MemoryStream();
                StreamWriter writer = new StreamWriter(ms);
                using (StreamReader rdr = new StreamReader(file, Encoding.GetEncoding(1252)))
                {
                    int i = 0;
                    while (!rdr.EndOfStream)
                    {
                        string s = rdr.ReadToEnd();
                        writer.Write(s.Replace(Convert.ToChar(0x0).ToString(), "\\N").Replace(Convert.ToChar(13).ToString(), " "));
                        writer.Flush();
                        i++;
                    }
                    file.Close();
                }
                ms.Seek(0, SeekOrigin.Begin);
                //load data from file
                CopyData copy = new CopyData(_inpsConnection.Connection);
                copy.CopyIn("drug_routes_mapping_table", ms);
            }
        }

        private void ImportScotishPricesForDrugPacks()
        {
            ulong dmdCode;
            int DrugPrice;
            
            string filename =  releasePath + @"\" + _dictionaryRow.Values["inps_version"].ToString().Substring(0, 7) + @"\inps\Scottish Tariff Extract.csv";
            //StreamReader file = new StreamReader(releasePath + @"\Reference\inps\Scottish Tariff Extract.csv");
            StreamReader file = new StreamReader(filename);
            try
            {
                Scoittish_Drug_Prices.Clear();
                Scoittish_Drug_Max_Prices.Clear();

                string line;
                while ((line = file.ReadLine()) != null)
                {
                    string[] p = line.Split(new Char[] { ',' });
                    if (ulong.TryParse(p[0], out  dmdCode) && int.TryParse(p[1], out DrugPrice))
                    {
                        if (!Scoittish_Drug_Prices.ContainsKey(dmdCode))
                            Scoittish_Drug_Prices.Add(dmdCode, DrugPrice);
                        else if (Scoittish_Drug_Prices[dmdCode] > DrugPrice)
                            Scoittish_Drug_Prices[dmdCode] = DrugPrice;
                   

                        if (!Scoittish_Drug_Max_Prices.ContainsKey(dmdCode))
                            Scoittish_Drug_Max_Prices.Add(dmdCode, DrugPrice);
                        else if (Scoittish_Drug_Max_Prices[dmdCode] < DrugPrice)
                            Scoittish_Drug_Max_Prices[dmdCode] = DrugPrice;
                       
                            
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
                //throw new Exception("Cannot find " + releasePath + @"\Reference\inps\Scottish Tariff Extract.csv", e);
            }
            finally
            {
                file.Close();
             }
        }

        private int GetScotishPricesForDrugPacks(ulong dmdCode, int DefaultValue)
        {
            return Scoittish_Drug_Prices.ContainsKey(dmdCode) ? Scoittish_Drug_Prices[dmdCode] : DefaultValue;
        }

        private int GetScotishMaxPricesForDrugPacks(ulong dmdCode, int DefaultValue)
        {
            return Scoittish_Drug_Max_Prices.ContainsKey(dmdCode) ? Scoittish_Drug_Max_Prices[dmdCode] : DefaultValue;
        }

        private void SetPOMAvailableFlag()
        {
            Trace.WriteLine(DateTime.Now + " " + " Set POM flag");

            NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection);
            command.CommandText = "UPDATE inps_drug.drug_gemscript SET has_pom = false";
            command.ExecuteNonQuery();

            string query = @"SELECT drug_id FROM inps_drug.pack where legal IN (SELECT id  FROM inps_drug.concepts where description LIKE '%POM')";
            try
            {
                Trace.WriteLine(DateTime.Now + "Running query:   " + query);
                DataTable data = _inpsConnection.GetTable(query);
                Trace.WriteLine(DateTime.Now + "Data row count is  " + data.Rows.Count.ToString());
                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        query = string.Format("UPDATE inps_drug.drug_gemscript SET has_pom = true WHERE drug_code = {0}", row["drug_id"]);
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine(DateTime.Now + "Error:   " + e.Message);
            }
        }

    }


}
