﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;

namespace INPS.V3.Data
{
    class DbfAccessor
    {
        private string _locationPath;
        private OleDbConnection _connFP;

        public DbfAccessor(string locationPath)
        {
            _locationPath = locationPath;
            _connFP = new OleDbConnection("Provider=VFPOLEDB.1; Data Source=" + _locationPath);
        }

        public DataTable getData(string query)
        {
            DataTable data = new DataTable();
            try
            {
                _connFP.Open();
                OleDbCommand cmdCreate = new OleDbCommand(query, _connFP);
                OleDbDataReader reader = cmdCreate.ExecuteReader();
                data.Load(reader);
                reader.Close();
            }
            catch(Exception e)
            {
                throw new Exception("Dbf query failed " + query + "\n" + e.Message);
            }
            finally
            {
                _connFP.Close();
            }
            return data;
        }
    }
}
