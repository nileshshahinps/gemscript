﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Xml;

namespace INPS.V3.Data
{
    public class LexProductDetails
    {
        private static LexDrugConnection _lexConnection = new LexDrugConnection();
        private DataRowReader _productRow;
        private static ConceptDictionary _concepts = new ConceptDictionary();
        private List<RdcType> _rdcCodes;
        private Dictionary<string, string> _bnfToRdc;
        private Concept _manufacturer;
        private Concept _strengthText;
        private Concept _formText;
        //load mapping file
        

        public LexProductDetails(DataRow source, Dictionary<string, string> bnfRdcMap)
        {
            _productRow = new DataRowReader(source);
            _bnfToRdc = bnfRdcMap;
        }

        protected LexDrugConnection LexConnection
        {
            get
            {
                if (!_lexConnection.Valid) _lexConnection.Connect();
                return _lexConnection;
            }
        }

        static public ConceptDictionary Concepts
        {
            get { return _concepts; }
        }

        public DataRowReader ProductRow
        {
            get { return _productRow; }
        }

        public uint LexCode
        {
            get
            {
                string code = ProductRow.GetString("drug_code");
                if (code != "ZZZZZZZZ")
                {
                    return Convert.ToUInt32(code);
                }
                return 2;
            }
        }

        public ulong DmdCode
        {
            get
            {
                //check if code exists in  old_drug
                InpsDrugConnection connection = new InpsDrugConnection();
                if (connection.Connect())
                {
                    string query = String.Format(@"select * from old_drug where drug_code = '{0}' and length(cast(dmd_code as varchar)) > 0", LexCode);
                    DataRow existingDrugCodeRow = connection.GetFirstRow(query);
                    connection.Close();
                    if (existingDrugCodeRow != null)
                    {
                        UInt64 dmd_code = Convert.ToUInt64(existingDrugCodeRow["dmd_code"]);
                        if (new ConceptCode(dmd_code).IsInps) return dmd_code;
                    }
                    //else create new code
                    ConceptCode drug = new DrugConcept(LexCode, Name).Code;
                    return drug.Code;
                }
                return 0;
            }
        }

        public Concept Category
        {
            get
            {
                if (ProductRow.GetYNBoolean("appliance", false) == true)
                    return Concepts.LoadInpsConcept(ConceptDictionary.CommonConcepts.Appliance);
                if (this.RdcCodes.Count > 0)
                    return Concepts.LoadInpsConcept(Rdc.GetRdcCategory(this.RdcCodes[0].Code));
                return null;
                //OK to base category just on first RDC? or should it be base on main rdc?
            }
        }

        public List<RdcType> RdcCodes
        {
            get
            {
                ReadRdcCodes();
                return _rdcCodes;
            }
        }

        private bool ReadRdcCodes()
        {
            if (_rdcCodes == null)
            {
                _rdcCodes = new List<RdcType>();
                string command = String.Format(@"select * from historical_drug.historical_drug_class_mapping where drug_code = {0}", LexCode.ToString());
                DataTable results = LexConnection.GetTable(command);
                if (results != null)
                {
                    foreach (DataRow row in results.Rows)
                    {
                        if (Convert.ToInt32(row["drug_class_code"]) != 0)
                        {
                            _rdcCodes.Add(new RdcType(Convert.ToUInt32(row["drug_class_code"]), Convert.ToBoolean(row["main"])));
                            string main = row["main"].ToString();
                        }
                    }
                }
            }
            return _rdcCodes.Count > 0;
        }

        public string Name
        {
            get { return ProductRow.GetString("name"); }
        }

        public UInt32 Generic
        {
            get 
            { 
                InpsDrugConnection connection = new InpsDrugConnection();
                if (connection.Connect())
                {
                    string query = String.Format(@"select vmp_code from old_drug where drug_code = '{0}' and length(cast(dmd_code as varchar)) > 0", LexCode);
                    DataRow existingDrugCodeRow = connection.GetFirstRow(query);
                    connection.Close();
                    if (existingDrugCodeRow != null)
                    {
                        UInt32 vmp_code = Convert.ToUInt32(existingDrugCodeRow["vmp_code"]);
                        return vmp_code;
                    }
                    return 0;
                }
                return 0;
            }
        }

        public string OrderNumber
        {
            get { return ProductRow.GetString("order_no"); }
        }

        public Concept Manufacturer
        {
            get
            {
                if (_manufacturer == null)
                {
                    string manufacturer = ProductRow.GetString("manufacturer");
                    if (manufacturer != "")
                        _manufacturer =  DmdProductDetails.Concepts.CreateInpsConcept(ConceptDictionary.CommonConcepts.Manufacturer, manufacturer);
                }
                return _manufacturer;
            }
        }

        public Concept StrengthText
        {
            get
            {
                if (_strengthText == null && Category.Description != "Appliance")
                {
                    string strengthText = ProductRow.GetString("strength");
                    if (strengthText != "")
                        _strengthText =  DmdProductDetails.Concepts.CreateInpsConcept(ConceptDictionary.CommonConcepts.Strength, strengthText);
                }
                return _strengthText;
            }
        }

        public float Strength
        {
            get
            {
                if (StrengthText != null)
                {
                    float strength;
                    string startText = StrengthText.Description;
                    StringBuilder finalText = new StringBuilder();
                    if (startText.Length > 0)
                        foreach (char c in startText)
                            if (Char.IsNumber(c))
                                finalText.Append(c);
                            else if (c == '.') finalText.Append(c);
                            else break;

                    if (finalText.Length > 0 && float.TryParse(finalText.ToString(), out strength)) return strength;
                }
                return 0;                   
            }
        }

        public Concept FormText
        {
            get
            {
                if (_formText == null)
                {
                    string formText = ProductRow.GetString("formulation");
                    if (formText != "")
                        _formText = DmdProductDetails.Concepts.CreateInpsConcept(ConceptDictionary.CommonConcepts.Formulation, formText);
                }
                return _formText;
            }
        }

        public string KeyForm
        {
            get
            {
                string keyForm = null;
                Concept form = FormText;
                if (form != null)
                {
                    string formText = form.Description.ToLower();
                    if (keyForm == null)
                    {
                        formText = formText.Split().Last();
                        keyForm = formText.PadRight(3).Substring(0, 3);
                    }
                }
                return keyForm;
            }
        }

        public bool IsGeneric
        {
            get { return (LexCode == Generic) && Manufacturer == null; }
        }

        public string SearchKey
        {
            get 
            {
                if (Name.Contains(" "))
                    return Name.Substring(0, Name.IndexOf(" ")).ToUpper();
                else return Name.ToUpper();
            }
        }

    }
}
