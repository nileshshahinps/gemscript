﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Linq.Expressions;

namespace INPS.V3.Data
{
    /// <summary>
    /// Wrapper for AMP data
    /// </summary>
    public class AmpDetails : DmdProductDetails
    {
        private VmpDetails _parentVmp;
        //private Dictionary<UInt64, AmppDetails> _amppMap;
        private List<AmppDetails> _ampps;
        private Concept _manufacturer;
        private Concept _flavour;
        private Concept _form;
        private Concept _availabilityRestrictionConcept;
        private List<Concept> _excipients;
        private List<Concept> _routes;
        private DataRowReader _ampInfo;
        private List<DataRowReader> _excipientRows;

        private bool? _isInvalid = null;
        private bool? _isDiscontinued = null;
        private bool? _isAvailable = null;
        private bool? _isFp10 = null;

        private AmpFlags _flags;

        public AmpFlags Flags
        {
            get 
            {
                if (_flags == null) _flags = new AmpFlags(this);
                return _flags;
            }
        }

        public AmpDetails(DataRow source, VmpDetails parentVmp)
            : base(source)
        {
            _parentVmp = parentVmp;
        }

        public override VmpDetails Vmp
        {
            get { return _parentVmp; }
        }

        public List<AmppDetails> Ampps
        {
            get
            {
                if (_ampps == null)
                {
                    ReadAmpps();
                }
                return _ampps;
            }
        }

        public override UInt64 PrevDmdCode
        {
            get { return 0; }
        }

        public override string Name
        {
            get { return ProductRow.GetString("description"); }
        }

        public bool IsMarkedInvalid
        {
            get { return ProductRow.GetBoolean("invalid") || IsComponentOnlyProduct; }
        }

        public override bool IsInvalid
        {
            get 
            {  
                if (_isInvalid == null)
                    _isInvalid = Ampps.All(ampp => ampp.IsInvalid /*|| ampp.IsDiscontinued != IsDiscontinued*/);
                return (bool)_isInvalid; 
            }
        }

        public bool IsCombinationProduct
        {
            get { return ProductRow.GetInt("combination_product") == 1; }
        }

        public bool IsComponentOnlyProduct
        {
            get { return ProductRow.GetInt("combination_product") == 2; }
        }

        public override bool IsActive
        {
            get 
            { return IsAvailable || Vmp.IsAvailable; }
        }

        public ushort ProductType
        {
            get { return 3; }
        }

        public override Concept Manufacturer
        {
            get
            {
                GenerateConcept(ref _manufacturer, "supplier", ConceptDictionary.CommonConcepts.Manufacturer);
                return _manufacturer;
            }
        }

        public override Concept Flavour
        {
            get
            {
                GenerateConcept(ref _flavour, "flavour", ConceptDictionary.CommonConcepts.Flavour);
                return _flavour;
            }
        }

        public override Concept Form
        {
            get
            {
                if (_form == null)
                {
                    if (Vmp.VmpForm == null || Vmp.VmpForm.Description == "Not applicable")
                    {
                        if (Ampps.Count > 0)
                            _form = Ampps.First().Vmpp.UOM;
                    }
                    else _form = Vmp.VmpForm;
                }
                return _form;
            }
        }

        public string OrderNumber
        {
            get { return AmpInfo.GetString("product_order_no"); }
        }

        public override string SearchKey
        {
            get 
            {
                string parsedSearchKey;
                if (IsBrandedGeneric || (Vmp.Name.StartsWith("Generic") && ("Generic " + Name).StartsWith(Vmp.Name)))
                    parsedSearchKey = Vmp.SearchKey;
                else
                {
                    parsedSearchKey = Name.ToUpper();

                    // Trim manufacturer
                    if (Manufacturer != null)
                    {
                        string manufacturer = String.Format(" ({0})", Manufacturer.Description).ToUpper();
                        if (parsedSearchKey.EndsWith(manufacturer))
                        {
                            parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.Length - manufacturer.Length);
                        }
                        //or if starts with manufacturer
                        if (parsedSearchKey.StartsWith(Manufacturer.Description.ToUpper()) /*&& !Vmp.Name.ToUpper().Contains(Manufacturer.Description.ToUpper())*/)
                        {
                            parsedSearchKey = parsedSearchKey.Remove(0, Manufacturer.Description.Length).Trim();
                        }
                    }

                    // Trim matching trailing text in VMP
                    if (Vmp != null && !IsBrandedGeneric)
                    {
                        string vmpName = Vmp.Name.ToUpper();
                        //remove definied strings which appear in vmp name but not amp name
                        string[] ignoreStrings = { " MODIFIED-RELEASE", " SUGAR FREE", " GLUTEN FREE", " GASTRO-RESISTANT", " PRESERVATIVE FREE", " CFC FREE", " CONCENTRATE FOR" };
                        for (int i = 0; i < ignoreStrings.Length; i++)
                        {
                            if (vmpName.Contains(ignoreStrings[i]))
                                vmpName = vmpName.Remove(vmpName.IndexOf(ignoreStrings[i]), ignoreStrings[i].Length);
                            if (parsedSearchKey.Contains(ignoreStrings[i]))
                                parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.IndexOf(ignoreStrings[i]), ignoreStrings[i].Length);
                        }

                        int numCharsTheSame = 0;
                        int numCharsToTrim = 0;
                        for (; numCharsTheSame < vmpName.Length && numCharsTheSame < parsedSearchKey.Length; ++numCharsTheSame)
                        {
                            char c1 = vmpName[vmpName.Length - (numCharsTheSame + 1)];
                            char c2 = parsedSearchKey[parsedSearchKey.Length - (numCharsTheSame + 1)];
                            if (c1 != c2)
                                break;
                            if (c1 == ' ')
                                numCharsToTrim = numCharsTheSame;
                        }


                        if (numCharsToTrim > 0)
                        {
                            // Trim to nearest space
                            parsedSearchKey = parsedSearchKey.Substring(0, parsedSearchKey.Length - numCharsToTrim);
                        }

                        parsedSearchKey = parsedSearchKey.Trim();
                        //check for strength and remove
                        if (Strength != null)
                        {
                            string strength = String.Format(" {0}", Strength.Description).ToUpper();
                            if (parsedSearchKey.Contains(strength))
                            {
                                parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.IndexOf(strength), strength.Length);
                            }
                            else
                            {
                                if (strength.Contains(" + "))
                                {
                                    strength = strength.Replace(" + ", "/");
                                    if (parsedSearchKey.Contains(strength))
                                    {
                                        parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.IndexOf(strength), strength.Length);
                                    }
                                    else
                                    {
                                        strength = strength.Replace(" + ", " / ");
                                        if (parsedSearchKey.Contains(strength))
                                        {
                                            parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.IndexOf(strength), strength.Length);
                                        }
                                    }
                                }
                                if (parsedSearchKey.Contains(" " + StrengthValue.ToString("G29") + " "))
                                    parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.IndexOf(StrengthValue.ToString()), StrengthValue.ToString().Length);
                                else if (parsedSearchKey.EndsWith(" " + StrengthValue.ToString("G29")))
                                    parsedSearchKey = parsedSearchKey.Remove(parsedSearchKey.Length - (" " + StrengthValue.ToString("G29")).Length);
                            }
                        }
                    }
                }

                return parsedSearchKey.Trim();
            }
        }

        public override bool IsBrandedGeneric
        {
            get { return Name.StartsWith(Vmp.Name); } 
        }

        public override bool IsDiscontinued
        {
            get 
            { 
                if (_isDiscontinued == null)
                    _isDiscontinued = Ampps.All(ampp=>ampp.IsDiscontinued || ampp.IsInvalid!= IsInvalid);
                return (bool)_isDiscontinued;
            }
        }

        public override bool IsFp10
        {
            get
            {
                if (_isFp10 != null) return (Boolean)_isFp10;
                return IsFp10England;
            }
            set { _isFp10 = value; }
        }

        public override bool IsFp10England
        {
            get { return IsIssuable && !IsBlacklisted && (IsMedicine || IsReimbursableDeviceEngland); }
        }

        public override bool IsFp10Wales
        {
            get { return IsIssuable && (IsMedicine || IsReimbursableDeviceWales); }
        }

        public override bool IsFp10Scotland
        {
            get { return IsIssuable && (IsMedicine || IsReimbursableDeviceScotland); }
        }

        public override bool IsFp10NI
        {
            get { return IsIssuable && (IsMedicine || IsReimbursableDeviceNI); }
        }

        public Int32 LicensingAuthority
        {
            get 
            { 
                return ProductRow.GetInt("licensing_authority");
            }
        }

        public override bool IsFp10MDA
        {
            get { return Ampps.Any(a => a.IsFp10MDA && a.IsInvalid == IsInvalid && a.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsDohApproved
        {
            get { return Ampps.Any(ampp => ampp.IsDohApproved && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        /*public override bool IsAcbs
        {
            get { return IsAcbsEngland; }
        }*/

        public override bool IsSls
        {
            get { return Ampps.Any(ampp => ampp.IsSls && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsBlacklisted
        {
            get { return Ampps.All(ampp => ampp.IsBlacklisted || ampp.IsInvalid != IsInvalid || ampp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsBlackTriangle
        {
            get { return ProductRow.GetBoolean("csm_monitoring_ind") || RData.Flags.Value.BlackTriangle == 1; }
        }

        public override bool IsPa
        {
            get { return Ampps.Any(ampp => ampp.IsPa && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsNurseFormulary
        {
            get { return Ampps.Any(ampp => ampp.IsNurseFormulary && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        public bool IsInpsSpecial
        {
            get { return AvailabilityRestriction == 6 || Manufacturer.Description == "Special Order" || Manufacturer.Description == "Drug Tariff Special Order"; }
        }

        public override bool IsSpecial
        {
            get { return IsInpsSpecial || IsImported; }
        }

        public bool CanHaveAfEndorsement
        {
            //21014611000001102 is code for 'Flavour Not Specified'
            get { return Manufacturer.Id == 21014611000001102; }
        }

        public override bool IsParallelImport
        {
            get { return ProductRow.GetBoolean("parallel_import_ind"); }
        }

        public override bool IsExtemp
        {
            //TODO update with authored flag
            get { return AvailabilityRestriction == 7 || Manufacturer.Description == "Extemp Order"; }
        }

        public override bool IsImported
        {
            //TODO update with authored flag?
            get { return AvailabilityRestriction == 4 || Manufacturer.Description.StartsWith("(Imported "); }
        }

        public override bool IsSecondaryCare
        {
            get { return Ampps.All(ampp => ampp.IsSecondaryCare || ampp.IsInvalid != IsInvalid || ampp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsReimbursableDevice
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDevice && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsReimbursableDeviceEngland
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceEngland && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceWales
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceWales && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceNI
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceNI && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceScotland
        {
            get { return Ampps.Any(ampp => ampp.IsReimbursableDeviceScotland && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsMedicine
        {
            get { return Ampps.Any(ampp => ampp.IsMedicine && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsUnlicensed
        {
            get { return Ampps.Any(ampp => ampp.IsUnlicensed && ampp.IsInvalid == IsInvalid && ampp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsAvailable
        {
            get 
            { 
                if (_isAvailable == null)
                    _isAvailable =  !IsInvalid && (AvailabilityRestriction != 9 || IsDiscontinued) && !IsImported;
                return (bool)_isAvailable;
            }
        }

        public Int32 AvailabilityRestriction
        {
            get { return ProductRow.GetInt("availability_restriction"); }
        }

        public Concept AvailabilityRestrictionConcept
        {
            get
            {
                GenerateConcept(ref _availabilityRestrictionConcept, "availability_restriction", ConceptDictionary.CommonConcepts.AvailabilityRestrictions);
                return _availabilityRestrictionConcept;
            }
        }

        public override bool UseGeneric
        {
            get { return Vmp.IsAvailable; }
        }

        public override bool UseBrand
        {
            get { return IsAvailable; }
        }

        public override bool IsInteractionControlled
        {
            /*true if dm+D drug or gemscript authored product which is NOT
             * medication with active ingredient excl homeopathic)
             * appliance with active ingredient*/
            get { return IsDmd || Rdc.IsHomeopathic(RData.RdcCodes) || Vmp.Category == null || (Vmp.Category.Description != "Medication" && ActiveIngredients.Count == 0); }
        }

        public List<Concept> Routes
        {
            get
            {
                ReadRoutes();
                return _routes;
            }
        }

        public List<Concept> Excipients
        {
            //this only contains dm+d excipients, not authored excipients
            get
            {
                _excipients = new List<Concept>();
                foreach (DataRowReader row in ExcipientRows)
                    _excipients.Add(Concepts.ImportExternalConcept(ConceptDictionary.CommonConcepts.Excipient, new Concept(row.GetConceptId("id"), row.GetString("name"))));
                return _excipients;
            }
        }

        public List<TypeIngredient> ActiveIngredients
        {
            //this only contains dm+d ingredients, not authored ingredients
            get
            {
                return Vmp.ActiveIngredients;
            }
        }

        private DataRowReader AmpInfo
        {
            get
            {
                ReadRelatedRow(ref _ampInfo, String.Format(@"select * from bcb_inps.amp_info where amp_id = {0}", DmdCode));
                return _ampInfo;
            }
        }

        private bool ReadAmpps()
        {
            //_amppMap = new Dictionary<ulong, AmppDetails>();
            _ampps = new List<AmppDetails>();

            //BcbConnection.Connect();
            string command = String.Format(@"select * from bcb_inps.ampp where amp_id = {0}", DmdCode.ToString());
            DataTable results = BcbConnection.GetTable(command);
            foreach (DataRow row in results.Rows)
            {
                AmppDetails amp = new AmppDetails(row, this);
                //_amppMap.Add(amp.DmdCode, amp);
                _ampps.Add(amp);
            }

            //add any children of reconciled AMP if AMPP not reconciled itself
            if (IsReconciledDmdProduct)
            {
                command = String.Format(@"select * from bcb_inps.amp where vmp_id = {0}", ReconciledGemscriptId.ToString());
                results = BcbConnection.GetTable(command);
                foreach (DataRow row in results.Rows)
                {
                    AmppDetails ampp = new AmppDetails(row, this);
                    if (!ampp.IsReconciledGemscriptProduct)
                        _ampps.Add(ampp);
                }
            }

            return _ampps.Count > 0;
        }

        private bool ReadRoutes()
        {
            if (_routes == null)
            {
                _routes = new List<Concept>();
                //BcbConnection.Connect();
                string command = String.Format(@"select * from vmp_route where vmp_id = {0}", Vmp.DmdCode.ToString());
                DataTable results = BcbConnection.GetTable(command);
                foreach (DataRow row in results.Rows)
                {
                    Concept newConcept = GenerateConcept(Convert.ToUInt64(row["route_code"]), ConceptDictionary.CommonConcepts.Route);
                    if (newConcept != null)
                        _routes.Add(newConcept);
                }
                //add routes from bcb

                //BcbConnection.Close();
                foreach (Resip.ResipAPI.TypeRoute route in RData.Routes)
                {
                    Concept newConcept = GenerateConcept(Convert.ToUInt64(route.Id), ConceptDictionary.CommonConcepts.Route);
                    if (newConcept != null)
                        _routes.Add(newConcept);
                }
            }
            if (_routes == null || _routes.Count < 1)
                _routes.Add(GenerateConcept(3594011000001102, ConceptDictionary.CommonConcepts.Route));
            return _routes.Count > 0;
        }

        private bool ReadExcipients()
        {
            if (_excipients == null)
            {
                _excipients = new List<Concept>();
                //_ingredientRows = new List<DataRowReader>();
                foreach (DataRowReader row in ExcipientRows)
                {
                    Concept newConcept = Concepts.ImportExternalConcept(ConceptDictionary.CommonConcepts.Excipient, new Concept(row.GetConceptId("id"), row.GetString("name")));
                    if (newConcept != null)
                        _excipients.Add(newConcept);
                }
            }
            return _excipients.Count > 0;
        }

        private List<DataRowReader> ExcipientRows
        {
            get
            {
                try
                {
                    string query = String.Format(@"select * from amp_ingredients join ingredients i on ingredient_id = i.id where amp_id = {0}", DmdCode);
                    DataTable results = BcbConnection.GetTable(query);
                    _excipientRows = new List<DataRowReader>();
                    foreach (DataRow row in results.Rows)
                    {
                        _excipientRows.Add(new DataRowReader(row));
                    }
                    //add ingredients for component products if is combination product
                    if (IsCombinationProduct)
                    {
                        //get list of ingredients
                        query = "SELECT DISTINCT i.*, ai.* FROM ampp ap1 JOIN ampp_combination_pack acp ON acp.parent_pack_id = ap1.\"ID\" " +
                                "JOIN ampp ap2 ON ap2.\"ID\" = acp.child_pack_id JOIN amp a2 ON a2.\"ID\" = ap2.amp_id JOIN amp_ingredients ai ON a2.\"ID\" = ai.amp_id JOIN ingredients i ON i.id = ai.ingredient_id " +
                                "WHERE ap1.amp_id = " + DmdCode;
                        results = BcbConnection.GetTable(query);
                        foreach (DataRow row in results.Rows)
                        {
                            _excipientRows.Add(new DataRowReader(row));
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("Error creating Excipient Row: ", e.Message);
                    throw e;
                }
                return _excipientRows;
            }
        }
    }

    public class AmpFlags : Flags
    {

        private AmpDetails _amp;
        private bool? _isFp10 = null;

        public AmpFlags(AmpDetails a)
        {
            _amp = a;
        }

        public override bool IsAvailable
        {
            get
            { return (bool)!_amp.IsInvalid && (_amp.AvailabilityRestriction != 9 || _amp.IsDiscontinued) && !_amp.IsComponentOnlyProduct; }
        }

        public override bool IsIssuable
        {
            get { return IsAvailable && !(_amp.IsExtemp || _amp.IsParallelImport); }
        }

        public override bool IsSecondaryCare
        {
            get { return _amp.AvailabilityRestriction == 8; }
        }

        public override bool IsImported
        {
            get { return _amp.IsImported; }
        }

        public override bool UseGeneric
        {
            get { return _amp.Vmp.Flags.IsAvailable; }
        }

        public override bool UseBrand
        {
            get { return IsAvailable; }
        }

        public override bool IsActive
        {
            get
            { return IsAvailable || _amp.Vmp.Flags.IsAvailable; }
        }

        public override bool IsFp10England
        {
            get
            {
                return IsIssuable && !_amp.IsBlacklisted && (_amp.IsMedicine || _amp.IsReimbursableDeviceEngland) && _amp.RData.Flags.Value.CountrySpecificitiesEngland == 1;
            }
        }

        public override bool IsFp10Wales
        {
            get
            {
                return IsIssuable && (_amp.IsMedicine || _amp.IsReimbursableDeviceWales) && _amp.RData.Flags.Value.CountrySpecificitiesWales == 1;
            }
        }

        public override bool IsFp10Scotland
        {
            get { return IsIssuable && (_amp.IsMedicine || _amp.IsReimbursableDeviceScotland) && _amp.RData.Flags.Value.CountrySpecificitiesScotland == 1; }
        }

        public override bool IsFp10NI
        {
            get { return IsIssuable && (_amp.IsMedicine || _amp.IsReimbursableDeviceNI) && _amp.RData.Flags.Value.CountrySpecificitiesNireland == 1; }
        }

        public override bool IsFp10
        {
            get
            {
                if (_isFp10 != null) return (Boolean)_isFp10;
                return IsFp10England;
            }
            set { _isFp10 = value; }
        }

        public bool IsSlsEngland
        {
            get { return _amp.RData.Flags.Value.SlsEngland == 1; }
        }

        public bool IsSlsScotland
        {
            get { return _amp.RData.Flags.Value.SlsScotland == 1; }
        }

        public bool IsSlsWales
        {
            get { return _amp.RData.Flags.Value.SlsWales == 1; }
        }

        public bool IsSlsNi
        {
            get { return _amp.RData.Flags.Value.SlsNireland == 1; }
        }
    }
}
