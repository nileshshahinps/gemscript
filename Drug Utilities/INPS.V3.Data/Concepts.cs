﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using Npgsql;
using NpgsqlTypes;

namespace INPS.V3.Data
{
    public class ConceptCode
    {
        private uint _localCode;
        private uint _localNamespace;
        private uint _globalNamespace;
        private uint _partitionId;

        static private uint _inpsNamespace = 1000027;

        public enum LocalNamespaces
        {
            DrugDictionaryMisc = 501,
            Drugs = 500,
            Temporary = 100,
        }

        public enum PartitionId
        {
            Concept = 10,
            Description = 11,
            Relationship = 12,
        }

        public ConceptCode(LocalNamespaces localNamespace, PartitionId partitionId, uint localCode)
        {
            _globalNamespace = _inpsNamespace;
            _localNamespace = (uint)localNamespace;
            _partitionId = (uint)partitionId;
            _localCode = localCode;
        }

        public ConceptCode(ulong fullCode)
        {
            _partitionId = (uint)((fullCode % 1000) / 10);
            //global namespace only if not core concept - idenitifed by partition 00, 01 or 02
            if (_partitionId <= 2)
                _localCode = (uint)(fullCode / 1000);
            else
            {
                _globalNamespace = (uint)((fullCode % 10000000000) / 1000);
                ulong codepart = fullCode / 10000000000;
                _localNamespace = (uint)(codepart % 1000);
                _localCode = (uint)(codepart / 1000);
            }
        }

        public UInt64 Code
        {
            get
            {
                if (IsGlobal)
                    return Convert.ToUInt64(Checksum.VerhoeffCompute((((ulong)_localCode * 100)
                                                                            + (ulong)_partitionId).ToString()));
                else
                    return Convert.ToUInt64(Checksum.VerhoeffCompute((((ulong)_localCode * 1000000000000)
                                                                            + ((ulong)_localNamespace * 1000000000)
                                                                            + ((ulong)_globalNamespace * 100)
                                                                            + (ulong)_partitionId).ToString()));
            }
        }

        private class Checksum
        {
            private static int[][] dihedral = new int[10][];
            private static int[][] fnF = new int[8][];
            private static string[] inverseD5;

            public static string VerhoeffCompute(string idValue)
            {
                try
                {
                    VerhoeffArrayInit();
                    long tCheck = 0;
                    for (int i = idValue.Length - 1; i >= 0; i--)
                    {
                        int fnf1 = (idValue.Length - i) % 8;
                        int fnf2 = Convert.ToInt16(idValue.Substring(i, 1));
                        tCheck = dihedral[tCheck][fnF[fnf1][fnf2]];
                    }
                    return idValue + inverseD5[tCheck];
                }
                catch (Exception Exception)
                {
                    throw Exception;
                }
            }

            private static void VerhoeffArrayInit()
            {
                if (inverseD5 != null) return;
                dihedral[0] = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                dihedral[1] = new int[10] { 1, 2, 3, 4, 0, 6, 7, 8, 9, 5 };
                dihedral[2] = new int[10] { 2, 3, 4, 0, 1, 7, 8, 9, 5, 6 };
                dihedral[3] = new int[10] { 3, 4, 0, 1, 2, 8, 9, 5, 6, 7 };
                dihedral[4] = new int[10] { 4, 0, 1, 2, 3, 9, 5, 6, 7, 8 };
                dihedral[5] = new int[10] { 5, 9, 8, 7, 6, 0, 4, 3, 2, 1 };
                dihedral[6] = new int[10] { 6, 5, 9, 8, 7, 1, 0, 4, 3, 2 };
                dihedral[7] = new int[10] { 7, 6, 5, 9, 8, 2, 1, 0, 4, 3 };
                dihedral[8] = new int[10] { 8, 7, 6, 5, 9, 3, 2, 1, 0, 4 };
                dihedral[9] = new int[10] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

                fnF[0] = new int[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                fnF[1] = new int[10] { 1, 5, 7, 6, 2, 8, 3, 0, 9, 4 };

                for (int i = 2; i <= 7; i++)
                {
                    fnF[i] = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    for (int j = 0; j <= 9; j++)
                        fnF[i][j] = fnF[i - 1][fnF[1][j]];
                }

                inverseD5 = new string[10] { "0", "4", "3", "2", "1", "5", "6", "7", "8", "9" };
            }
        }

        public bool IsGlobal
        {
            get { return _globalNamespace == 0; }
        }

        public bool IsInps
        {
            get { return _globalNamespace == _inpsNamespace; }
        }

        public ConceptCode PreIncrement()
        {
            _localCode++;
            return this;
        }

        static public ConceptCode ToCode(ConceptDictionary.CommonConcepts conceptEnum)
        {
            if ((int)conceptEnum > 2 && (int)conceptEnum < 20)
                return new ConceptCode(LocalNamespaces.DrugDictionaryMisc, PartitionId.Relationship, (uint)conceptEnum);
            return new ConceptCode(LocalNamespaces.DrugDictionaryMisc, PartitionId.Concept, (uint)conceptEnum);
        }
        static public ulong ToNum(ConceptDictionary.CommonConcepts conceptEnum)
        {
            ConceptCode code = ConceptCode.ToCode(conceptEnum);
            return code.Code;
        }
    }

    public class DrugConcept
    {
        static ConceptCode _nextDrugId = null;
        InpsDrugConnection _inpsConnection = new InpsDrugConnection();
        ConceptCode _code;
        String _drugName;
        uint _visionCode;

        void InitialiseDrugIdCounter()
        {
            uint lastProductCode = 0;
            string query = @"select max(dmd_code) as max_id from old_drug where cast(dmd_code as varchar) like '%5001000027%' and dmd_code < 500005001000027100";
            _inpsConnection.Connect();
            DataRow result = _inpsConnection.GetFirstRow(query);
            if (result != null && !result.IsNull("max_id"))
                lastProductCode = Convert.ToUInt32(Convert.ToUInt64(result["max_id"])/10000000000000);
            _nextDrugId = new ConceptCode(ConceptCode.LocalNamespaces.Drugs, ConceptCode.PartitionId.Concept, lastProductCode);
            _inpsConnection.Close();
        }

        public DrugConcept(uint visionCode, String drugname)
        {
            if (_nextDrugId == null)
                InitialiseDrugIdCounter();
            _nextDrugId.PreIncrement();
            _visionCode = visionCode;
            _drugName = drugname;
            _code = _nextDrugId;
        }

        public ConceptCode Code
        {
            get
            {
                return _code;
            }
        }   
    }

    public class Concept
    {
        private static Concept _nullConcept = new Concept("");
        private UInt64 _id;
        private string _description;

        static Concept NullConcept
        {
            get { return _nullConcept; }
        }

        public UInt64 Id
        {
            get { return _id; }
        }
        public string Description
        {
            get { return _description; }
        }
        public bool IsNull
        {
            get { return _id == 0; }
        }

        public Concept(UInt64 id, string description)
        {
            _id = id;
            _description = description;
        }

        public Concept(ConceptDictionary.CommonConcepts conceptEnum, string description)
            : this(ConceptCode.ToNum(conceptEnum), description)
        {

        }

        public Concept(string description)
        {
            _id = 0;
            _description = description;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ConceptDictionary
    {
        public enum CommonConcepts
        {
            //once assigned don't need to create every time
            Concept = 1,
            Relationship,
            IsA,
            HasActiveIngredient,
            HasExcipient,
            HasContraindication,
            HasPrecaution,
            HasSideEffect,
            HasLabelWarning,
            HasUse,
            HasRoute,

            DrugAttribute = 20,
            Vtm,
            Manufacturer,
            Unit,
            Formulation,
            CDC,
            DmdType,
            LexUnit,
            Flavour,
            HighRiskWarning,
            LegalStatus,
            Strength,
            Category,
            PrescribingSatus,
            AvailabilityRestrictions,

            ActiveIngredient = 40,
            Excipient,
            Contraindication,
            Precaution,
            SideEffect,
            LabelWarning,
            Use,
            Route,

            Medication = 60,
            Appliance,
            Reagent,
            Oxygen,

            Vmp = 70,
            Amp,
            Vmpp,
            Ampp,
        }

        // Memory Cache
        private Dictionary<UInt64, Concept> _bcbConcepts = new Dictionary<ulong, Concept>();
        private Dictionary<UInt64, Concept> _inpsConceptCodeDictionary = new Dictionary<UInt64, Concept>();
        private Dictionary<string, Concept> _inpsConceptTextDictionary = new Dictionary<string, Concept>();

        private bool _baseConceptsInitialised = false;

        // Data 
        static ConceptCode _nextId = new ConceptCode(ConceptCode.LocalNamespaces.DrugDictionaryMisc, ConceptCode.PartitionId.Concept, 1);
        BcbConnection _bcbConnection = new BcbConnection();
        InpsDrugConnection _inpsConnection = new InpsDrugConnection();
        TableRow _conceptRow;
        TableRow _relationshipRow;

        // Concepts
        public Concept RelationshipConcept
        {
            get { return LoadInpsConcept(CommonConcepts.Relationship); }
        }
        public Concept IsAConcept
        {
            get { return LoadInpsConcept(CommonConcepts.IsA); }
        }

        public ConceptDictionary()
        {
            if (!_bcbConnection.Connect()) throw new Exception("bcb connection failed");
            if (!_inpsConnection.Connect()) throw new Exception("INPS ConnectionState failed");

            TableDefinition conceptTableDef = new TableDefinition("concepts");
            conceptTableDef.AddAttribute(new TableAttribute("id", NpgsqlDbType.Bigint));
            conceptTableDef.AddAttribute(new TableAttribute("description", NpgsqlDbType.Text));
            _conceptRow = new TableRow(conceptTableDef);

            TableDefinition relationshipTableDef = new TableDefinition("relationships");
            relationshipTableDef.AddAttribute(new TableAttribute("id", NpgsqlDbType.Bigint));
            relationshipTableDef.AddAttribute(new TableAttribute("subject", NpgsqlDbType.Bigint));
            relationshipTableDef.AddAttribute(new TableAttribute("relationship", NpgsqlDbType.Bigint));
            relationshipTableDef.AddAttribute(new TableAttribute("object", NpgsqlDbType.Bigint));
            _relationshipRow = new TableRow(relationshipTableDef);
        }

        static public bool IsSnomedInSource(CommonConcepts conceptId)
        {
            switch (conceptId)
            {
                case ConceptDictionary.CommonConcepts.Vtm:
                case ConceptDictionary.CommonConcepts.Manufacturer:
                case ConceptDictionary.CommonConcepts.Unit:
                case ConceptDictionary.CommonConcepts.Formulation:
                case ConceptDictionary.CommonConcepts.Route:
                    return true;
                default: return false;
            }
        }

        public Concept GetCachedBcbConcept(UInt64 id)
        {
            if (ConceptDictionary.IsSnomedInSource((ConceptDictionary.CommonConcepts)id))
            {
                if (_bcbConcepts.ContainsKey(id))
                {
                    return _bcbConcepts[id];
                }
            }
            return null;
        }

        public Concept GetBcbConcept(UInt64 id, CommonConcepts parentConceptCode)
        {
            Concept result = GetCachedBcbConcept(id);
            if (result == null)
            {
                string table = "";
                string idField = "code";
                string textField = "description";
                switch (parentConceptCode)
                {
                    case CommonConcepts.Formulation: table = "lk_formulation"; break;
                    case CommonConcepts.Unit: table = "lk_unit_of_measure"; break;
                    case CommonConcepts.CDC: table = "lk_control_drug_category"; break;
                    case CommonConcepts.Manufacturer: table = "lk_supplier"; break;
                    case CommonConcepts.Flavour: table = "lk_flavour"; break;
                    case CommonConcepts.Route: table = "lk_administration_route"; break;
                    case CommonConcepts.LegalStatus: table = "lk_legal_category"; break;
                    case CommonConcepts.PrescribingSatus: table = "lk_prescribing_status"; break;
                    case CommonConcepts.AvailabilityRestrictions: table = "lk_availability_restriction"; break;
                    case CommonConcepts.Vtm: table = "vtm"; idField = "ID"; textField = "full_name"; break;
                    default: return null;
                }

                string command = String.Format(@"select * from ""{0}"" where ""{1}"" = {2}", table, idField, id.ToString());
                DataRow conceptRow = _bcbConnection.GetFirstRow(command);
                if (conceptRow != null)
                {
                    result = new Concept(id, conceptRow[textField].ToString());
                   /* if (ConceptDictionary.IsSnomedInSource(parentConceptCode))
                        _bcbConcepts[id] = result;*/
                }
            }
            return result;
        }

        public void InitialiseBaseConcepts()
        {
            if (!_baseConceptsInitialised)
            {
                // Add base concepts
                Concept root = new Concept(CommonConcepts.Concept, "Concept");
                Concept relationship = new Concept(CommonConcepts.Relationship, "Relationship");
                Concept drugAttribute = new Concept(CommonConcepts.DrugAttribute, "Drug Attribute");
                Concept isA = new Concept(CommonConcepts.IsA, "is a");
                Concept drugCategory = new Concept(CommonConcepts.Category, "Category");
                Concept dmdType = new Concept(CommonConcepts.DmdType, "Dmd Type");
                AddConcept(root, relationship);
                AddConcept(root, drugAttribute);
                AddConcept(relationship, isA);
                AddConcept(relationship, new Concept(CommonConcepts.HasActiveIngredient, "has active ingredient"));
                AddConcept(relationship, new Concept(CommonConcepts.HasExcipient, "has excipient"));
                AddConcept(relationship, new Concept(CommonConcepts.HasContraindication, "has contraindication"));
                AddConcept(relationship, new Concept(CommonConcepts.HasPrecaution, "has precaution"));
                AddConcept(relationship, new Concept(CommonConcepts.HasSideEffect, "has side effect"));
                AddConcept(relationship, new Concept(CommonConcepts.HasLabelWarning, "has label warning"));
                AddConcept(relationship, new Concept(CommonConcepts.HasUse, "has use"));
                AddConcept(relationship, new Concept(CommonConcepts.HasRoute, "has route"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Vtm, "Vtm"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Manufacturer, "Manufacturer"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Unit, "Unit"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Formulation, "Formulation"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.CDC, "CDC"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.LexUnit, "Lex Unit"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Flavour, "Flavour"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Strength, "Strength"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.HighRiskWarning, "High Risk Warning"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.LegalStatus, "Legal Status"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.PrescribingSatus, "Prescribing Status"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.AvailabilityRestrictions, "Availability Restrictions"));

                AddConcept(drugAttribute, new Concept(CommonConcepts.ActiveIngredient, "Active Ingredient"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Excipient, "Excipient"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Contraindication, "Contraindication"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Precaution, "Precaution"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.SideEffect, "Side Effect"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.LabelWarning, "Label Warning"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Use, "Use"));
                AddConcept(drugAttribute, new Concept(CommonConcepts.Route, "Route"));

                AddConcept(drugAttribute, drugCategory);
                AddConcept(drugCategory, new Concept(CommonConcepts.Medication, "Medication"));
                AddConcept(drugCategory, new Concept(CommonConcepts.Appliance, "Appliance"));
                AddConcept(drugCategory, new Concept(CommonConcepts.Oxygen, "Oxygen"));
                AddConcept(drugCategory, new Concept(CommonConcepts.Reagent, "Reagent"));

                AddConcept(drugAttribute, dmdType);
                AddConcept(dmdType, new Concept(CommonConcepts.Vmp, "VMP"));
                AddConcept(dmdType, new Concept(CommonConcepts.Vmpp, "VMPP"));
                AddConcept(dmdType, new Concept(CommonConcepts.Amp, "AMP"));
                AddConcept(dmdType, new Concept(CommonConcepts.Ampp, "AMPP"));
                _baseConceptsInitialised = true;
                InitialiseIdCounter();
            } 
        }

        public Concept LoadInpsConcept(CommonConcepts code)
        {
            return LoadInpsConcept(ConceptCode.ToNum(code));
        }

        public Concept LoadInpsConcept(ulong code)
        {
            ////Trace.WriteLine("METHOD: LoadInpsConcept");
            ////Trace.Indent();
            Concept result;
            if (!_inpsConceptCodeDictionary.TryGetValue(code, out result))
            {
                string query = String.Format(@"select * from concepts c where c.id = '{0}'", code);
                DataRow conceptRow = _inpsConnection.GetFirstRow(query);
                if (conceptRow != null)
                {
                    // Found in db. Add concept to cache - uses too much memory?
                    result = ReadConcept(conceptRow);
                    //Trace.WriteLine("...concept found - " + result.Description);
                    //_inpsConceptCodeDictionary[result.Id] = result;
                }
            }
           // //Trace.Unindent();
            return result;
        }

        public Concept ImportExternalConcept(ConceptDictionary.CommonConcepts isA, Concept conceptToAdd)
        {
            return ImportExternalConcept(LoadInpsConcept(isA), conceptToAdd);
        }

        public Concept ImportExternalConcept(Concept isA, Concept conceptToAdd)
        {
            Concept result = LoadInpsConcept(conceptToAdd.Id);
            if (result == null)
            {
                if (AddConcept(isA, conceptToAdd))
                    result = conceptToAdd;
            }
            else
            {
                if (result.Description != conceptToAdd.Description)
                {
                    // Update current record
                }
                result = conceptToAdd;
            }
            return result;
        }

        public Concept CreateInpsConcept(ConceptDictionary.CommonConcepts isA, string description)
        {
            //Trace.WriteLine("METHOD: CreateInpsConcept(" + description + ")");
            //don't return code if description is blank
            if (description == "") return null;
            return CreateInpsConcept(LoadInpsConcept(isA), description);
            
        }

        public Concept CreateInpsConcept(Concept isA, string description)
        {
            Concept result = null;
            string key = GetKey(isA, description);
            if (!_inpsConceptTextDictionary.TryGetValue(key, out result))
            {
                // Not cached. Try to read from db
                string query = String.Format(@"select * from concepts c inner join relationships r on r.subject = c.id where c.description = $${0}$$ and r.object = {1}",
                                    description, isA.Id); //todo use parameters to handle special characters such as '
                DataRow conceptRow = _inpsConnection.GetFirstRow(query);
                if (conceptRow != null)
                {
                    // Found in db. Add concept to cache
                    result = ReadConcept(conceptRow);
                    /*_inpsConceptTextDictionary[key] = result;
                    if (!_inpsConceptCodeDictionary.ContainsKey(result.Id))
                    {
                        _inpsConceptCodeDictionary[result.Id] = result;
                    }*/
                }
                else
                {
                    // Create new concept and add to db and cache
                    result = new Concept(_nextId.Code, description);
                    AddConcept(isA, result);
                    _nextId.PreIncrement();
                    
                }
            }
            return result;
        }

        private Concept ReadConcept(DataRow row)
        {
            return new Concept(Convert.ToUInt64(row["id"]), row["description"].ToString());
        }

        public bool AddConcept(Concept isA, Concept concept)
        {
            NpgsqlCommand command = new NpgsqlCommand("SET search_path TO inps_drug", _inpsConnection.Connection);
            command.ExecuteNonQuery();
            command.CommandText = "";
            //check if concept already in dictionary
            string query = String.Format("SELECT * from concepts where id={0} and description=$${1}$$", concept.Id, concept.Description);
            if (_inpsConnection.GetFirstRow(query) == null)
            {
                // Add to concept table
                _conceptRow.Clear();
                _conceptRow.SetValue("id", concept.Id);
                _conceptRow.SetValue("description", concept.Description);
                _conceptRow.AppendToTable(command);

                // Add to relationships table
                _relationshipRow.Clear();
                _relationshipRow.SetValue("subject", concept.Id);
                _relationshipRow.SetValue("relationship", ConceptCode.ToNum(CommonConcepts.IsA));
                _relationshipRow.SetValue("object", isA.Id);
                _relationshipRow.AppendToTable(command);
            }

            // Add to cache
            //_inpsConceptTextDictionary[GetKey(isA, concept.Description)] = concept;
            //_inpsConceptCodeDictionary[concept.Id] = concept;
            return true;
        }

        public bool AddRelationship(ulong subjectCode, ulong relationshipCode, ulong objectCode)
        {
            using (NpgsqlCommand command = new NpgsqlCommand("", _inpsConnection.Connection))
            {
                // Add to relationships table
                //check is not already in table
                string query = String.Format("select * from relationships where object = {0} and subject = {1} and relationship = {2}", objectCode, subjectCode, relationshipCode);
                DataRow res = _inpsConnection.GetFirstRow(query);
                if (res == null)
                {
                    _relationshipRow.Clear();
                    _relationshipRow.SetValue("subject", subjectCode);
                    _relationshipRow.SetValue("relationship", relationshipCode);
                    _relationshipRow.SetValue("object", objectCode);
                    _relationshipRow.AppendToTable(command);
                }
                return true;
            }
        }

        private string GetKey(Concept isA, string description)
        {
            // todo: left pad id with zeros
            return Convert.ToString(isA == null ? 0 : isA.Id) + description;
        }

        void InitialiseIdCounter()
        {
            string query = @"select max(id) as max_id from concepts where cast(id as varchar) like '%1000027%'"; //todo find max Vision generated concept for a given namespace
            DataRow result = _inpsConnection.GetFirstRow(query);
            if (result != null && !result.IsNull("max_id"))
            {
                ulong lastProductCode = Convert.ToUInt64(result["max_id"]);
                _nextId = new ConceptCode(lastProductCode);
                _nextId.PreIncrement();
            }
        }
    }
}
