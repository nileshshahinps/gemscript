﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using Npgsql;
using NpgsqlTypes;
using MySql.Data.MySqlClient;

namespace INPS.V3.Data
{
    public abstract class DrugConnection : IDisposable
    {
        private NpgsqlConnection connection = null;

        public virtual string Database
        {
            get { return DrugDataBuilder.Settings["database"].ToString(); }
        }

       public abstract string Schema
        {
            get;
        }

        public virtual string Server
        {
            get { return DrugDataBuilder.Settings["host"].ToString(); }
        }

        public virtual string Uid
        {
            get { return DrugDataBuilder.Settings["user"].ToString(); }
        }

        public virtual string Password
        {
            get { return DrugDataBuilder.Settings["password"].ToString(); }
        }

        public DrugConnection()
        {
        }

        public bool Valid
        {
            get { return connection != null; }
        }

        public NpgsqlConnection Connection
        {
            get { return connection; }
        }


        public void Dispose()
        {
            connection.Dispose();
            connection = null;
        }

        public void Close()
        {
            connection.Close();
            connection = null;
        }

        public bool Connect()
        {
            if (connection == null)
            {
                string connectionString = String.Format(@"Server={0};Port=5432;User Id={1};Password={2};Database={3}; SearchPath={4}; CommandTimeout = 1200", Server, Uid, Password, Database, Schema);
                connection = new NpgsqlConnection(connectionString);
                try
                {
                    connection.Open();
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(DateTime.Now + " " + ex.Message);
                    connection = null;
                    return false;
                }
            }
            return true;
        }

        public bool RestoreDatabase(string databasePath)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
#if !DEBUG
                info.FileName = "C:\\Program Files\\PostgreSQL\\9.0\\bin\\pg_restore.exe";
#endif
#if DEBUG
                info.FileName = "D:\\Program Files\\PostgreSQL\\9.3\\bin\\pg_restore.exe";
#endif
                info.Arguments = "-h " + Server + " -U " + Uid + " -d " + Database + " \"" + databasePath + "\"";
                info.CreateNoWindow = true;
                info.UseShellExecute = false;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = info;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.EnvironmentVariables["PGPASSWORD"] = Password;
                proc.Start();
                string output = proc.StandardOutput.ReadToEnd();
                string error = proc.StandardError.ReadToEnd();
                proc.WaitForExit();
                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Restore successfully executed");
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                return false;
            }
        }

        public void BackupDatabase(string path, string[] schemas)
        {
            try
            {
                System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
#if !DEBUG
                info.FileName = "C:\\Program Files\\PostgreSQL\\9.0\\bin\\pg_dump.exe";
#endif
#if DEBUG
                info.FileName = "D:\\Program Files\\PostgreSQL\\9.3\\bin\\pg_dump.exe";
#endif
                //"-h localhost -U postgres -n bkup_bcb_datas -n bkup_bcb_etr -n bkup_inps_drug -Fc -f R:\RESIP\2015.02.01\Vision\Download\Gemscript\Gemscript.backup inps
                info.Arguments = "-h " + Server + " -U " + Uid + (schemas.Length > 0 ? " -n " + String.Join(" -n ", schemas) : "") +  " -Fc -f " + path + " " + Database;
                //info.Arguments = "-h localhost -U postgres -n bcb_datas -f C:\\ bcb_test";
                info.CreateNoWindow = true;
                info.UseShellExecute = false;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = info;
                proc.StartInfo.RedirectStandardError = true;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.EnvironmentVariables["PGPASSWORD"] = Password;
                proc.Start();
                proc.BeginOutputReadLine();
                proc.BeginErrorReadLine();
                proc.WaitForExit();
                Debug.WriteLine(DateTime.Now.ToLongTimeString() + " Backup successfully executed");
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " Failed to create gemscript backup\n" + ex.Message);
                throw ex;
            }
        }

        public void RenameSchema(string oldname, string newname)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(String.Format("ALTER SCHEMA {0} RENAME TO {1}", oldname, newname), connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " " + ex.ToString());
            }
        }

        public void DropSchema(string name)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(String.Format("DROP SCHEMA {0} CASCADE", name), connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " " + ex.ToString());
            }
        }

        public void RenameTable(string schema, string oldname, string newname)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(String.Format("ALTER TABLE {0}.{1} RENAME TO {2}", schema, oldname, newname), connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " " + ex.ToString());
            }
        }

        public void CopySchema(string oldname, string newname)
        {
            try
            {
                NpgsqlCommand command = new NpgsqlCommand(String.Format("CREATE TABLE {0} (LIKE {1} INCLUDING CONSTRAINTS INCLUDING INDEXES)", newname, oldname), connection);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(DateTime.Now + " " + ex.ToString());
            }
        }

        public DataSet GetData(string table, string command)
        {
            if (Valid && command.Length > 0)
            {
                DataSet returnData = new DataSet(table);
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(command, connection);
                using (adapter)
                {
                    try
                    {
                        adapter.Fill(returnData, table);
                        return returnData;
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine(DateTime.Now + " DrugConnection.GetData error: " + command + "\n" + e.Message);
                        returnData.Dispose();
                    }
                }
            }
            return null;
        }

        public DataTable GetTable(string command)
        {
            DataSet ds = GetData("table", command);
            if (ds != null)
            {
                return ds.Tables["table"];
            }
            return null;
        }

        public DataRow GetFirstRow(string command)
        {
            DataTable dt = GetTable(command);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0];
            }
            return null;
        }

        public void RunCommand(string query)
        {
            NpgsqlCommand command = new NpgsqlCommand("", connection);
            command.CommandText = query;
            command.ExecuteNonQuery();
        }
    }

    public class BcbConnection : DrugConnection
    {
        public override string Schema
        {
            get { return "bcb_inps"; }
        }

    }

    public class InpsDrugConnection : DrugConnection
    {
        public override string Schema
        {
            get { return "inps_drug"; }
        }
    }

    public class LexDrugConnection : DrugConnection
    {
         public override string Schema
        {
            get { return "historical_drug"; }
        }

    }

    public class BcbDataConnection : DrugConnection
    {
        public override string Schema
        {
            get { return "bcb_datas"; }
        }
    }

    public class TableAttribute
    {
        private string _name;
        private NpgsqlDbType _type;
        private int? _size;

        public string Name { get { return _name; } }
        public NpgsqlDbType Type { get { return _type; } }
        public int? Size { get { return _size; } }
        public string ParameterName { get { return "@s" + _name; } }

        public TableAttribute(string name, NpgsqlDbType type)
        {
            _name = name;
            _type = type;
            _size = null;
        }

        public TableAttribute(string name, NpgsqlDbType type, int size)
        {
            _name = name;
            _type = type;
            _size = size;
        }

        public void AddToCommand(NpgsqlCommand command, object value)
        {
            if (Size.HasValue)
            {
                command.Parameters.Add(ParameterName, Type, Size.Value);
            }
            else
            {
                command.Parameters.Add(ParameterName, Type);
            }
            command.Parameters[ParameterName].Value = value;
        }
    }

    public class TableDefinition
    {
        private string _name;
        private List<TableAttribute> _attributes = new List<TableAttribute>();
        private Dictionary<string, TableAttribute> _attributeMap = new Dictionary<string, TableAttribute>();

        public List<TableAttribute> Attributes
        {
            get { return _attributes; }
        }

        public string Name
        {
            get { return _name; }
        }

        public TableDefinition(string name)
        {
            _name = name;
        }

        public TableAttribute GetAttribute(string name)
        {
            if (_attributeMap.ContainsKey(name))
                return _attributeMap[name];
            return null;
        }

        public void AddAttribute(TableAttribute attribute)
        {
            _attributes.Add(attribute);
            _attributeMap.Add(attribute.Name, attribute);
        }
    }

    public class TableRow
    {
        private TableDefinition _definition;
        private Dictionary<string, object> _values;

        public TableRow(TableDefinition definition)
        {
            _definition = definition;
            _values = new Dictionary<string, object>();
        }

        public void SetValue(string attribute, object value)
        {
            if (_definition.GetAttribute(attribute) != null)
            {
                if (_values.ContainsKey(attribute))
                    _values.Remove(attribute);

                _values.Add(attribute, value);
            }
        }

        public void SetValue(string attribute, Concept value)
        {
            if (value != null)
                SetValue(attribute, value.Id);
            else
                SetValue(attribute, (object)null);
        }

        public void Clear()
        {
            _values.Clear();
        }

        public Dictionary<string, object> Values
        {
            get { return _values; }
        }

        public TableDefinition Definition
        {
            get { return _definition; }
        }

        public bool AppendToTable(NpgsqlCommand command)
        {
            StringBuilder attributeList = new StringBuilder();
            StringBuilder parameterList = new StringBuilder();
            string separator = "";
            foreach (string attributeName in _values.Keys)
            {
                TableAttribute attribute = _definition.GetAttribute(attributeName);
                if (attribute != null)
                {
                    attributeList.Append(separator + "\"" + attribute.Name + "\"");
                    parameterList.Append(separator + attribute.ParameterName);
                    separator = ",";
                    attribute.AddToCommand(command, _values[attributeName]);
                }
                else
                {
                    return false;
                }
            }

            if (attributeList.Length > 0)
            {
                
                    command.CommandText = String.Format(@"insert into {0} ({1}) values({2})", _definition.Name, attributeList, parameterList);
                try
                {
                    command.ExecuteNonQuery();
                    command.CommandText = "";
                    command.Parameters.Clear();
                    return true;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(DateTime.Now + " error in " + command.CommandText);
                    Trace.WriteLine(e.Message);
                }
            }
            return false;
        }

    }

    public class TableUpdater
    {
        private TableRow _whereClause; // Use table row for now. No support for >, < etc

        public TableRow WhereClause
        {
            get { return _whereClause; }
        }

        public TableUpdater(TableDefinition definition)
        {
            _whereClause = new TableRow(definition);
        }

        public bool AppendToTable(NpgsqlCommand command, TableRow data)
        {
            //Trace.WriteLine("Method: AppendToTable");
            command.CommandText = "";
            command.Parameters.Clear();
            StringBuilder attributeList = new StringBuilder();
            StringBuilder parameterList = new StringBuilder();
            string separator = "";
            foreach (string attributeName in data.Values.Keys)
            {
                TableAttribute attribute = data.Definition.GetAttribute(attributeName);
                if (attribute != null)
                {
                    attributeList.Append(separator + attribute.Name);
                    parameterList.Append(separator + attribute.ParameterName);
                    separator = ",";
                    attribute.AddToCommand(command, data.Values[attributeName]);
                }
                else
                {
                    return false;
                }
            }

            if (attributeList.Length > 0)
            {
                try
                {
                    command.CommandText = String.Format(@"insert into {0} ({1}) values({2})", data.Definition.Name, attributeList, parameterList);
                    //Trace.WriteLine(command.CommandText);
                    command.ExecuteNonQuery();
                    command.CommandText = "";
                    command.Parameters.Clear();
                    return true;
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Error thrown executing command " + command.CommandText);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Table row data:");
                        foreach (KeyValuePair<string, object> kvp in data.Values)
                            sb.Append(" key: {" + kvp.Key + "}; value: {" + kvp.Value + "}");
                    Trace.WriteLine("Table row data " + sb);
                    throw e;
                }
            }
            return false;
        }

        public bool UpdateInTable(NpgsqlCommand command, TableRow data)
        {
            StringBuilder setCommand = new StringBuilder();
            StringBuilder whereCommand = new StringBuilder();
            string separator = "";
            foreach (string attributeName in data.Values.Keys)
            {
                TableAttribute attribute = data.Definition.GetAttribute(attributeName);
                if (attribute != null)
                {
                    setCommand.Append(separator);
                    setCommand.Append(attribute.Name);
                    setCommand.Append("=");
                    setCommand.Append(attribute.ParameterName);
                    separator = ", ";

                    attribute.AddToCommand(command, data.Values[attributeName]);
                }
                else
                {
                    return false;
                }
            }

            separator = "";
            foreach (string attributeName in WhereClause.Values.Keys)
            {
                TableAttribute attribute = data.Definition.GetAttribute(attributeName);
                if (attribute != null)
                {
                    whereCommand.Append(attribute.Name);
                    whereCommand.Append(" = ");
                    whereCommand.Append(attribute.ParameterName); // TODO need different parameter names for update,where, order by etc
                    separator = " AND ";

                    attribute.AddToCommand(command, WhereClause.Values[attributeName]);
                }
            }
            if (setCommand.Length > 0 && whereCommand.Length > 0)
            {
                command.CommandText = String.Format(@"UPDATE {0} SET {1} WHERE {2}", data.Definition.Name, setCommand, whereCommand);
                command.ExecuteNonQuery();
                command.CommandText = "";
                command.Parameters.Clear();
                return true;
            }
            else if (setCommand.Length > 0)
            {
                command.CommandText = String.Format(@"UPDATE {0} SET {1}", data.Definition.Name, setCommand);
                command.ExecuteNonQuery();
                command.CommandText = "";
                command.Parameters.Clear();
                return true;
            }
            return false;
        }
    }

    public class DataRowReader
    {
        private DataRow _row;

        public DataRowReader(DataRow row)
        {
            _row = row;
        }

        public bool HasData
        {
            get { return _row != null; }
        }

        public bool IsNull(string field)
        {
            return _row == null || _row.IsNull(field);
        }

        public string GetString(string field)
        {
            return _row == null || _row.IsNull(field) ? "" : _row[field].ToString();
        }

        public UInt64 GetConceptId(string field)
        {
            return _row == null || _row.IsNull(field) ? 0 : Convert.ToUInt64(_row[field]);
        }

        public UInt32 GetId(string field)
        {
            return _row == null || _row.IsNull(field) ? 0 : Convert.ToUInt32(_row[field]);
        }

        public int GetInt(string field)
        {
            return _row == null || _row.IsNull(field) ? 0 : Convert.ToInt32(_row[field]);
        }

        public float GetFloat(string field)
        {
            return _row == null || _row.IsNull(field) ? 0 : float.Parse(_row[field].ToString());
        }

        public Double GetDouble(string field)
        {
            return _row == null || _row.IsNull(field) ? 0 : Convert.ToDouble(_row[field]);
        }

        public bool GetBoolean(string field)
        {
            return GetInt(field) == 1;
        }

        public DateTime GetDate(string field)
        {
            return _row == null || _row.IsNull(field) ? new DateTime(0) : Convert.ToDateTime(_row[field]);
        }

        public bool GetYNBoolean(string field, bool defaultValue)
        {
            string value = GetString(field).ToLower();
            if (value == "y")
                return true;
            else if (value == "n")
                return false;
            return defaultValue;
        }
    }

    public class CopyData
    {
        private NpgsqlConnection _connection = null;

        public CopyData(NpgsqlConnection conn)
        {
            _connection = conn;
        }

        public void CopyIn(string table, Stream file, String[] columns = null)
        {
            NpgsqlCommand command 
                = new NpgsqlCommand(String.Format("COPY {0} {1} FROM STDIN", table, columns != null? '(' + String.Join(",", columns) + ')' : ""), _connection);
            int t = _connection.ProcessID;
            NpgsqlCopyIn cin = new NpgsqlCopyIn(command, command.Connection, file);
            try
            {
                cin.Start();
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                cin.End();
                file.Close();
            }
        }

        public void CopyOutCsvFormat(string query, Stream file)
        {
            NpgsqlCommand command = new NpgsqlCommand(String.Format("COPY ({0}) to STDOUT CSV Quote $$'$$ FORCE QUOTE *", query), _connection);
            NpgsqlCopyOut cout = new NpgsqlCopyOut(command, command.Connection, file);
            try
            {
                cout.Start();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cout.End();
                file.Close();
            }
        }

        public void CopyOutPipelineFormat(string query, Stream file)
        {   
            NpgsqlCommand command = new NpgsqlCommand(String.Format("COPY ({0}) to STDOUT DELIMITER '|' CSV Quote $$'$$", query), _connection);
            NpgsqlCopyOut cout = new NpgsqlCopyOut(command, command.Connection, file);
            try
            {
                cout.Start();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                cout.End();
                file.Close();
            }
        }
    }
}
