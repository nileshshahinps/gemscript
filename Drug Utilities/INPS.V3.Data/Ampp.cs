﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace INPS.V3.Data
{

    public class AmppDetails : DmdProductDetails
    {
        private AmpDetails _parentAmp;
        private VmppDetails _parentVmpp;
        private DataRowReader _priceRow;
        private DataRowReader _packInfoRow;
        private DataRowReader _prescribingInfoRow;
        private DataRowReader _reimbursementInfoRow;
        private Concept _legalStatus;

        private bool? _isInvalid = null;
        private bool? _isDiscontinued = null;
        private bool? _isAvailable = null;

        public AmppDetails(DataRow source, AmpDetails parentAmp)
            : base(source)
        {
            _parentAmp = parentAmp;
            _parentVmpp = _parentAmp.Vmp.GetVmpp(VmppCode);
        }

        public override VmpDetails Vmp
        {
            get { return Amp.Vmp; }
        }
        public ulong VmppCode
        {
            get { return ProductRow.GetConceptId("vmpp_id"); }
        }

        public AmpDetails Amp
        {
            get { return _parentAmp; }
        }

        public VmppDetails Vmpp
        {
            get { return _parentVmpp; }
        }

        public override UInt64 PrevDmdCode
        {
            get { return 0; }
        }

        public override string Name
        {
            get { return ProductRow.GetString("full_name"); }
        }

        public int Price
        {
            get { return PriceRow.GetInt("price"); }
        }

        public override bool IsDiscontinued
        {
            get 
            {
                if (_isDiscontinued == null)
                    _isDiscontinued = ProductRow.GetBoolean("discontinued_ind") || IsAuthoredDiscontinued || Vmpp.IsAuthoredDiscontinued || Amp.IsAuthoredDiscontinued || Vmp.IsAuthoredDiscontinued;
                return (bool)_isDiscontinued; 
            }
        }

        public bool DiscontinuedBefore(DateTime date)
        {
            if (!ProductRow.GetBoolean("discontinued_ind")) return false;
            return !(ProductRow.GetDate("discontinued_date") == new DateTime(0)) && ProductRow.GetDate("discontinued_date").CompareTo(date) < 0;
        }

        public Concept LegalStatus
        {
            get
            {
                GenerateConcept(ref _legalStatus, "legal_category", ConceptDictionary.CommonConcepts.LegalStatus);
                return _legalStatus;
            }
        }

        public string OrderNumber
        {
            get { return PackInfoRow.GetString("pack_order_no"); }
        }

        public string SubPack
        {
            get { return ProductRow.GetString("sub_pack_info"); }
        }

        public override bool IsFp10MDA
        {
            get { return PrescribingInfoRow.GetBoolean("fp10_mda_ind"); }
        }

        public override bool IsDohApproved
        {
            get { return Vmpp.IsDohApproved; }
        }

        public override bool IsAcbs
        {
            get { return PrescribingInfoRow.GetBoolean("acbs_ind"); }
        }

        public override bool IsSls
        {
            get { return PrescribingInfoRow.GetBoolean("schedule2_ind"); }
        }

        public override bool IsBlacklisted
        {
            get { return PrescribingInfoRow.GetBoolean("schedule1_ind"); }
        }

        public override bool IsBlackTriangle
        {
            get { return Amp.IsBlackTriangle; }
        }

        public override bool IsPa
        {
            get { return PrescribingInfoRow.GetBoolean("padm_ind"); }
        }

        public override bool IsNurseFormulary
        {
            get { return PrescribingInfoRow.GetBoolean("nurse_formulary_ind"); }
        }

        public bool IsDivisible
        {
            get { return ReimbursementInfoRow.GetBoolean("broken_bulk_ind"); }
        }

        public bool IsPrescribable
        {
            get { return (PackInfoRow.GetInt("reimbursement_status") == 1); }
        }

        public override bool IsSpecial
        {
            get { return Amp.IsSpecial; }
        }

        public override bool IsInvalid
        {
            get 
            { 
                if (_isInvalid == null)
                    _isInvalid = ProductRow.GetBoolean("invalid") || Amp.Vmp.IsMarkedInvalid || Amp.IsMarkedInvalid || Vmpp.IsMarkedInvalid || DiscontinuedBefore(DmdProductDetails.DISCONTINUED_CUTOFF_DATE);
                return (bool)_isInvalid;
            }
        }

        //don't check whther amp is active, as may noe vbe active but atill be available as a special - in such case active ampp will only appear when special selected in Vision
        public override bool IsActive
        {
            get { return !IsInvalid && (IsAvailable); }
        }

        public override bool IsParallelImport
        {
            get { return Amp.IsParallelImport; }
        }

        public override bool IsExtemp
        {
            get { return Amp.IsExtemp; }
        }

        public override bool IsImported
        {
            get { return Amp.IsImported; }
        }

        public override bool IsSecondaryCare
        {
            get { return PrescribingInfoRow.GetBoolean("hospital_ind") || Amp.AvailabilityRestriction == 8; }
        }

        public override bool IsReimbursableDevice
        {
            get { return (Amp.LicensingAuthority == 2 || (Amp.LicensingAuthority == 3 && PackInfoRow.GetString("reimbursement_status") != "")) &&  PackInfoRow.GetInt("reimbursement_status") == 1; }
        }

        public override bool IsReimbursableDeviceEngland
        {
            get { return (Amp.LicensingAuthority == 2 || (Amp.LicensingAuthority == 3 && PackInfoRow.GetString("reimbursement_status") != "")) && Amp.RData.Flags.Value.CountrySpecificitiesEngland == 1; }
        }

        public override bool IsReimbursableDeviceWales
        {
            get { return (Amp.LicensingAuthority == 2 || (Amp.LicensingAuthority == 3 && PackInfoRow.GetString("reimbursement_status") != "")) && Amp.RData.Flags.Value.CountrySpecificitiesWales == 1; }
        }

        public override bool IsReimbursableDeviceNI
        {
            get { return (Amp.LicensingAuthority == 2 || (Amp.LicensingAuthority == 3 && PackInfoRow.GetString("reimbursement_status") != "")) && Amp.RData.Flags.Value.CountrySpecificitiesNireland == 1; }
        }

        public override bool IsReimbursableDeviceScotland
        {
            get { return (Amp.LicensingAuthority == 2 || (Amp.LicensingAuthority == 3 && PackInfoRow.GetString("reimbursement_status") != "")) && Amp.RData.Flags.Value.CountrySpecificitiesScotland == 1; }
        }

        public override bool IsMedicine
        {
            get { return Amp.LicensingAuthority == 0 || Amp.LicensingAuthority == 1 || Amp.LicensingAuthority == 4 || (Amp.LicensingAuthority == 3 && PackInfoRow.GetString("reimbursement_status") == ""); }
        }

        public override bool IsUnlicensed
        {
            get { return Amp.LicensingAuthority == 0 || Amp.LicensingAuthority == 3; }
        }
        
        public override bool IsAvailable
        {
            get
            {
                if (_isAvailable == null)
                    _isAvailable = !IsComponentOnlyPack;
                return (bool)_isAvailable; 
            }
        }

        public bool IsComponentOnlyPack
        {
            get { return ProductRow.GetInt("combination_pack") == 2; }
        }

        public override bool IsScottishNurseFormulary
        {
            get { return Amp.IsScottishNurseFormulary; }
        }

        public override bool UseGeneric
        {
            get { return Amp.UseGeneric; }
        }

        public override bool UseBrand
        {
            get { return Amp.UseBrand; }
        }

        protected DataRowReader PriceRow
        {
            get
            {
                ReadRelatedRow(ref _priceRow, String.Format(@"select * from bcb_inps.ampp_price_info where ampp_id = {0}", DmdCode));
                return _priceRow;
            }
        }

        protected DataRowReader PackInfoRow
        {
            get
            {
                ReadRelatedRow(ref _packInfoRow, String.Format(@"select * from bcb_inps.ampp_pack_info where ampp_id = {0}", DmdCode));
                return _packInfoRow;
            }
        }

        protected DataRowReader PrescribingInfoRow
        {
            get
            {
                ReadRelatedRow(ref _prescribingInfoRow, String.Format(@"select * from bcb_inps.ampp_prescribing_info where ampp_id = {0}", DmdCode));
                return _prescribingInfoRow;
            }
        }

        protected DataRowReader ReimbursementInfoRow
        {
            get
            {
                ReadRelatedRow(ref _reimbursementInfoRow, String.Format(@"select broken_bulk_ind from bcb_inps.ampp_reimbursement_info where ampp_id = {0}", DmdCode));
                return _reimbursementInfoRow;
            }
        }
    }

}
