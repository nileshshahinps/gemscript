﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;

namespace INPS.V3.Data
{
    public class VmpDetails : DmdProductDetails
    {
        //private Dictionary<UInt64, AmpDetails> _ampMap;
        private List<AmpDetails> _amps;
        private Dictionary<UInt64, VmppDetails> _vmppMap;
        private List<VmppDetails> _vmpps;

        private List<DataRowReader> _ingredientRows;
        private DataRowReader _cdcRow;
        private DataRowReader _doseFormRow;

        private int? _schedule;

        private Concept _form;
        private Concept _vmpForm;
        private Concept _vtm;
        private Concept _cdc;
        private Concept _strength;
        private Concept _prescribingStatusConcept;

        private Boolean? _isFp10;

        //this may need to be a struct to include strength and unit
        private List<TypeIngredient> _activeIngredients;

       
        //private List<Concept> _contraindications;
        //private List<Concept> _precautions;
        //private List<Concept> _sideEffects;
        //private List<Concept> _labelWarnings;
        //private List<Concept> _uses;
        private List<Concept> _routes;

        private bool? _isInvalid = null;
        private bool? _isDiscontinued = null;
        private bool? _isAvailable = null;

        private VmpFlags _flags;

        public VmpFlags Flags
        {
            get 
            { 
                    if (_flags == null) _flags = new VmpFlags(this);
                    return _flags; 
            }
        }

        public VmpDetails(DataRow source)
            : base(source)
        {                
        }

        public override Concept Vtm
        {
            get 
            {
                GenerateConcept(ref _vtm, "vtm_id", ConceptDictionary.CommonConcepts.Vtm);
                return _vtm;
            }
        }

        public override VmpDetails Vmp
        {
            get { return this; }
        }

        public List<AmpDetails> Amps
        {
            get
            {
                ReadAmps();
                return _amps;
            }
        }

        public List<VmppDetails> Vmpps
        {
            get
            {
                ReadVmpps();
                return _vmpps;
            }
        }


        /*public AmpDetails GetAmp(UInt64 id)
        {
            ReadAmps();
            AmpDetails amp;
            _ampMap.TryGetValue(id, out amp);
            return amp;
        }*/

        public VmppDetails GetVmpp(UInt64 id)
        {
            ReadVmpps();
            VmppDetails vmpp;
            _vmppMap.TryGetValue(id, out vmpp);
            return vmpp;
        }

        public Int32 PrescribingStatus
        {
            get { return ProductRow.GetInt("prescribing_status"); }
        }

        public Concept PrescribingStatusConcept
        {
            get
            {
                GenerateConcept(ref _prescribingStatusConcept, "prescribing_status", ConceptDictionary.CommonConcepts.PrescribingSatus);
                return _prescribingStatusConcept;
            }
        }

        public override string Name
        {
            get 
            {
                return ProductRow.GetString("full_name"); 
            }
        }

        public ushort ProductType
        {
            get { return 2; }
        }

        public bool IsMarkedInvalid
        {
            get { return ProductRow.GetBoolean("invalid"); }
        }

        public bool IsCombinationProduct
        {
            get { return ProductRow.GetInt("combination_product_indicator") == 1; }
        }

        public bool IsComponentOnlyProduct
        {
            get { return ProductRow.GetInt("combination_product_indicator") == 2; }
        }

        public override bool IsInvalid
        {
            get 
            { 
                if (_isInvalid == null) 
                    _isInvalid = Amps.All(amp => amp.IsInvalid /*|| amp.IsDiscontinued != IsDiscontinued*/);
                return (bool)_isInvalid;
            }
        }


        public bool IsReplaced
        {
            get { return IsMlex2DmdDuplicate; }
        }

        public Concept Category
        {
            get
            {
                //list of concept descriptions by priority
                List<string> categories = new List<String> { "Oxygen", "Reagent", "Appliance", "Medication" };
                Concept category = null;
                Concept thisCategory = null;
                for (int i = 0; i < this.RData.RdcCodes.Count; i++)
                {
                    thisCategory = Concepts.LoadInpsConcept(Rdc.GetRdcCategory(this.RData.RdcCodes[i].Code));
                    if (category == null)
                        category = thisCategory;
                    else if (categories.IndexOf(thisCategory.Description) < categories.IndexOf(category.Description))
                        category = thisCategory;
                }

                return category;
            }
        }

        public Concept VmpForm
        {
            get
            {
                if (_vmpForm == null && DoseFormRow != null)
                    _vmpForm = GenerateConcept(DoseFormRow.GetConceptId("formulation_code"), ConceptDictionary.CommonConcepts.Formulation);
                return _vmpForm;
            }
        }

        public override Concept Form
        {
            get
            {
                if (_form == null)
                {
                    if (VmpForm == null || VmpForm.Description == "Not applicable")
                    {
                        if (Vmpps.Count > 0)
                            _form = Vmpps.First().UOM;
                    }
                    else _form = VmpForm;
                }
                return _form;
            }
        }

        public Concept Schedule
        {
            get
            {
                if (_cdc == null && CdcRow != null)
                {
                    _cdc = GenerateConcept(CdcRow.GetConceptId("cdc_code"), ConceptDictionary.CommonConcepts.CDC);
                }
                return _cdc;
            }
        }

        public override Concept Strength
        {
            get
            {
                if (_strength == null)
                { //todo - multiple ingredients e.g. "25mg/100ml + 50mg/50ml"
                    StringBuilder strengthText = new StringBuilder();
                    int count = 0;
                    //if (IngredientRow.Count >1)
                    //    Trace.WriteLine("here");
                    //check authored composition data first
                    foreach (DataRowReader row in IngredientRows)
                    {
                        if (!row.IsNull("strength_numerator") && !row.IsNull("strength_numerator_unit"))
                        {
                            if (count > 0) strengthText.Append(" + ");
                            float numeratorVal = row.GetFloat("strength_numerator");
                            Concept numeratorUnit = GenerateConcept(row.GetConceptId("strength_numerator_unit"), ConceptDictionary.CommonConcepts.Unit);
                            strengthText.AppendFormat("{0:0.##}", row.GetFloat("strength_numerator"));
                            strengthText.Append(numeratorUnit.Description);
                            if (!row.IsNull("strength_denominator") && !row.IsNull("strength_denominator_unit"))
                            {
                                Concept denominatorUnit = GenerateConcept(row.GetConceptId("strength_denominator_unit"), ConceptDictionary.CommonConcepts.Unit);
                                strengthText.AppendFormat("/{0:0.##}", row.GetFloat("strength_denominator"));
                                strengthText.Append(denominatorUnit.Description);
                            }
                            count = 1;
                        }
                    }
                    _strength = Concepts.CreateInpsConcept(ConceptDictionary.CommonConcepts.Strength, strengthText.ToString());
                }
                return _strength;
            }
        }

        public override float NormalisedStrength
        {
            get
            {
                //if units are mcg or g convert to mcg, if L convert to ml
                //for sorting only - NOT FOR DISPLAY

                if (StrengthDenominator == null)
                    return StrengthValue;
                if (StrengthDenominator.Description == "microgram")
                    return StrengthValue / 1000;
                else if (StrengthDenominator.Description == "gram")
                    return StrengthValue * 1000;
                else if (StrengthDenominator.Description == "litre")
                    return StrengthValue * 1000;                
                else return StrengthValue;
            }
        }

        public override float StrengthValue
        {
            get
            {
                if (IngredientRows.Count > 0)
                    return IngredientRows[0].GetFloat("strength_numerator");
                else return 0;
            }
        }

        public Concept StrengthDenominator
        {
            get
            {
                if (IngredientRows.Count > 0)
                    return GenerateConcept(IngredientRows[0].GetConceptId("strength_numerator_unit"), ConceptDictionary.CommonConcepts.Unit);
                else return null;
            }
        }

        public override bool IsDiscontinued
        {
            get 
            {
                if (_isDiscontinued == null)
                    _isDiscontinued = Amps.All(amp => amp.IsDiscontinued || amp.IsInvalid != IsInvalid);
                return (bool)_isDiscontinued; 
            }
        }

        public override bool IsFp10MDA
        {
            get { return Amps.Any(amp => amp.IsFp10MDA && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsDohApproved
        {
            get { return Vmpps.Any(vmpp => vmpp.IsDohApproved && vmpp.IsInvalid == IsInvalid && vmpp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsControlled
        {
            get { return ScheduleNumber > 0 && ScheduleNumber < 4 /*&& !(Name.StartsWith("Temazepam"))*/; }
        }

        public override bool IsAcbsEngland
        {
            get { return Amps.Any(amp => amp.IsAcbsEngland && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsAcbsWales
        {
            get { return Amps.Any(amp => amp.IsAcbsWales && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsAcbsScotland
        {
            get { return Amps.Any(amp => amp.IsAcbsScotland && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsAcbsNI
        {
            get { return Amps.Any(amp => amp.IsAcbsNI && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsSls
        {
            get { return Amps.Any(amp => amp.IsSls && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsBlacklisted
        {
            get { return PrescribingStatus != 1; }
        }

        public override bool IsBlackTriangle
        {
            get { return RData.Flags.Value.BlackTriangle == 1 || Amps.Any(amp => amp.IsBlackTriangle && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsPa
        {
            get { return Amps.Any(amp => amp.IsPa && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsNurseFormulary
        {
            get { return Amps.Any(amp => amp.IsNurseFormulary && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public bool IsInpsSpecial
        {
            get { return (Amps.All(amp => amp.IsSpecial || amp.IsInvalid != IsInvalid || amp.IsDiscontinued != IsDiscontinued)); }
        }

        public override bool IsSpecial
        {
            get { return (IsImported || IsInpsSpecial || Amps.All(amp => amp.IsSpecial || amp.IsInvalid != IsInvalid || amp.IsDiscontinued != IsDiscontinued)); }
        }

        public bool CanHaveAfEndorsement
        {
            get { return Amps.Count(amp => amp.Flavour != null && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued) > 1; }
        }

        public override bool IsParallelImport
        {
            get { return Amps.All(amp => amp.IsParallelImport || amp.IsInvalid != IsInvalid || amp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsExtemp
        {
            get { return Amps.All(amp => amp.IsExtemp || amp.IsInvalid != IsInvalid || amp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsReimbursableDevice
        {
            get { return Amps.Any(amp => amp.IsReimbursableDevice && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsReimbursableDeviceEngland
        {
            get { return Amps.Any(ampp => ampp.IsReimbursableDeviceEngland && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceWales
        {
            get { return Amps.Any(ampp => ampp.IsReimbursableDeviceWales && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceNI
        {
            get { return Amps.Any(ampp => ampp.IsReimbursableDeviceNI && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsReimbursableDeviceScotland
        {
            get { return Amps.Any(ampp => ampp.IsReimbursableDeviceScotland && ampp.IsDiscontinued == IsDiscontinued && ampp.IsInvalid == IsInvalid); }
        }

        public override bool IsMedicine
        {
            get { return Amps.Any(amp => amp.IsMedicine && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override bool IsUnlicensed
        {
            get { return Amps.Any(amp => amp.IsUnlicensed && amp.IsInvalid == IsInvalid && amp.IsDiscontinued == IsDiscontinued); }
        }

        public override int ScheduleNumber
        {
            get
            {
                if (!_schedule.HasValue)
                {
                    _schedule = 0;
                    Concept cdc = Schedule;
                    if (cdc != null)
                    {
                        string number = cdc.Description.ToLower().Replace("schedule ", "");
                        //System.Diagnostics.Trace.WriteLine(number);
                        _schedule = Convert.ToInt16(number.Substring(0, 1));
                    }
                }
                return _schedule.Value;
            }
        }

        public override bool IsInteractionControlled
        {
            /*true if dm+D drug or gemscript authored product which is NOT
             * medication with active ingredient excl homeopathic)
             * appliance with active ingredient*/
            get { return IsDmd || Category == null || Rdc.IsHomeopathic(RData.RdcCodes) || (Category.Description != "Medication" && ActiveIngredients.Count == 0); }
        }

        public List<Concept> Routes
        {
            get
            {
                ReadRoutes();
                return _routes;
            }
        }

        public List<TypeIngredient> ActiveIngredients
        {
            //this only contains dm+d ingredients, not authored ingredients
            get
            {
                ReadIngredients();
                return _activeIngredients;
            }
        }

        private DataRowReader CdcRow
        {
            get
            {
                ReadRelatedRow(ref _cdcRow, String.Format(@"select * from bcb_inps.vmp_control_drug_cat where vmp_id = {0}", DmdCode));
                return _cdcRow;
            }
        }

        public override UInt64 PrevDmdCode
        {
            get { return ProductRow.GetConceptId("prev_id"); }
        }

        private DataRowReader DoseFormRow
        {
            get
            {
                ReadRelatedRow(ref _doseFormRow, String.Format(@"select * from bcb_inps.vmp_formulation where vmp_id = {0}", DmdCode));
                return _doseFormRow;
            }
        }

        private bool ReadAmps()
        {
            if (_amps == null)
            {
                _amps = new List<AmpDetails>();

                ///BcbConnection.Connect();
                //string command = String.Format(@"select * from bcb_inps.amp where vmp_id = {0} and parallel_import_ind=0", DmdCode.ToString());

                //include parallel import but make issuable = false
                string command = String.Format(@"select ""ID"", invalid, description, supplier, licensing_authority, combination_product, flavour, csm_monitoring_ind, parallel_import_ind, availability_restriction from bcb_inps.amp where vmp_id = {0}", DmdCode.ToString());
                DataTable results = BcbConnection.GetTable(command);
                foreach (DataRow row in results.Rows)
                {
                    AmpDetails amp = new AmpDetails(row, this);
                    _amps.Add(amp);
                }
                //add any children of reconciled VMP if AMP not reconciled itself
                if (IsReconciledDmdProduct)
                {
                    command = String.Format(@"select * from bcb_inps.amp where vmp_id = {0}", ReconciledGemscriptId.ToString());
                    results = BcbConnection.GetTable(command);
                    foreach (DataRow row in results.Rows)
                    {
                        AmpDetails amp = new AmpDetails(row, this);
                        if (!amp.IsReconciledGemscriptProduct)
                            _amps.Add(amp);
                    }
                }

                //BcbConnection.Close();
            }
            return _amps.Count > 0;
        }

        private bool ReadVmpps()
        {
            if (_vmppMap == null)
            {
                _vmppMap = new Dictionary<ulong, VmppDetails>();
                _vmpps = new List<VmppDetails>();

                //BcbConnection.Connect();
                string command = String.Format(@"select * from vmpp where vmp_id = {0}", DmdCode.ToString());
                DataTable results = BcbConnection.GetTable(command);
               	foreach (DataRow row in results.Rows)
                {
                    VmppDetails vmpp = new VmppDetails(row, this);
                    _vmppMap.Add(vmpp.DmdCode, vmpp);
                    _vmpps.Add(vmpp);
                }

                //add any children of reconciled VMP (nb will not be output if all AMPPS reconciled)
                if (IsReconciledDmdProduct)
                {
                    command = String.Format(@"select * from bcb_inps.vmpp where vmp_id = {0}", ReconciledGemscriptId.ToString());
                    results = BcbConnection.GetTable(command);
                    foreach (DataRow row in results.Rows)
                    {
                        VmppDetails vmpp = new VmppDetails(row, this);
                        _vmppMap.Add(vmpp.DmdCode, vmpp);
                        _vmpps.Add(vmpp);
                    }
                }
                //BcbConnection.Close();
            }
            return _vmppMap.Count > 0;
        }

        private bool ReadRoutes()
        {
            if (_routes == null)
            {
                _routes = new List<Concept>();
                //BcbConnection.Connect();
                string command = String.Format(@"select * from vmp_route where vmp_id = {0}", DmdCode.ToString());
                DataTable results = BcbConnection.GetTable(command);
                foreach (DataRow row in results.Rows)
                {
                    Concept newConcept = GenerateConcept(Convert.ToUInt64(row["route_code"]), ConceptDictionary.CommonConcepts.Route);
                    if (newConcept != null)
                        _routes.Add(newConcept);
                }
                //add routes from bcb

                //BcbConnection.Close();
                foreach (Resip.ResipAPI.TypeRoute route in RData.Routes)
                {
                    Concept newConcept = GenerateConcept(Convert.ToUInt64(route.Id), ConceptDictionary.CommonConcepts.Route);
                    if (newConcept != null)
                        _routes.Add(newConcept);
               }
            }
            if (_routes == null || _routes.Count < 1)
                _routes.Add(GenerateConcept(3594011000001102, ConceptDictionary.CommonConcepts.Route));
            return _routes.Count > 0;
        }

        private bool ReadIngredients()
        {
            if (_activeIngredients == null)
            {
                _activeIngredients = new List<TypeIngredient>();
                //string command = String.Format(@"select * from vmp_ingredients join ingredients i on ingredient_id = i.id where vmp_id = {0}", DmdCode);
                //DataTable results = BcbConnection.GetTable(command);
                //_ingredientRows = new List<DataRowReader>();
                foreach (DataRowReader row in IngredientRows)
                {
                    TypeIngredient ingredient= new TypeIngredient();
                    ingredient.Ingredient = Concepts.ImportExternalConcept(ConceptDictionary.CommonConcepts.ActiveIngredient, new Concept(row.GetConceptId("id"), row.GetString("name")));
                    ingredient.Strength = row.GetDouble("strength_numerator");
                    //ingredient.Unit = row.GetConceptId("strength_numerator_unit");
                    UInt64 i = row.GetConceptId("strength_numerator_unit");
                    if (row.GetConceptId("strength_numerator_unit") != 0)
                        ingredient.Unit = Concepts.ImportExternalConcept(ConceptDictionary.CommonConcepts.Unit, new Concept(row.GetConceptId("strength_numerator_unit"), row.GetString("description")));
                    _activeIngredients.Add(ingredient);
                }
            }
            return _activeIngredients.Count > 0;
        }

        private List<DataRowReader> IngredientRows
        {
            get
            {
                try
                {
                    string query = String.Format(@"select * from vmp_ingredients vi join ingredients i on vi.ingredient_id = i.id left join lk_unit_of_measure uom on uom.code = vi.strength_numerator_unit where vmp_id = {0}", DmdCode);
                    DataTable results = BcbConnection.GetTable(query);
                    _ingredientRows = new List<DataRowReader>();
                    foreach (DataRow row in results.Rows)
                    {
                        _ingredientRows.Add(new DataRowReader(row));
                    }
                    //add ingredients for component products if is combination product
                    if (IsCombinationProduct)
                    {
                        //get list of ingredients
                        query = "SELECT DISTINCT i.*, vi.*, uom.* FROM vmpp vp1 JOIN vmpp_combination_pack vcp ON vcp.parent_pack_id = vp1.\"ID\" " +
	                            "JOIN vmpp vp2 ON vp2.\"ID\" = vcp.child_pack_id JOIN vmp v2 ON v2.\"ID\" = vp2.vmp_id JOIN vmp_ingredients vi ON v2.\"ID\" = vi.vmp_id JOIN ingredients i ON i.id = vi.ingredient_id " +
                                " LEFT JOIN lk_unit_of_measure uom ON uom.code = vi.strength_numerator_unit WHERE vp1.vmp_id = " + DmdCode;
                        results = BcbConnection.GetTable(query);
                        foreach (DataRow row in results.Rows)
                        {
                            _ingredientRows.Add(new DataRowReader(row));
                        }
                    }
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Error creating IngredientRow: " + e.Message);
                    throw e;
                }
                return _ingredientRows;
            }
        }
        /******************************** ENSURE THESE VARIABLES ARE UPDATED IN VmpFlags SECTION OF CODE***********************************************************/
        public override bool IsActive
        {
            get
            {
                return (IsAvailable || Amps.Any(amp => amp.IsAvailable) && !IsMlex2DmdDuplicate);
            }
        }

        public override bool IsAvailable
        {
            get
            {
                if (_isAvailable == null)
                    _isAvailable = !IsInvalid && PrescribingStatus != 2 && PrescribingStatus != 3 && PrescribingStatus != 4 & !IsComponentOnlyProduct;
                return (bool)_isAvailable;
            }
        }

        public override bool IsFp10
        {
            get
            {
                if (_isFp10 != null) return (Boolean)_isFp10;
                return IsFp10England;
            }
            set { _isFp10 = value; }
        }

        public override bool IsFp10England
        {
            get { return IsIssuable && RData.Flags.Value.CountrySpecificitiesEngland == 1; }
        }

        public override bool IsFp10Wales
        {
            get { return IsIssuable && RData.Flags.Value.CountrySpecificitiesWales == 1; }
        }

        public override bool IsFp10Scotland
        {
            get { return IsIssuable && RData.Flags.Value.CountrySpecificitiesScotland == 1; }
        }

        public override bool IsFp10NI
        {
            get { return IsIssuable && RData.Flags.Value.CountrySpecificitiesNireland == 1; }
        }

        public override bool IsImported
        {
            get { return Amps.All(amp => amp.IsImported || amp.IsInvalid != IsInvalid || amp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool IsSecondaryCare
        {
            get { return Amps.All(amp => amp.IsSecondaryCare || amp.IsInvalid != IsInvalid || amp.IsDiscontinued != IsDiscontinued); }
        }

        public override bool UseGeneric
        {
            get { return IsAvailable; }
        }

        public override bool UseBrand
        {
            get { return Amps.Any(amp => amp.IsAvailable) && ((PrescribingStatus != 1 && PrescribingStatus != 8) || RData.Flags.Value.UseBrand == 1); }
        }
        /*********************************************************************************************************************************************************/

    }

    public class VmpFlags : Flags
    {

        private VmpDetails _vmp;
        private bool? _isFp10 = null;

        public VmpFlags(VmpDetails v)
        {
            _vmp = v;
        }

        public override bool IsActive
        {
            get
            {
                return (IsAvailable || _vmp.Amps.Any(amp => amp.Flags.IsAvailable) && !_vmp.IsMlex2DmdDuplicate);
            }
        }

        public override bool IsAvailable
        {
            get
            {
                return (bool)_vmp.IsAvailable;
            }
        }

        public override bool IsIssuable
        {
            get { return IsAvailable && !(_vmp.IsExtemp || _vmp.IsParallelImport); }
        }

        public override bool IsSecondaryCare
        {
            get { return _vmp.IsSecondaryCare; }
        }

        public override bool IsImported
        {
            get { return _vmp.Amps.All(amp => amp.Flags.IsImported); }
        }

        public override bool IsFp10
        {
            get
            {
                if (_isFp10 != null) return (Boolean)_isFp10;
                return IsFp10England;
            }
            set { _isFp10 = value; }
        }

        public override bool IsFp10England
        {
            get { return IsIssuable && _vmp.RData.Flags.Value.CountrySpecificitiesEngland == 1; }
        }

        public override bool IsFp10Wales
        {
            get { return IsIssuable && _vmp.RData.Flags.Value.CountrySpecificitiesWales == 1; }
        }

        public override bool IsFp10Scotland
        {
            get { return IsIssuable && _vmp.RData.Flags.Value.CountrySpecificitiesScotland == 1; }
        }

        public override bool IsFp10NI
        {
            get { return IsIssuable && _vmp.RData.Flags.Value.CountrySpecificitiesNireland == 1; }
        }

        public override bool UseGeneric
        {
            get { return IsAvailable; }
        }

        public override bool UseBrand
        {
            get { return _vmp.Amps.Any(amp => amp.Flags.IsAvailable) && ((_vmp.PrescribingStatus != 1 && _vmp.PrescribingStatus != 8) || _vmp.RData.Flags.Value.UseBrand == 1); }
        }

        public bool IsSlsEngland
        {
            get { return _vmp.RData.Flags.Value.SlsEngland == 1; }
        }

        public bool IsSlsScotland
        {
            get { return _vmp.RData.Flags.Value.SlsScotland == 1; }
        }

        public bool IsSlsWales
        {
            get { return _vmp.RData.Flags.Value.SlsWales == 1; }
        }

        public bool IsSlsNi
        {
            get { return _vmp.RData.Flags.Value.SlsNireland == 1; }
        }
    }
}
